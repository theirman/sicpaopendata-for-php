<?php

    //CURL
    const MY_CURLOPT_CONNECTTIMEOUT = 5;
    const MY_CURLOPT_MAXREDIRS      = 10;
    const MY_CURLOPT_RETURNTRANSFER = 1;
    const MY_CURLOPT_SSL_VERIFYHOST = false;
    const MY_CURLOPT_SSL_VERIFYPEER = false;
    const MY_CURLOPT_TIMEOUT        = 15;
    const MY_CURLOPT_VERBOSE        = false;

?>