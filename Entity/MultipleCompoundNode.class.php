<?php

    namespace SicpaOpenData\Metadata;
    use SicpaOpenData\Helper as Helper;

    require_once("Entity\AuthorizedFields.class.php");
    require_once("Entity\BaseNode.class.php");
    require_once("Entity\RequiredFields.class.php");
    require_once("Helper\helper.php");



    /** Classe implémentant les nodes à valeurs composées multiples
     *  @author Thierry HEIRMAN
     *  @since Juillet 2021
     */
    class MultipleCompoundNode extends BaseNode
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  

        /** Cette propriété contient la valeur du node */
        private $value;





        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          

        /**
         * Constructeur
         * @param $typeName : nom du node
         * @param $values : valeurs du node
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $value1 = array(
         *                          "name11" => new SimplePrimitiveNode("name11", "value11"),
         *                          "name12" => new MultipleControlledVocabularyNode("name12", new ArrayList<String>(){{ add("controlledValue121"); add("controlledValue122"); }}),
         *                     );
         *      
         *      $value2 = array(
         *                          "name21" => new SimpleControlledVocabularyNode("name211", "value211"),
         *                          "name22" => new MultiplePrimitiveNode("name22", new ArrayList<String>(){{ add("value221"); add("value222"); }}),
         *                     );
         *      
         *      $values = array( $value1, value2 );
         *      
         *      $mcn = new MultipleCompoundNode("name", $values);     
         * </pre>
         */
        public function __construct($typeName, $values=array())
        {
            $this->setMultiple(true);
            $this->setTypeClass("compound");
            $this->setTypeName($typeName);
            $this->value = array();

            $this->addValues($values);
        }





        //     ___  ____________________________  _____  ____
        //    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
        //   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
        //  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
        //

        /**
         * Permet d'obtenir la valeur de l'attribut <strong>value</strong> 
         * @return valeur de l'attribut <strong>value</strong> 
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $value = $mcn->getValue(); 
         * </pre>
         */
        public function getValue()
        {
            return $this->value;
        }


        
        
        
        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             

        /**
         * <strong>addValue</strong> est une méthode qui permet d'ajouter une valeur à la liste des valeurs du node
         * @param $value : valeur à ajouter 
         * @see SimplePrimitiveNode
         * @see SimpleControlledVocabularyNode
         * @see MultiplePrimitiveNode
         * @see MultipleControlledVocabularyNode
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
          *      $value  = array(
         *                          "value1"    => new SimplePrimitiveNode(),
         *                          "value2"    => new SimpleControlledVocabularyNode(),
         *                          "value3"    => new MultiplePrimitiveNode(),
         *                          "value4"    => new MultipleControlledVocabularyNode(),
         *                      );
         *      
         *      $mcn->addValue(value);
         * </pre>
         */
        public function addValue($value)
        {
            if(!$this->isValueValid($value))
                return;

            if (!MultipleCompoundNode::areSubnodesValid($this->getTypeName(), $value))
                return;

            array_push($this->value, $value);
        }

        /**
         * <strong>addValues</strong> est une méthode qui permet d'ajouter une liste de valeurs à la liste des valeurs du node
         * @param $values : valeur à ajouter 
         * @see SimplePrimitiveNode
         * @see SimpleControlledVocabularyNode
         * @see MultiplePrimitiveNode
         * @see MultipleControlledVocabularyNode
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $value1 = array(
         *                          "name11" => new SimplePrimitiveNode("name11", "value11"),
         *                          "name12" => new MultipleControlledVocabularyNode("name12", new ArrayList<String>(){{ add("controlledValue121"); add("controlledValue122"); }}),
         *                     );
         *      
         *      $value2 = array(
         *                          "name21" => new SimpleControlledVocabularyNode("name211", "value211"),
         *                          "name22" => new MultiplePrimitiveNode("name22", new ArrayList<String>(){{ add("value221"); add("value222"); }}),
         *                     );
         *      
         *      $values = array( $value1, value2 );
         *      
         *      $mcn->addValue($values);
         * </pre>
         */
        public function addValues($values)
        {
            foreach ($values as $value)
            {
                if (!is_array($value))
                    continue;

                $this->addValue($value);
            }
        }

        /**
         * <strong>areSubnodesValid</strong> est une méthode qui permet de vérifier que les subnodes sont valides<br>
         * <i>Note : si le nodeName en paramètre est vide, c'est la valeur du node qui sera testée; sinon, c'est la valeur en paramètre qui le sera</i><br>
         * @param $nodeName : le nom du noeud pour lequel vérifier les subnodes
         * @param $value : une hashtable contenant les subnodes à vérifier
         * @return true si les subnodes sont valides, false sinon
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *          if( $this->areSubnodesValid("name", $subnodes) )
         *          {
         *              ...
         *          }
         * </pre>
         */
        private function areSubnodesValid($nodeName="", $value=null)
        {
            if(empty($nodeName))
            {
                foreach ($this->value as $value)
                {
                    foreach($value as $key => $subnode)
                    {
                        if ( !array_key_exists($subnode->getTypeName(), AuthorizedFields::listAuthorizedFieldsForCategoryNode($this->getTypeName())) )
                            return false;

                        if(!Helper::contains(get_class($subnode), AuthorizedFields::getNodeType($subnode->getTypeName())))
                            return false;
                    }
                }

                return true;
            }
            else
            {
                foreach($value as $key=>$subnode)
                {
                    if ( !array_key_exists($subnode->getTypeName(), AuthorizedFields::listAuthorizedFieldsForCategoryNode($nodeName)) )
                        return false;

                    if(!Helper::contains(get_class($subnode), AuthorizedFields::getNodeType($subnode->getTypeName())))
                        return false;
                }

                return true;
            }


        }

        /**
         * <strong>clearValues</strong> est une méthode qui permet de réinitialiser la liste des valeurs du node
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $mcn->clearValues(); 
         * </pre>
         */
        public function clearValues()
        {
            $this->value = array();
        }

        /**
         * <strong>containsValue</strong> est une méthode qui permet de vérifier si une valeur existe dans la liste des valeurs du node
         * @param $value : valeur à vérifier 
         * @return true si la valeur appartient à la liste de valeurs du node, false sinon
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $contains = $mcn->containsValue($value); 
         * </pre>
         */
        public function containsValue($value)
        {
            return $this->indexOf($value) >= 0;
        }

        /**
         * <strong>getValueAt</strong> est une méthode qui permet de récupérer la valeur située à un index précis
         * @param $index : index de la valeur à récupérer 
         * @return valeur situé à l'index
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $value = $mcn->getValueAt(3); 
         * </pre>
         */
        public function getValueAt($index)
        {
            try
            {
                return $this->value[$index];
            }
            catch(Exception $e)
            {
                return null;
            }
        }

        /**
         * <strong>indexOf</strong> est une méthode qui permet de récupérer l'index d'une valeur
         * @param $value : valeur à rechercher 
         * @return index de la valeur
         * @see SimplePrimitiveNode
         * @see SimpleControlledVocabularyNode
         * @see MultiplePrimitiveNode
         * @see MultipleControlledVocabularyNode
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $index = $mcn->indexOf($value);
         * </pre>
         */
        public function indexOf($value)
        {
            foreach ($this->value as $node)
            {
                $contains = true;
                
                foreach ($node as $key => $subnode)
                {
                    if(!array_key_exists($key, $value))
                        $contains = false;

                    if (get_class($node[$key]) != get_class($value[$key]))
                        $contains = false;

                    if (!MultipleCompoundNode::isEquals($node[$key], $value[$key]))
                        $contains = false;
                }

                if ($contains)
                    return array_search($node, $this->getValue());
            }

            return -1;
        }

        /**
         * <strong>isValid</strong> est une méthode qui permet de s'assurer de la validité du node
         * @return true si le node est valide, false sinon
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      if($mcn->isValid())
         *      {
         *          ...
         *      }
         * </pre>
         */
        public function isValid()
        {
            return is_bool($this->getMultiple())        && $this->getMultiple()
                && is_string($this->getTypeClass())     && $this->getTypeClass() == "compound"
                && is_string($this->getTypeName())      
                && is_array($this->getValue())              
                && $this->isValueValid()
                && $this->areSubnodesValid();
        }

        /**
         * <strong>isValueValid</strong> est une méthode qui permet de s'assurer de la validité de la valeur du node<br>
         * <i>Note : si la valeur en paramètre est non nulle, elle sera testée, sinon c'est la valeur du node qui sera testée</i><br>
         * @param $value : valeur à tester 
         * @return true si la valeur est valide, false sinon
         * @see SimplePrimitiveNode
         * @see SimpleControlledVocabularyNode
         * @see MultiplePrimitiveNode
         * @see MultipleControlledVocabularyNode
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      if($this->isValueValid($value))
         *      {
         *          ...
         *      }
         * </pre>
         */
        private function isValueValid($value=null)
        {
            if(is_null($value) || empty($value))
            {
                foreach($this->getValue() as $value)
                {
                    foreach($value as $node)
                    {
                        if ($node instanceof SimplePrimitiveNode
                            || $node instanceof SimpleControlledVocabularyNode
                            || $node instanceof MultiplePrimitiveNode
                            || $node instanceof MultipleControlledVocabularyNode
                           )
                            continue;
                        else
                            return false;
                    }
                }

                return true;
            }
            else
            {
                foreach($value as $node)
                {
                    if ($node instanceof SimplePrimitiveNode
                        || $node instanceof SimpleControlledVocabularyNode
                        || $node instanceof MultiplePrimitiveNode
                        || $node instanceof MultipleControlledVocabularyNode
                       )
                        continue;
                    else
                        return false;
                }

                return true;
            }

        }

        /**
         * <strong>removeAll</strong> est une méthode qui permet de supprimer toutes les valeurs de la liste des valeurs
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $mcn->removeAll(); 
         * </pre>
         */
        public function removeAll()
        {
            $this->clearValues();
        }

        /**
         * <strong>removeValue</strong> est une méthode qui permet de supprimer une valeur de la liste des valeurs
         * @param $value : valeur à supprimer
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $mcn->removeValue($value);
         * </pre>
         */
        public function removeValue($value)
        {
            try
            {
                $index = $this->indexOf($value);
                $this->removeValueAt($index);
            }
            catch(Exception $e)
            {
            }
        }

        /**
         * <strong>removeValueAt</strong> est une méthode qui permet de supprimer la valeur située à un index précis
         * @param $index : index de la valeur à supprimer
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $mcn->removeValueAt(1); 
         * </pre>
         */
        public function removeValueAt($index)
        {
            try
            {
                unset($this->value[$index]);
            }
            catch(Exception $e)
            {
            }
        }

        /**
         * <strong>toArray</strong> est une méthode qui permet d'obtenir le node sous forme d'un tableau
         * @return la représentation du node sous forme d'un tableau
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $array = $this->toArray(); 
         * </pre>
         */
        public function toArray()
        {
            $objectArray["value"] = array();

            foreach ($this->value as $value) 
            {
                $valueArray = array();

                foreach ($value as $field) 
                {
                    $valueArray[$field->getTypeName()]              = array();
                    $valueArray[$field->getTypeName()]["multiple"]  = $field->getMultiple();
                    $valueArray[$field->getTypeName()]["typeClass"] = $field->getTypeClass();
                    $valueArray[$field->getTypeName()]["typeName"]  = $field->getTypeName();
                    $valueArray[$field->getTypeName()]["value"]     = $field->getValue();
                }

                $objectArray["value"][] = $valueArray;
            }
    
            return array(
                            "multiple"  => $this->getMultiple(),
                            "typeClass" => $this->getTypeClass(),
                            "typeName"  => $this->getTypeName(),
                            "value"     => $objectArray
                        );
        }

        /**
         * <strong>toJSON</strong> est une méthode qui permet d'obtenir une représentation JSON du node
         * @return la représentation du node sous forme de chaine JSON
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $json = $mcn->toJSON(); 
         * </pre>
         */
        public function toJSON($prettyPrint=false)
        {
            if (!$this->isValid())
                return "";

            if($prettyPrint)
                return json_encode($this->toArray(), JSON_PRETTY_PRINT);
            else
                return json_encode($this->toArray());
        }

        /**
         * <strong>toString</strong> est une méthode qui permet d'obtenir une représentation textuelle du node (JSON minifié)
         * @return la représentation du node sous forme de chaine de caractère
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $json = $mcn->toString(); 
         * </pre>
         */
        public function toString()
        {
            return $this->toJSON(false);
        }





        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //

        /**
         * <strong>isEquals</strong> est une méthode qui permet de vérifier la similarité de deux nodes
         * @param $node1 : premier node à tester
         * @param $node2 : second node à tester
         * @return true si les deux nodes sont similaires, false sinon
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *          if(MultipleCompoundNode::isEquals($node1, $node2))
         *          {
         *              ...
         *          }
         * </pre>
         */
        public static function isEquals($node1, $node2)
        {
            switch(get_class($node1))
            {
                case "SicpaOpenData\Metadata\SimplePrimitiveNode":
                    return MultipleCompoundNode::isSimplePrimitiveNodeEquals($node1, $node2);

                case "SicpaOpenData\Metadata\SimpleControlledVocabularyNode":
                    return MultipleCompoundNode::isSimpleControlledVocabularyNodeEquals($node1, $node2);

                case "SicpaOpenData\Metadata\MultiplePrimitiveNode":
                    return MultipleCompoundNode::isMultiplePrimitiveNodeEquals($node1, $node2);

                case "SicpaOpenData\Metadata\MultipleControlledVocabularyNode":
                    return MultipleCompoundNode::isMultipleControlledVocabularyNodeEquals($node1, $node2);

                default:
                    return false;
            }
        }

        /**
         * <strong>isSimpleControlledVocabularyNodeEquals</strong> est une méthode qui permet de vérifier la similarité de deux SimpleControlledVocabularyNodes
         * @param $node1 : premier node à tester
         * @param $node2 : second node à tester
         * @return true si les deux nodes sont similaires, false sinon
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *          if(MultipleCompoundNode::isSimpleControlledVocabularyNodeEquals($node1, $node2))
         *          {
         *              ...
         *          }
         * </pre>
         */
        public static function isSimpleControlledVocabularyNodeEquals($node1,  $node2)
        {
            if (get_class($node1) != get_class($node2))
                return false;

            if ($node1->getMultiple() != $node2->getMultiple())
                return false;

            if ($node1->getTypeClass() != $node2->getTypeClass())
                return false;

            if ($node1->getTypeName() != $node2->getTypeName())
                return false;

            if ($node1->getValue() != $node2->getValue())
                return false;

            return true;
        }

        /**
         * <strong>isSimplePrimitiveNodeEquals</strong> est une méthode qui permet de vérifier la similarité de deux SimplePrimitiveNode
         * @param $node1 : premier node à tester
         * @param $node2 : second node à tester
         * @return true si les deux nodes sont similaires, false sinon
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *          if(MultipleCompoundNode::isSimplePrimitiveNodeEquals($node1, $node2))
         *          {
         *              ...
         *          }
         * </pre>
         */
        public static function isSimplePrimitiveNodeEquals($node1, $node2)
        {
            if (get_class($node1) != get_class($node2))
                return false;

            if ($node1->getMultiple() != $node2->getMultiple())
                return false;

            if ($node1->getTypeClass() != $node2->getTypeClass())
                return false;

            if ($node1->getTypeName() != $node2->getTypeName())
                return false;

            if ($node1->getValue() != $node2->getValue())
                return false;

            return true;
        }

        /**
         * <strong>isMultipleControlledVocabularyNodeEquals</strong> est une méthode qui permet de vérifier la similarité de deux MultipleControlledVocabularyNode
         * @param $node1 : premier node à tester
         * @param $node2 : second node à tester
         * @return true si les deux nodes sont similaires, false sinon
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *          if(MultipleCompoundNode::isMultipleControlledVocabularyNodeEquals($node1, $node2))
         *          {
         *              ...
         *          }
         * </pre>
         */
        public static function isMultipleControlledVocabularyNodeEquals($node1, $node2)
        {
            if (get_class($node1) != get_class($node2))
                return false;

            if ($node1->getMultiple() != $node2->getMultiple())
                return false;

            if ($node1->getTypeClass() != $node2->getTypeClass())
                return false;

            if ($node1->getTypeName() != $node2->getTypeName())
                return false;

            foreach ($node1->getValue() as $value)
                if (!in_array($value, $node2->getValue()))
                    return false;

            foreach ($node2->getValue() as $value)
                if (!in_array($value, $node1->getValue()))
                    return false;

            return true;
        }

        /**
         * <strong>isMultiplePrimitiveNodeEquals</strong> est une méthode qui permet de vérifier la similarité de deux MultiplePrimitiveNode
         * @param $node1 : premier node à tester
         * @param $node2 : second node à tester
         * @return true si les deux nodes sont similaires, false sinon
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *          if(MultipleCompoundNode::isMultiplePrimitiveNodeEquals($node1, $node2))
         *          {
         *              ...
         *          }
         * </pre>
         */
        public static function isMultiplePrimitiveNodeEquals($node1, $node2)
        {
            if (get_class($node1) != get_class($node2))
                return false;

            if ($node1->getMultiple() != $node2->getMultiple())
                return false;

            if ($node1->getTypeClass() != $node2->getTypeClass())
                return false;

            if ($node1->getTypeName() != $node2->getTypeName())
                return false;

            foreach ($node1->getValue() as $value)
                if (!in_array($value, $node2->getValue()))
                    return false;

            foreach ($node2->getValue() as $value)
                if (!in_array($value, $node1->getValue()))
                    return false;

            return true;
        }

    }

?>