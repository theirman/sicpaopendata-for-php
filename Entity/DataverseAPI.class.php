<?php

    namespace SicpaOpenData\API;
    use SicpaOpenData\Helper as Helper;

    require_once("Helper/helper.php");
    require_once("Config/config-curl.php");



    /** Classe implémentant les méthodes d'accès aux dataverses INRAE
     *  @author Thierry HEIRMAN
     *  @since Juillet 2021
     */
    class DataverseAPI
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  





        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          





        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             





        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //




        /**
         * Cette méthode permet d'éxécuter une requête de type GET
         * @param $apiToken         jeton API de l'utilisateur
         * @param $endpoint         adresse vers la ressource HTTP
         * @return                  reponse http
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $jetonAPI   = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *      $endpoint   = "http://localhost:57170/api/method?param=1234";
         *          
         *      $reponse    = DataverseAPI::executeGetRequest($jetonAPI, $endpoint);
         * </pre>
         */
        private static function executeGetRequest($apiToken, $endpoint)
        {
            // j'instancie mon client de connexion 
            $client = Helper::getHttpClient();

            // je met à jour mon client avec l'adresse du endpoint
            curl_setopt($client, CURLOPT_URL, $endpoint);

            // je met à jour mon client avec les entetes HTTP
            curl_setopt($client, CURLOPT_HTTPHEADER, DataverseAPI::getHttpHeaders(null, $apiToken));

            try
            {
                // j'exécute la requête et je retourne la réponse
                return curl_exec($client);                
            }
            catch(Exception $e)
            {                
            }
            finally
            {
                // je clos le client HTTP
                curl_close($client);
            }
        }
            
        /**
         * Cette méthode permet d'éxécuter une requête de type POST
         * @param $apiToken         jeton API de l'utilisateur
         * @param $endpoint         adresse vers la ressource HTTP
         * @param $data             paramètre POST de la requete
         * @return                  reponse http
         * 
         * <hr>
         * <strong>Exemple : POST sans paramètre</strong>
         * <pre>
         *      $jetonAPI       = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *      $endpoint       = "http://localhost:57170/api/method?param=1234";
         *          
         *      $reponse        = DataverseAPI::executePostRequest($jetonAPI, $endpoint);
         * </pre>
         * <strong>Exemple : POST avec paramètre</strong>
         * <pre>
         *      $jetonAPI       = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *      $endpoint       = "http://localhost:57170/api/method?param=1234";
         *      $data           = "ceci est un test";
         *          
         *      $reponse   = DataverseAPI.executePostRequest($jetonAPI, $endpoint, $data);
         * </pre>
         * <strong>Exemple : POST avec fichier</strong>
         * <pre>
         *      $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *      $endpoint         = "http://localhost:57170/api/method?param=1234";
         *      $cheminFichier    = "c:\\temp\\fichier.csv";
         *      $data             = array
         *                          (
         *                              'file' => new \CURLFile(realpath($cheminFichier), mime_content_type($cheminFichier), basename($cheminFichier))
         *                          );
         *          
         *      $reponse   = DataverseAPI.executePostRequest($jetonAPI, $endpoint, $data);
         * </pre>
         */
        private static function executePostRequest($apiToken, $endpoint, $data=null)
        {
            // j'instancie mon client de connexion 
            $client = Helper::getHttpClient();

            // je met à jour mon client avec l'adresse du endpoint
            curl_setopt($client, CURLOPT_URL, $endpoint);

            // je met à jour mon client avec les entetes HTTP
            curl_setopt($client, CURLOPT_HTTPHEADER, DataverseAPI::getHttpHeaders("multipart/form-data", $apiToken));

            // je définis ma requête comme une requete POST
            curl_setopt($client, CURLOPT_POST, true);
            
            // si la reqete POST contient des données, je les y ajoute
            if ($data)
                curl_setopt($client, CURLOPT_POSTFIELDS, $data);

            try
            {
                // j'exécute la requête et je retourne la réponse
                return curl_exec($client);                
            }
            catch(Exception $e)
            {                
            }
            finally
            {
                // je clos le client HTTP
                curl_close($client);
            }
        }


        /**
         * Cette méthode permet de lister le contenu d'un dataset sous la forme d'une trame JSON
         * @param $serverBaseURI    adresse du dataverse
         * @param $apiToken         jeton API de l'utilisateur
         * @param $doi              DOI ciblé
         * @return                  trame JSON détaillant le contenu du dataset
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $adresseServeur = "http://localhost:57170";
         *      $doi            = "doi:10.12345/ABCDEF";
         *      $jetonAPI       = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *          
         *      $contenuDataset = DataverseAPI::getDatasetContents($adresseServeur, $jetonAPI, $doi);
         * </pre>
         */
        public static function getDatasetContents($serverBaseURI, $apiToken, $doi)
        {
            // je déclare et initialise mon endpoint
            $endpoint = "/api/datasets/:persistentId/?persistentId={PERSISTENT_IDENTIFIER}";

            // je met à jour le endpoint avec les paramètres de la requete url
            $endpoint = preg_replace("#{PERSISTENT_IDENTIFIER}#", $doi, $endpoint);
            $endpoint = preg_replace("#\/*$#", "", $serverBaseURI) . $endpoint;

            // j'execute la requete et retourne le résultat
            return DataverseAPI::executeGetRequest($apiToken, $endpoint);        
        }

        /**
         * Cette méthode permet de lister les fichiers d'un dataset sous la forme d'une trame JSON
         * @param $serverBaseURI     adresse du dataverse
         * @param $apiToken          jeton API de l'utilisateur
         * @param $datasetID         identifiant numérique du dataset
         * @param $datasetVersion    numéro de version du dataset
         * @return                   trame JSON listant les fichiers du dataset
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $adresseServeur   = "http://localhost:57170";
         *      $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *      $idDataset        = "123456";
         *      $versionDataset   = "3.0";
         *          
         *      $listeFichiers    = DataverseAPI::getDatasetFiles($adresseServeur, $jetonAPI, $idDataset, $versionDataset);
         * </pre>
         */
        public static function getDatasetFiles($serverBaseURI, $apiToken, $datasetID, $datasetVersion)
        {
            // je déclare et initialise mon endpoint
            $endpoint = "/api/datasets/{DATASET_ID}/versions/{DATASET_VERSION}/files";

            // je met à jour le endpoint avec les paramètres de la requete url
            $endpoint = preg_replace("#{DATASET_ID}#", $datasetID, $endpoint);
            $endpoint = preg_replace("#{DATASET_VERSION}#", $datasetVersion, $endpoint);
            $endpoint = preg_replace("#\/*$#", "", $serverBaseURI) . $endpoint;
            
            // j'execute la requete et retourne le résultat
            return DataverseAPI::executeGetRequest($apiToken, $endpoint);        
        }

        /**
         * Cette méthode permet d'exporter les métadonnées d'un dataset dans un format de métadonnées demandé en paramètre
         * @param $serverBaseURI     adresse du dataverse
         * @param $apiToken          jeton API de l'utilisateur
         * @param $doi               DOI ciblé
         * @param $metadataFormat    format des métadonnées
         * @return                   trame JSON détaillant les métadonnées dans le format démandé
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $adresseServeur   = "http://localhost:57170";
         *      $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *      $doi              = "doi:10.12345/ABCDEF";
         *      $formatMetadonnee = "Datacite";
         *          
         *      $metadonnees      = DataverseAPI::getDatasetMetadataExport($adresseServeur, $jetonAPI, $doi, $formatMetadonnee);
         * </pre>
         */
        public static function getDatasetMetadataExport($serverBaseURI, $apiToken, $doi, $metadataFormat)
        {
            // je déclare et initialise mon endpoint
            $endpoint = "/api/datasets/export?exporter={METADATA_FORMAT}&persistentId={PERSISTENT_IDENTIFIER}";

            // je met à jour le endpoint avec les paramètres de la requete url
            $endpoint = preg_replace("#{PERSISTENT_IDENTIFIER}#", $doi, $endpoint);
            $endpoint = preg_replace("#{METADATA_FORMAT}#", $metadataFormat, $endpoint);
            $endpoint = preg_replace("#\/*$#", "", $serverBaseURI) . $endpoint;
            
            // j'execute la requete et retourne le résultat
            return DataverseAPI::executeGetRequest($apiToken, $endpoint);        
        }

        /**
         * Cette méthode permet de lister le contenu d'une version d'un dataset sous la forme d'une trame JSON
         * @param $serverBaseURI     adresse du dataverse
         * @param $apiToken          jeton API de l'utilisateur
         * @param $datasetID         identifiant numérique du dataset
         * @param $datasetVersion    numéro de version du dataset
         * @return                   trame JSON détaillant le contenu d'une version du dataset
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $adresseServeur   = "http://localhost:57170";
         *      $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *      $idDataset        = "123456";
         *      $versionDataset   = "3.0";
         *          
         *      $versionDataset   = DataverseAPI::getDatasetVersion($adresseServeur, $jetonAPI, $idDataset, $versionDataset);
         * </pre>
         */
        public static function getDatasetVersion($serverBaseURI, $apiToken, $datasetID, $datasetVersion)
        {
            // je déclare et initialise mon endpoint
            $endpoint = "/api/datasets/{DATASET_ID}/versions/{DATASET_VERSION}";

            // je met à jour le endpoint avec les paramètres de la requete url
            $endpoint = preg_replace("#{DATASET_ID}#", $datasetID, $endpoint);
            $endpoint = preg_replace("#{DATASET_VERSION}#", $datasetVersion, $endpoint);
            $endpoint = preg_replace("#\/*$#", "", $serverBaseURI) . $endpoint;
            
            // j'execute la requete et retourne le résultat
            return DataverseAPI::executeGetRequest($apiToken, $endpoint);        
        }

        /**
         * Cette méthode permet de lister toutes les versions d'un dataset sous la forme d'une trame JSON
         * @param $serverBaseURI     adresse du dataverse
         * @param $apiToken          jeton API de l'utilisateur
         * @param $datasetID         identifiant numérique du dataset
         * @return                   trame JSON listant toutes les versions du dataset
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $adresseServeur   = "http://localhost:57170";
         *      $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *      $idDataset        = "123456";
         *          
         *      $listeVersions    = DataverseAPI::getDatasetVersionList($adresseServeur, $jetonAPI, $idDataset);
         * </pre>
         */
        public static function getDatasetVersionList($serverBaseURI, $apiToken, $datasetID)
        {
            // je déclare et initialise mon endpoint
            $endpoint = "/api/datasets/{DATASET_ID}/versions";

            // je met à jour le endpoint avec les paramètres de la requete url
            $endpoint = preg_replace("#{DATASET_ID}#", $datasetID, $endpoint);
            $endpoint = preg_replace("#\/*$#", "", $serverBaseURI) . $endpoint;
            
            // j'execute la requete et retourne le résultat
            return DataverseAPI::executeGetRequest($apiToken, $endpoint);        
        }

        /**
         * Cette méthode permet de lister le contenu d'un dataverse sous la forme d'une trame JSON
         * @param $serverBaseURI     adresse du dataverse
         * @param $apiToken          jeton API de l'utilisateur
         * @param $dataverseID       identifiant du dataverse
         * @return                   trame JSON détaillant le contenu du dataverse
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $adresseServeur   = "http://localhost:57170";
         *      $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *      $idDataverse      = "INRAE Dataverse";
         *          
         *      $contenuDataverse = DataverseAPI::getDataverseContents($adresseServeur, $jetonAPI, $idDataverse);
         * </pre>
         */
        public static function getDataverseContents($serverBaseURI, $apiToken, $dataverseID)
        {
            // je déclare et initialise mon endpoint
            $endpoint = "/api/dataverses/{DATAVERSE_ID}/contents";

            // je met à jour le endpoint avec les paramètres de la requete url
            $endpoint = preg_replace("#{DATAVERSE_ID}#", $dataverseID, $endpoint);
            $endpoint = preg_replace("#\/*$#", "", $serverBaseURI) . $endpoint;
            
            // j'execute la requete et retourne le résultat
            return DataverseAPI::executeGetRequest($apiToken, $endpoint);        
        }

        /**
         * Cette méthode permet de lister les infos concernant un dataverse sous la forme d'une trame JSON
         * @param serverBaseURI     adresse du dataverse
         * @param apiToken          jeton API de l'utilisateur
         * @param dataverseID       identifiant du dataverse
         * @return trame JSON détaillant le contenu du dataverse
         * @throws IOException 
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *         $adresseServeur   = "http://localhost:57170";
         *         $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *         $idDataverse      = "INRAE Dataverse";
         *         
         *         $infosDataverse   = DataverseAPI::GetDataverseInfos($adresseServeur, $jetonAPI, $idDataverse);
         * </pre>
         */
        public static function getDataverseInfos($serverBaseURI, $apiToken, $dataverseID)
        {
            // je déclare et initialise mon endpoint
            $endpoint = "/api/dataverses/{DATAVERSE_ID}";

            // je met à jour le endpoint avec les paramètres de la requete url
            $endpoint = preg_replace("#{DATAVERSE_ID}#", $dataverseID, $endpoint);
            $endpoint = preg_replace("#\/*$#", "", $serverBaseURI) . $endpoint;
            
            // j'execute la requete et retourne le résultat
            return DataverseAPI::executeGetRequest($apiToken, $endpoint);        
        }

        /**
         * Cette méthode permet de lister les métdonnées d'un dataverse sous la forme d'une trame JSON
         * @param $serverBaseURI     adresse du dataverse
         * @param $apiToken          jeton API de l'utilisateur
         * @param $dataverseID       identifiant du dataverse
         * @return                   trame JSON listant les métadonnées d'un dataverse
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *         $adresseServeur   = "http://localhost:57170";
         *         $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *         $idDataverse      = "INRAE Dataverse";
         *         
         *         $metadonnees      = DataverseAPI::getDataverseMetadata($adresseServeur, $jetonAPI, $idDataverse);
         * </pre>
         */
        public static function getDataverseMetadata($serverBaseURI, $apiToken, $dataverseID)
        {
            // je déclare et initialise mon endpoint
            $endpoint = "/api/dataverses/{DATAVERSE_ID}/metadatablocks";

            // je met à jour le endpoint avec les paramètres de la requete url
            $endpoint = preg_replace("#{DATAVERSE_ID}#", $dataverseID, $endpoint);
            $endpoint = preg_replace("#\/*$#", "", $serverBaseURI) . $endpoint;
            
            // j'execute la requete et retourne le résultat
            return DataverseAPI::executeGetRequest($apiToken, $endpoint);        
        }
      
        /**
         * Cette méthode permet de créer les entêtes HTTP d'une requête REST
         * @param $contentType          le type de contenu de la requete REST
         * @param $token                le token d'authentification si nécessaire
         * @return                      entetes HTTP
         *
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *         $typeDeContenu    = "application/json";
         *         $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *         
         *         $entetesHTTP      = DataverseAPI::getHttpHeaders($typeDeContenu, $jetonAPI);
         * </pre>
         */
        public static function getHttpHeaders($contentType="", $token="")
        {
            $headers = array();

            if(!empty($contentType))
                array_push($headers, "content-type: ".$contentType);

            if(!empty($token))
                array_push($headers, "X-Dataverse-key: ".$token);

            return $headers;
        }

        /**
         * Cette méthode permet d'uploader un fichier vers un dataset
         * @param $serverBaseURI     adresse du dataverse
         * @param $apiToken          jeton API de l'utilisateur
         * @param $doi               DOI ciblé
         * @param $filePath          chemin vers le fichier à uploader
         * @return                   trame JSON détaillant le succès/l'échec de l'upload de fichier
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *          $adresseServeur   = "http://localhost:57170";
         *          $doi              = "doi:10.12345/ABCDEF";
         *          $cheminFichier    = "c:\\temp\\fichier.csv";
         *          $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *          
         *          $status           = DataverseAPI::postDatasetAddFile($adresseServeur, $jetonAPI, $doi, $cheminFichier);
         * </pre>
         */
        public static function postDatasetAddFile($serverBaseURI, $apiToken, $doi, $filePath)
        {
            // je déclare et initialise mon endpoint
            $endpoint = "/api/datasets/:persistentId/add?persistentId={PERSISTENT_IDENTIFIER}";

            // je met à jour le endpoint avec les paramètres de la requete url
            $endpoint = preg_replace("#{PERSISTENT_IDENTIFIER}#", $doi, $endpoint);
            $endpoint = preg_replace("#\/*$#", "", $serverBaseURI) . $endpoint;
            
            // je prépare le fichier des métadonnées
            $data = array
                    (
                        'file' => new \CURLFile(realpath($filePath), mime_content_type($filePath), basename($filePath))
                    );

            // j'execute la requete et retourne le résultat
            return DataverseAPI::executePostRequest($apiToken, $endpoint, $data);        
        }

        /**
         * Cette méthode permet de créer un dataset
         * @param $serverBaseURI     adresse du dataverse
         * @param $apiToken          jeton API de l'utilisateur
         * @param $dataverseID       DOI ciblé
         * @param $metadataJSON      chemin vers le fichier à uploader
         * @return                   trame JSON détaillant le succès/l'échec de la création du dataset
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *          $adresseServeur   = "http://localhost:57170";
         *          $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *          $idDataverse      = "INRAE Dataverse";
         *          $metadata          = file_get_contents("c:\\temp\\metadata.json");
         *          
         *          $status           = DataverseAPI::postDatasetCreate($adresseServeur, $jetonAPI, $idDataverse, $metadata);
         * </pre>
         */
        public static function postDatasetCreate($serverBaseURI, $apiToken, $dataverseID, $metadataJSON)
        {
            // je déclare et initialise mon endpoint
            $endpoint = "/api/dataverses/{DATAVERSE_ID}/datasets";

             // je met à jour le endpoint avec les paramètres de la requete url
            $endpoint = preg_replace("#{DATAVERSE_ID}#", $dataverseID, $endpoint);
            $endpoint = preg_replace("#\/*$#", "", $serverBaseURI) . $endpoint;
            
            // j'execute la requete et retourne le résultat
            return DataverseAPI::executePostRequest($apiToken, $endpoint, $metadataJSON);        
        }

        /**
         * Cette méthode permet de publier un dataset
         * @param $serverBaseURI     adresse du dataverse
         * @param $apiToken          jeton API de l'utilisateur
         * @param $doi               DOI ciblé
         * @param $versionType       le type de version {minor | major}
         * @return                   trame JSON détaillant le succès/l'échec de la publication du dataset
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *          $adresseServeur   = "http://localhost:57170";
         *          $doi              = "doi:10.12345/ABCDEF";
         *          $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *          $typeVersion      = "minor";
         *          
         *          $status           = DataverseAPI::postDatasetPublish($adresseServeur, $jetonAPI, $doi, $typeVersion);
         * </pre>
         */
        public static function postDatasetPublish($serverBaseURI, $apiToken, $doi, $versionType)
        {
            // je déclare et initialise mon endpoint
            $endpoint = "/api/datasets/:persistentId/actions/:publish?persistentId={PERSISTENT_IDENTIFIER}&type={VERSION_TYPE}";

             // je met à jour le endpoint avec les paramètres de la requete url
            $endpoint = preg_replace("#{PERSISTENT_IDENTIFIER}#", $doi, $endpoint);
            $endpoint = preg_replace("#{VERSION_TYPE}#", $versionType, $endpoint);
            $endpoint = preg_replace("#\/*$#", "", $serverBaseURI) . $endpoint;
            
            // j'execute la requete et retourne le résultat
            return DataverseAPI::executePostRequest($apiToken, $endpoint);        
        }
    }

?>