<?php

    namespace SicpaOpenData\Metadata;
    use SicpaOpenData\Helper as Helper;

    require_once("Entity/CategoryNode.class.php");



    /** Classe implémentant le node citation
     *  @author Thierry HEIRMAN
     *  @since Juillet 2021
     */
    class SocialScienceNode extends CategoryNode
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  


        /** Cette propriété recense la liste des fields requis pour ce node */
        private $authorizedFields;

        /** Cette propriété recense la liste des fields requis pour ce node */
        private $requiredFields;



        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          

        /**
         * Constructeur sans paramètre
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $ssn = new SocialScienceNode(); 
         * </pre>
         */
        public function __construct()
        {
            $this->setDisplayName("Social Science and Humanities Metadata");
            $this->initFields();
            $this->authorizedFields   = AuthorizedFields::listAuthorizedFieldsForSocialScienceNode();
            $this->requiredFields     = RequiredFields::listRequiredFieldsForSocialScienceNode();
        }


        
        
        
        //     ___  ____________________________  _____  ____
        //    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
        //   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
        //  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
        //





        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             

        /**
         * <strong>addMultipleCompoundNodeField</strong> est une méthode qui permet d'ajouter un noeud de type MultipleCompoundNode à la liste des fields du node
         * @param $node : node à ajouter 
         * @see MultipleCompoundNode
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $value1 = array
         *      (
         *          "name11"    => new SimplePrimitiveNode("name11", "value11"),
         *          "name12"    => new SimplePrimitiveNode("name12", "value12"),
         *      );
         *  
         *      $value2 = array
         *      (
         *          "name21"    => new MultipleControlledVocabularyNode("name21", "value21"),
         *          "name22"    => new SimplePrimitiveNode("name22", "value22"),
         *      );
         *
         *      $ssn = new SocialScienceNode();
         *      $ssn->addField(new MultipleCompoundNode("name", array($value1, $value2)));
         * </pre>
         */
        protected function addMultipleCompoundNodeField($node)
        {
            if (!$node->isValid() || !array_key_exists($node->getTypeName(), $this->authorizedFields) || $this->authorizedFields[$node->getTypeName()] != "MultipleCompoundNode")
                return false;

            array_push($this->fields, $node);
            return in_array($node, $this->fields);
        }

        /**
         * <strong>addMultipleControlledVocabularyNodeField</strong> est une méthode qui permet d'ajouter un noeud de type MultipleControlledVocabularyNode à la liste des fields du node
         * @param $node : node à ajouter 
         * @see MultipleControlledVocabularyNode
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $values = array
         *      (
         *          "controlledValue1",
         *          "controlledValue2",
         *          "controlledValue3",
         *      );
         *          
         *      $ssn = new SocialScienceNode();
         *      $ssn->addField(new MultipleControlledVocabularyNode("name", values));
         * </pre>
         */
        protected function addMultipleControlledVocabularyNodeField($node)
        {
            if (!$node->isValid() || !array_key_exists($node->getTypeName(), $this->authorizedFields) || $this->authorizedFields[$node->getTypeName()] != "MultipleControlledVocabularyNode")
                return false;

            array_push($this->fields, $node);
            return in_array($node, $this->fields);
        }

        /**
         * <strong>addMultiplePrimitiveNodeField</strong> est une méthode qui permet d'ajouter un noeud de type MultiplePrimitiveNode à la liste des fields du node
         * @param $node : node à ajouter 
         * @see MultiplePrimitiveNode
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $values = array
         *      (
         *          "value1",
         *          "value2",
         *          "value3",
         *      );
         *          
         *      $ssn = new SocialScienceNode();
         *      $ssn->addField(new MultiplePrimitiveNode("name", values));
         * </pre>
         */
        protected function addMultiplePrimitiveNodeField($node)
        {
            if (!$node->isValid() || !array_key_exists($node->getTypeName(), $this->authorizedFields) || $this->authorizedFields[$node->getTypeName()] != "MultiplePrimitiveNode")
                return false;

            array_push($this->fields, $node);
            return in_array($node, $this->fields);
        }

        /**
         * <strong>addSimpleCompoundNodeField</strong> est une méthode qui permet d'ajouter un noeud de type SimpleCompoundNode à la liste des fields du node
         * @param $node : node à ajouter 
         * @see SimpleCompoundNode
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $value = array
         *      (
         *          "name1" => new SimplePrimitiveNode("name1", "value1"),
         *          "name2" => new SimplePrimitiveNode("name2", "value2"),
         *          "name3" => new SimplePrimitiveNode("name3", "value3"),
         *      );
         *  
         *      $ssn = new SocialScienceNode();
         *      $ssn->addField(new SimpleCompoundNode("name", value));
         * </pre>
         */
        protected function addSimpleCompoundNodeField($node)
        {
            if (!$node->isValid() || !array_key_exists($node->getTypeName(), $this->authorizedFields) || $this->authorizedFields[$node->getTypeName()] != "SimpleCompoundNode")
                return false;

            array_push($this->fields, $node);
            return in_array($node, $this->fields);
        }

        /**
         * <strong>addSimpleControlledVocabularyNodeField</strong> est une méthode qui permet d'ajouter un noeud de type SimpleControlledVocabularyNode à la liste des fields du node
         * @param $node : node à ajouter 
         * @see SimpleControlledVocabularyNode
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $ssn = new SocialScienceNode();
         *      $ssn->addField(new SimpleControlledVocabularyNode("name", "controlledValue"));
         * </pre>
         */
        protected function addSimpleControlledVocabularyNodeField($node)
        {
            if (!$node->isValid() || !array_key_exists($node->getTypeName(), $this->authorizedFields) || $this->authorizedFields[$node->getTypeName()] != "SimpleControlledVocabularyNode")
                return false;

            array_push($this->fields, $node);
            return in_array($node, $this->fields);
        }

        /**
         * <strong>addSimplePrimitiveNodeField</strong> est une méthode qui permet d'ajouter un noeud de type SimplePrimitiveNode à la liste des fields du node
         * @param $node : node à ajouter 
         * @see SimplePrimitiveNode
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $ssn = new SocialScienceNode();
         *      $ssn->addField(new SimplePrimitiveNode("name", "value"));
         * </pre>
         */
        protected function addSimplePrimitiveNodeField($node)
        {
            if (!$node->isValid() || !array_key_exists($node->getTypeName(), $this->authorizedFields) || $this->authorizedFields[$node->getTypeName()] != "SimplePrimitiveNode")
                return false;

            array_push($this->fields, $node);
            return in_array($node, $this->fields);
        }

        /**
         * <strong>isValid</strong>  est une méthode qui permet de s'assurer de la validité du node
         * @return true si le node est valide, false sinon
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      if($ssn->isValid)
         *      {
         *          ...
         *      }
         * </pre>
         */
        public function isValid()
        {
            $isRequiredPresent   = true;
            $isPresentAuthorized = true;

            foreach ($this->requiredFields as $name=>$class)
            {
                if(!in_array($name, $this->fieldNames()))
                {
                    $isRequiredPresent = false;
                    break;
                }
            }

            foreach ($this->fields as $field)
            {
                if(!array_key_exists($field->getTypeName(), $this->authorizedFields))
                {
                    $isPresentAuthorized = false;
                    break;
                }

                if(!Helper::contains(get_class($field), $this->authorizedFields[$field->getTypeName()]))
                {
                    $isPresentAuthorized = false;
                    break;
                }
            }

            return $this->getDisplayName() == "Social Science and Humanities Metadata" && $isRequiredPresent && $isPresentAuthorized;
        }






        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //



    }


?>

