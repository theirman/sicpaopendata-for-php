<?php

    namespace SicpaOpenData\Metadata;

    

    /** Classe implémentant les nodes de base
     *  @author Thierry HEIRMAN
     *  @since Juillet 2021
     */
    class BaseNode
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  

        /** Cette propriété indique si l'on est en présence d'un noeud mono- ou multi-valeur(s) */
        private $multiple; 

        /** Cette propriété indique le type de la valeur attendue */
        private $typeClass; 

        /** Cette propriété identifie la valeur */
        private $typeName; 


        
        
        
        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          

        /**
         * Constructeur sans paramètre
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $bn = new BaseNode();
         * </pre>
         */
        public function __construct()
        {
            $this->setMultiple(false);
            $this->setTypeClass("");
            $this->setTypeName("");
        }
        
        
        
        
        //     ___  ____________________________  _____  ____
        //    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
        //   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
        //  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
        //

        /**
         * Permet d'obtenir la valeur de l'attribut <strong>multiple</strong> 
         * @return valeur de l'attribut <strong>multiple</strong>
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $multiple = $bn.getMultiple(); 
         * </pre>
         */
        public function getMultiple()
        {
            return $this->multiple;
        }
        
        /**
         * Permet d'obtenir la valeur de l'attribut <strong>typeClass</strong> 
         * @return valeur de l'attribut <strong>typeClass</strong>
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $typeClass = $bn.getTypeClass(); 
         * </pre>
         */
        public function getTypeClass()
        {
            return $this->typeClass;
        }

        /**
         * Permet d'obtenir la valeur de l'attribut <strong>typeName</strong> 
         * @return valeur de l'attribut <strong>typeName</strong>
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $typeName = $bn.getTypeName();
         * </pre>
         */
        public function getTypeName()
        {
            return $this->typeName;
        }

        /**
         * Permet de mettre à jour la valeur de l'attribut <strong>multiple</strong>
         * @param $multiple : valeur à enregistrer dans l'attribut <strong>multiple</strong>
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $bn.setMultiple(true); 
         * </pre>
         */
        protected function setMultiple($multiple)
        {
            $this->multiple = $multiple;
        }
        
        /**
         * Permet de mettre à jour la valeur de l'attribut <strong>typeClass</strong>
         * @param $typeClass : valeur à enregistrer dans l'attribut <strong>typeClass</strong>
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $bn.setTypeClass("compound"); 
         * </pre>
         */
        protected function setTypeClass($typeClass)
        {
            $this->typeClass = $typeClass;
        }

        /**
         * Permet de mettre à jour la valeur de l'attribut <strong>typeName</strong>
         * @param $typeName : valeur à enregistrer dans l'attribut <strong>typeName</strong>
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $bn.setTypeName("language"); 
         * </pre>
         */
        public function setTypeName($typeName)
        {
            $this->typeName = $typeName;
        }


        
        
        
        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             

        /**
         * <strong>toArray</strong> est une méthode qui permet d'obtenir le node sous forme d'un tableau
         * @return la représentation du node sous forme d'un tableau
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $array = $this->toArray(); 
         * </pre>
         */
        public function toArray()
        {
            return array(
                            "multiple"  => $this->getMultiple(),
                            "typeClass" => $this->getTypeClass(),
                            "typeName"  => $this->getTypeName(),
                            "value"     => $this->getValue()
                        );
        }



        
        
        
        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //





    }


?>