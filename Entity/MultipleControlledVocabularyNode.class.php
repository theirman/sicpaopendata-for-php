<?php

    namespace SicpaOpenData\Metadata;

    require_once("Entity\BaseNode.class.php");
    require_once("Entity\ControlledVocabulary.class.php");



    /** Classe implémentant les nodes à valeurs textuelles multiples, valeurs contraintes par un vocabulaire contrôlé
     *  @author Thierry HEIRMAN
     *  @since Juillet 2021
     */
    class MultipleControlledVocabularyNode extends BaseNode
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  

        /** Cette propriété contient la valeur du node */
        private $value;





        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          

        /**
         * Constructeur
         * @param $typeName :   nom du node
         * @param $values :     valeurs du node
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $values = array
         *      (
         *          "controlledValue1",
         *          "controlledValue2",
         *          "controlledValue3",
         *      );
         *      
         *      $mcvn = new MultipleControlledVocabularyNode("name", $values);     
         * </pre>
         */
        public function __construct($typeName="", $values=array())
        {
            $this->setMultiple(true);
            $this->setTypeClass("controlledVocabulary");
            $this->setTypeName($typeName);
            $this->value = array();

            $this->addValues($values);
        }





        //     ___  ____________________________  _____  ____
        //    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
        //   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
        //  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
        //

        /**
         * Permet d'obtenir la valeur de l'attribut <strong>value</strong> 
         * @return valeur de l'attribut <strong>value</strong> 
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $value = $mcvn->getValue(); 
         * </pre>
         */
        public function getValue()
        {
            return $this->value;
        }


        
        
        
        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             

        /**
         * <strong>addValue</strong> est une méthode qui permet d'ajouter une valeur à la liste des valeurs du node
         * @param $value : valeur à ajouter
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $mcvn->addValue("value"); 
         * </pre>
         */
        public function addValue($value)
        {
            if(!$this->isAuthorizedValue($value))
                return;
                
            array_push($this->value, $value);
        }

        /**
         * <strong>addValues</strong> est une méthode qui permet d'ajouter une liste de valeurs à la liste des valeurs du node
         * @param $values : valeurs à ajouter
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $values = array
         *      (
         *          "controlledValue1",
         *          "controlledValue2",
         *          "controlledValue3",
         *      );
         *      
         *      $mcvn->addValues($values);
         * </pre>
         */
        public function addValues($values)
        {
            foreach($values as $value)
                $this->addValue($value);
        }

        /**
         * <strong>clearValues</strong> est une méthode qui permet de réinitialiser la liste des valeurs du node
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $mcvn->clearValues(); 
         * </pre>
         */
        public function clearValues()
        {
            $this->value = array();
        }

        /**
         * <strong>containsValue</strong> est une méthode qui permet de vérifier si une valeur existe dans la liste des valeurs du node
         * @param $value : valeur à vérifier
         * @return true si la valeur appartient à la liste de valeurs du node, false sinon
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $mcvn->containsValue("value"); 
         * </pre>
         */
        public function containsValue($value)
        {
            return in_array($value, $this->getValue());
        }

        /**
         * <strong>getValueAt</strong> est une méthode qui permet de récupérer la valeur située à un index précis
         * @param $index : index de la valeur à récupérer
         * @return valeur situé à l'index
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $mcvn->getValueAt(3); 
         * </pre>
         */
        public function getValueAt($index)
        {
            try
            {
                return $this->value[$index];
            }
            catch(Exception $e)
            {
                return "";
            }
         }

        /**
         * <strong>indexOf</strong> est une méthode qui permet de récupérer l'index d'une valeur
         * @param $value : valeur à rechercher
         * @return index de la valeur
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $mcvn->indexOf("value"); 
         * </pre>
         */
        public function indexOf($value)
        {
            return array_search($value, $this->getValue());
        }

        /**
         * <strong>isAuthorizedValue</strong> est une méthode qui permet de s'assurer que la valeur en paramètre est bien une valeur autorisée par le vocabulaire contrôlé
         * @param $value : valeur à tester
         * @return true si le node est valide, false sinon
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      if($mcvn->isAuthorizedValue("value"))
         *      {
         *          ...
         *      }
         * </pre>
         */
        public function isAuthorizedValue($value)
        {
            $controlledVocabulary = ControlledVocabulary::listValuesFor($this->getTypeName());
            return in_array($value, $controlledVocabulary);
        }

        /**
         * <strong>isValid</strong> est une méthode qui permet de s'assurer de la validité du node
         * @return true si le node est valide, false sinon
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      if($mcvn->isValid())
         *      {
         *          ...
         *      }
         * </pre>
         */
        public function isValid()
        {
            return is_bool($this->getMultiple())        && $this->getMultiple()
                && is_string($this->getTypeClass())     && $this->getTypeClass() == "controlledVocabulary"
                && is_string($this->getTypeName())
                && is_array($this->getValue())          && $this->isValueValid();
        }

        /**
         * <strong>isValueValid</strong> est une méthode qui permet de s'assurer de la validité de la valeur <br/>
         * => Chaque élément de la liste de valeurs du node doit être une chaîne de caractères
         * @return true si la valeur est valide, false sinon
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *          if($this->isValueValid())
         *          {
         *              ...
         *          }
         * </pre>
         */
        private function isValueValid()
        {
            foreach ($this->value as $value)
                if (!is_string($value))
                    return false;

            return true;
        }

        /**
         * <strong>removeAll</strong> est une méthode qui permet de supprimer toutes les valeurs de la liste des valeurs
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $mcvn->removeAll(); 
         * </pre>
         */
        public function removeAll()
        {
            $this->clearValues();
        }

        /**
         * <strong>removeValue</strong> est une méthode qui permet de supprimer une valeur de la liste des valeurs
         * @param $value : valeur à supprimer
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $mcvn->removeValue("value"); 
         * </pre>
         */
        public function removeValue($value)
        {
            try
            {
                $index = $this->indexOf($value);
                $this->removeValueAt($index);
            }
            catch(Exception $e)
            {
            }
        }

        /**
         * <strong>removeValueAt</strong> est une méthode qui permet de supprimer la valeur située à un index précis
         * @param $index : index de la valeur à supprimer
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $mcvn->removeValueAt(3); 
         * </pre>
         */
        public function removeValueAt($index)
        {
            try
            {
                unset($this->value[$index]);
            }
            catch(Exception $e)
            {
            }
        }

        /**
         * <strong>toJSON</strong> est une méthode qui permet d'obtenir une représentation JSON du node
         * @return la représentation du node sous forme de chaine JSON
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      String json = $mcvn->toJSON(); 
         * </pre>
         */
        public function toJSON($prettyPrint=false)
        {
            if (!$this->isValid())
                return "";

            if($prettyPrint)
                return json_encode($this->toArray(), JSON_PRETTY_PRINT);
            else
                return json_encode($this->toArray());
        }

        /**
         * <strong>toString</strong> est une méthode qui permet d'obtenir une représentation textuelle du node (JSON minifié)
         * @return la représentation du node sous forme de chaine de caractère
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      String json = $mcvn->toString(); 
         * </pre>
         */
        public function toString()
        {
            return $this->toJSON(false);
        }

        



        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //



    }

?>