<?php

    namespace SicpaOpenData\Metadata;

    

    /** Classe détaillant les valeurs autorisées par nodes
     *  @author Thierry HEIRMAN
     *  @since Juillet 2021
     */
    class AuthorizedFields
    {

        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //


        /**
         * <strong>getNodeType</strong> est une méthode qui permet de récupérer le type d'un node
         * @param $node : le node pour lequel on souhaite récupérer le type
         * @return le type de node
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $nodeType = AuthorizedFields::getNodeType("author"); 
         * </pre>
         */
        public static function getNodeType($node)
        {
            switch($node)            
            {
                case "accessToSources":                     return "SimplePrimitiveNode";
                case "actionsToMinimizeLoss":               return "SimplePrimitiveNode";
                case "ageOfSource":                         return "MultiplePrimitiveNode";
                case "alternativeTitle":                    return "SimplePrimitiveNode";
                case "alternativeURL":                      return "SimplePrimitiveNode";
                case "author":                              return "MultipleCompoundNode";
                case "authorAffiliation":                   return "SimplePrimitiveNode";
                case "authorIdentifier":                    return "SimplePrimitiveNode";
                case "authorIdentifierScheme":              return "SimpleControlledVocabularyNode";
                case "authorName":                          return "SimplePrimitiveNode";
                case "bugDatabase":                         return "SimplePrimitiveNode";
                case "changes":                             return "SimplePrimitiveNode";
                case "characteristicOfSources":             return "SimplePrimitiveNode";
                case "citations":                           return "MultiplePrimitiveNode";
                case "city":                                return "SimplePrimitiveNode";
                case "cleaningOperations":                  return "SimplePrimitiveNode";
                case "collectionMode":                      return "SimpleControlledVocabularyNode";
                case "collectionModeOther":                 return "SimplePrimitiveNode";
                case "collectorTraining":                   return "SimplePrimitiveNode";
                case "conformity":                          return "MultipleCompoundNode";
                case "contributor":                         return "MultipleCompoundNode";
                case "contributorAffiliation":              return "SimplePrimitiveNode";
                case "contributorIdentifier":               return "SimplePrimitiveNode";
                case "contributorIdentifierScheme":         return "SimpleControlledVocabularyNode";
                case "contributorName":                     return "SimplePrimitiveNode";
                case "contributorType":                     return "SimpleControlledVocabularyNode";
                case "controlOperations":                   return "SimplePrimitiveNode";
                case "country":                             return "SimpleControlledVocabularyNode";
                case "dataCollectionSituation":             return "SimplePrimitiveNode";
                case "dataCollector":                       return "SimplePrimitiveNode";
                case "dataOrigin":                          return "MultipleControlledVocabularyNode";
                case "datasetContact":                      return "MultipleCompoundNode";
                case "datasetContactAffiliation":           return "SimplePrimitiveNode";
                case "datasetContactEmail":                 return "SimplePrimitiveNode";
                case "datasetContactName":                  return "SimplePrimitiveNode";
                case "datasetLevelErrorNotes":              return "SimplePrimitiveNode";
                case "dataSources":                         return "MultiplePrimitiveNode";
                case "dateOfCollection":                    return "MultipleCompoundNode";
                case "dateOfCollectionEnd":                 return "SimplePrimitiveNode";
                case "dateOfCollectionStart":               return "SimplePrimitiveNode";
                case "dateOfDeposit":                       return "SimplePrimitiveNode";
                case "degree":                              return "SimpleControlledVocabularyNode";
                case "depositor":                           return "SimplePrimitiveNode";
                case "designedForOntologyTask":             return "MultipleControlledVocabularyNode";
                case "deviationsFromSampleDesign":          return "SimplePrimitiveNode";
                case "distributionDate":                    return "SimplePrimitiveNode";
                case "distributor":                         return "MultipleCompoundNode";
                case "distributorAbbreviation":             return "SimplePrimitiveNode";
                case "distributorAffiliation":              return "SimplePrimitiveNode";
                case "distributorLogoURL":                  return "SimplePrimitiveNode";
                case "distributorName":                     return "SimplePrimitiveNode";
                case "distributorURL":                      return "SimplePrimitiveNode";
                case "dsDescription":                       return "MultipleCompoundNode";
                case "dsDescriptionDate":                   return "SimplePrimitiveNode";
                case "dsDescriptionValue":                  return "SimplePrimitiveNode";
                case "eastLongitude":                       return "SimplePrimitiveNode";
                case "endpoint":                            return "SimplePrimitiveNode";
                case "experimentNumber":                    return "MultiplePrimitiveNode";
                case "frequencyOfDataCollection":           return "SimplePrimitiveNode";
                case "geographicalReferential":             return "MultipleCompoundNode";
                case "geographicBoundingBox":               return "MultipleCompoundNode";
                case "geographicCoverage":                  return "MultipleCompoundNode";
                case "geographicUnit":                      return "MultiplePrimitiveNode";
                case "grantNumber":                         return "MultipleCompoundNode";
                case "grantNumberAgency":                   return "SimplePrimitiveNode";
                case "grantNumberValue":                    return "SimplePrimitiveNode";
                case "hasFormalityLevel":                   return "SimpleControlledVocabularyNode";
                case "hasOntologyLanguage":                 return "MultipleControlledVocabularyNode";
                case "imports":                             return "MultiplePrimitiveNode";
                case "journalArticleType":                  return "SimpleControlledVocabularyNode";
                case "journalIssue":                        return "SimplePrimitiveNode";
                case "journalPubDate":                      return "SimplePrimitiveNode";
                case "journalVolume":                       return "SimplePrimitiveNode";
                case "journalVolumeIssue":                  return "MultipleCompoundNode";
                case "keyword":                             return "MultipleCompoundNode";
                case "keywordTermURI":                      return "SimplePrimitiveNode";
                case "keywordValue":                        return "SimplePrimitiveNode";
                case "keywordVocabulary":                   return "SimplePrimitiveNode";
                case "keywordVocabularyURI":                return "SimplePrimitiveNode";
                case "kindOfData":                          return "MultipleControlledVocabularyNode";
                case "kindOfDataOther":                     return "MultiplePrimitiveNode";
                case "knownUsage":                          return "SimplePrimitiveNode";
                case "language":                            return "MultipleControlledVocabularyNode";
                case "level":                               return "SimplePrimitiveNode";
                case "lifeCycleStep":                       return "MultipleControlledVocabularyNode";
                case "lineage":                             return "MultiplePrimitiveNode";
                case "modificationDate":                    return "SimplePrimitiveNode";
                case "northLongitude":                      return "SimplePrimitiveNode";
                case "notesText":                           return "SimplePrimitiveNode";
                case "originOfSources":                     return "SimplePrimitiveNode";
                case "otherDataAppraisal":                  return "SimplePrimitiveNode";
                case "otherGeographicCoverage":             return "SimplePrimitiveNode";
                case "otherId":                             return "MultipleCompoundNode";
                case "otherIdAgency":                       return "SimplePrimitiveNode";
                case "otherIdValue":                        return "SimplePrimitiveNode";
                case "otherReferences":                     return "MultiplePrimitiveNode";
                case "priorVersion":                        return "MultiplePrimitiveNode";
                case "producer":                            return "MultipleCompoundNode";
                case "producerAbbreviation":                return "SimplePrimitiveNode";
                case "producerAffiliation":                 return "SimplePrimitiveNode";
                case "producerLogoURL":                     return "SimplePrimitiveNode";
                case "producerName":                        return "SimplePrimitiveNode";
                case "producerURL":                         return "SimplePrimitiveNode";
                case "productionDate":                      return "SimplePrimitiveNode";
                case "productionPlace":                     return "SimplePrimitiveNode";
                case "project":                             return "SimpleCompoundNode";
                case "projectAcronym":                      return "SimplePrimitiveNode";
                case "projectId":                           return "SimplePrimitiveNode";
                case "projectTask":                         return "SimplePrimitiveNode";
                case "projectTitle":                        return "SimplePrimitiveNode";
                case "projectURL":                          return "SimplePrimitiveNode";
                case "projectWorkPackage":                  return "SimplePrimitiveNode";
                case "publication":                         return "MultipleCompoundNode";
                case "publicationCitation":                 return "SimplePrimitiveNode";
                case "publicationIDNumber":                 return "SimplePrimitiveNode";
                case "publicationIDType":                   return "SimpleControlledVocabularyNode";
                case "publicationURL":                      return "SimplePrimitiveNode";
                case "qualityValidity":                     return "MultipleCompoundNode";
                case "relatedDataset":                      return "MultipleCompoundNode";
                case "relatedDatasetCitation":              return "SimplePrimitiveNode";
                case "relatedDatasetIDNumber":              return "SimplePrimitiveNode";
                case "relatedDatasetIDType":                return "SimpleControlledVocabularyNode";
                case "relatedDatasetURL":                   return "SimplePrimitiveNode";
                case "relatedMaterial":                     return "MultiplePrimitiveNode";
                case "researchInstrument":                  return "SimplePrimitiveNode";
                case "resourceVersion":                     return "SimpleCompoundNode";
                case "responseRate":                        return "SimplePrimitiveNode";
                case "samplingErrorEstimates":              return "SimplePrimitiveNode";
                case "samplingProcedure":                   return "SimpleControlledVocabularyNode";
                case "samplingProcedureOther":              return "SimplePrimitiveNode";
                case "series":                              return "SimpleCompoundNode";
                case "seriesInformation":                   return "SimplePrimitiveNode";
                case "seriesName":                          return "SimplePrimitiveNode";
                case "socialScienceNotes":                  return "SimpleCompoundNode";
                case "socialScienceNotesSubject":           return "SimplePrimitiveNode";
                case "socialScienceNotesText":              return "SimplePrimitiveNode";
                case "socialScienceNotesType":              return "SimplePrimitiveNode";
                case "software":                            return "MultipleCompoundNode";
                case "softwareName":                        return "SimplePrimitiveNode";
                case "softwareVersion":                     return "SimplePrimitiveNode";
                case "source":                              return "MultipleCompoundNode";
                case "southLongitude":                      return "SimplePrimitiveNode";
                case "spatialResolution":                   return "MultiplePrimitiveNode";
                case "specification":                       return "MultiplePrimitiveNode";
                case "state":                               return "SimplePrimitiveNode";
                case "studyAssayCellType":                  return "MultiplePrimitiveNode";
                case "studyAssayMeasurementType":           return "MultipleControlledVocabularyNode";
                case "studyAssayOrganism":                  return "MultipleControlledVocabularyNode";
                case "studyAssayOtherMeasurmentType":       return "MultiplePrimitiveNode";
                case "studyAssayOtherOrganism":             return "MultiplePrimitiveNode";
                case "studyAssayPlatform":                  return "MultipleControlledVocabularyNode";
                case "studyAssayPlatformOther":             return "MultiplePrimitiveNode";
                case "studyAssayTechnologyType":            return "MultipleControlledVocabularyNode";
                case "studyAssayTechnologyTypeOther":       return "MultiplePrimitiveNode";
                case "studyDesignType":                     return "MultipleControlledVocabularyNode";
                case "studyDesignTypeOther":                return "MultiplePrimitiveNode";
                case "studyFactorType":                     return "MultipleControlledVocabularyNode";
                case "studyFactorTypeOther":                return "MultiplePrimitiveNode";
                case "studyProtocolType":                   return "MultiplePrimitiveNode";
                case "studySampleType":                     return "MultiplePrimitiveNode";
                case "subject":                             return "MultipleControlledVocabularyNode";
                case "subtitle":                            return "SimplePrimitiveNode";
                case "targetSampleActualSize":              return "SimplePrimitiveNode";
                case "targetSampleSize":                    return "SimpleCompoundNode";
                case "targetSampleSizeFormula":             return "SimplePrimitiveNode";
                case "timeMethod":                          return "SimpleControlledVocabularyNode";
                case "timeMethodOther":                     return "SimplePrimitiveNode";
                case "timePeriodCovered":                   return "MultipleCompoundNode";
                case "timePeriodCoveredEnd":                return "SimplePrimitiveNode";
                case "timePeriodCoveredStart":              return "SimplePrimitiveNode";
                case "title":                               return "SimplePrimitiveNode";
                case "topicClassification":                 return "MultipleCompoundNode";
                case "topicClassValue":                     return "SimplePrimitiveNode";
                case "topicClassVocab":                     return "SimplePrimitiveNode";
                case "topicClassVocabURI":                  return "SimplePrimitiveNode";
                case "typeOfSource":                        return "MultiplePrimitiveNode";
                case "typeOfSR":                            return "SimpleControlledVocabularyNode";
                case "unitOfAnalysis":                      return "MultipleControlledVocabularyNode";
                case "unitOfAnalysisOther":                 return "SimplePrimitiveNode";
                case "universe":                            return "MultiplePrimitiveNode";
                case "URI":                                 return "MultiplePrimitiveNode";
                case "version":                             return "SimplePrimitiveNode";
                case "versionInfo":                         return "SimplePrimitiveNode";
                case "versionStatus":                       return "MultipleControlledVocabularyNode";
                case "weighting":                           return "SimplePrimitiveNode";
                case "westLongitude":                       return "SimplePrimitiveNode";
                default:                                    return "";
            }
        }

        /**
         * <strong>listAuthorizedFields</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le document des métadonnées
         * @return la liste des fields autorisés
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $authorizedFields = AuthorizedFields::listAuthorizedFields();
         * </pre>
         */
        public static function listAuthorizedFields()
        {
            return array_merge(
                AuthorizedFields::listAuthorizedFieldsForBiomedicalNode(),
                AuthorizedFields::listAuthorizedFieldsForCitationNode(),
                AuthorizedFields::listAuthorizedFieldsForDerivedTextNode(),
                AuthorizedFields::listAuthorizedFieldsForGeospatialNode(),
                AuthorizedFields::listAuthorizedFieldsForJournalNode(),
                AuthorizedFields::listAuthorizedFieldsForSemanticsNode(),
                AuthorizedFields::listAuthorizedFieldsForSocialScienceNode()
            );
        }

        /**
         * <strong>listAuthorizedFieldsForCategoryNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés pour un node donné
         * @param $node : le node pour lequel lister les champs autorisés
         * @return la liste des fields autorisés pour le node passé en paramètre
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $authorizedFields = AuthorizedFields::listAuthorizedFieldsForCategoryNode("citation");
         * </pre>
         */
        public static function listAuthorizedFieldsForCategoryNode($node)
        {
            switch ($node)
            {
                case "biomedical":      return AuthorizedFields::listAuthorizedFieldsForBiomedicalNode();
                case "citation":        return AuthorizedFields::listAuthorizedFieldsForCitationNode();
                case "Derived-text":    return AuthorizedFields::listAuthorizedFieldsForDerivedTextNode();
                case "geospatial":      return AuthorizedFields::listAuthorizedFieldsForGeospatialNode();
                case "journal":         return AuthorizedFields::listAuthorizedFieldsForJournalNode();
                case "semantics":       return AuthorizedFields::listAuthorizedFieldsForSemanticsNode();
                case "socialscience":   return AuthorizedFields::listAuthorizedFieldsForSocialScienceNode();
                default:                return AuthorizedFields::listAuthorizedFieldsForCompoundSubnode($node);
            }
        }

        /**
         * <strong>listAuthorizedFieldsForCompoundSubNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés pour un subnode donné
         * @param $compoundSubnode : le subnode pour lequel lister les champs autorisés
         * @return la liste des fields autorisés pour le subnode passé en paramètre
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $authorizedFields = AuthorizedFields::listAuthorizedFieldsForCompoundSubNode("author"); 
         * </pre>
         */
        public static function listAuthorizedFieldsForCompoundSubnode($compoundSubnode)
        {
            switch ($compoundSubnode)
            {
                case "author":                      
                    return array
                    (
                        "authorAffiliation"                 =>  "SimplePrimitiveNode",            
                        "authorIdentifier"                  =>  "SimplePrimitiveNode",            
                        "authorIdentifierScheme"            =>  "SimpleControlledVocabularyNode", 
                        "authorName"                        =>  "SimplePrimitiveNode",            
                    );

                case "contributor":                 
                    return array
                    (
                        "contributorAffiliation"            =>  "SimplePrimitiveNode",               
                        "contributorIdentifier"             =>  "SimplePrimitiveNode",               
                        "contributorIdentifierScheme"       =>  "SimpleControlledVocabularyNode",    
                        "contributorName"                   =>  "SimplePrimitiveNode",               
                        "contributorType"                   =>  "SimpleControlledVocabularyNode",    
                    );
                
                case "datasetContact":              
                    return array
                    (
                        "datasetContactAffiliation"         =>  "SimplePrimitiveNode",
                        "datasetContactEmail"               =>  "SimplePrimitiveNode",
                        "datasetContactName"                =>  "SimplePrimitiveNode",
                    );
                
                case "dateOfCollection":            
                    return array
                    (
                        "dateOfCollectionStart"             =>  "SimplePrimitiveNode",
                        "dateOfCollectionEnd"               =>  "SimplePrimitiveNode",
                    );
                
                case "distributor":                 
                    return array
                    (
                        "distributorAbbreviation"           =>  "SimplePrimitiveNode",
                        "distributorAffiliation"            =>  "SimplePrimitiveNode",
                        "distributorLogoURL"                =>  "SimplePrimitiveNode",
                        "distributorName"                   =>  "SimplePrimitiveNode",
                        "distributorURL"                    =>  "SimplePrimitiveNode",
                    );
                
                case "dsDescription":               
                    return array
                    (
                        "dsDescriptionDate"                 =>  "SimplePrimitiveNode",
                        "dsDescriptionValue"                =>  "SimplePrimitiveNode",
                    );
                
                case "grantNumber":                 
                    return array
                    (
                        "grantNumberAgency"                 =>  "SimplePrimitiveNode",
                        "grantNumberValue"                  =>  "SimplePrimitiveNode",
                    );
                
                case "keyword":                     
                    return array
                    (
                        "keywordTermURI"                    =>  "SimplePrimitiveNode",
                        "keywordValue"                      =>  "SimplePrimitiveNode",
                        "keywordVocabulary"                 =>  "SimplePrimitiveNode",
                        "keywordVocabularyURI"              =>  "SimplePrimitiveNode",
                    );
                
                case "otherId":                     
                    return array
                    (
                        "otherIdAgency"                     =>  "SimplePrimitiveNode",
                        "otherIdValue"                      =>  "SimplePrimitiveNode",
                    );
                
                case "producer":                    
                    return array
                    (
                        "producerAbbreviation"              =>  "SimplePrimitiveNode",
                        "producerAffiliation"               =>  "SimplePrimitiveNode",
                        "producerLogoURL"                   =>  "SimplePrimitiveNode",
                        "producerName"                      =>  "SimplePrimitiveNode",
                        "producerURL"                       =>  "SimplePrimitiveNode",
                    );
                
                case "project":                     
                    return array
                    (
                        "projectAcronym"                    =>  "SimplePrimitiveNode",
                        "projectId"                         =>  "SimplePrimitiveNode",
                        "projectTask"                       =>  "SimplePrimitiveNode",
                        "projectTitle"                      =>  "SimplePrimitiveNode",
                        "projectURL"                        =>  "SimplePrimitiveNode",
                        "projectWorkPackage"                =>  "SimplePrimitiveNode",
                    );
                
                case "publication":                 
                    return array
                    (
                        "publicationCitation"               =>  "SimplePrimitiveNode",
                        "publicationIDNumber"               =>  "SimplePrimitiveNode",
                        "publicationIDType"                 =>  "SimpleControlledVocabularyNode",
                        "publicationURL"                    =>  "SimplePrimitiveNode",
                    );
                
                case "relatedDataset":              
                    return array
                    (
                        "relatedDatasetCitation"            =>  "SimplePrimitiveNode",
                        "relatedDatasetIDNumber"            =>  "SimplePrimitiveNode",
                        "relatedDatasetIDType"              =>  "SimpleControlledVocabularyNode",
                        "relatedDatasetURL"                 =>  "SimplePrimitiveNode",
                    );
                
                case "series":                      
                    return array
                    (
                        "seriesInformation"                 =>  "SimplePrimitiveNode",
                        "seriesName"                        =>  "SimplePrimitiveNode",
                    );
                
                case "software":                    
                    return array
                    (
                        "softwareName"                      =>  "SimplePrimitiveNode",
                        "softwareVersion"                   =>  "SimplePrimitiveNode",
                    );
                
                case "timePeriodCovered":           
                    return array
                    (
                        "timePeriodCoveredStart"            =>  "SimplePrimitiveNode",
                        "timePeriodCoveredEnd"              =>  "SimplePrimitiveNode",
                    );
                
                case "topicClassification":         
                    return array
                    (
                        "topicClassValue"                   =>  "SimplePrimitiveNode",
                        "topicClassVocab"                   =>  "SimplePrimitiveNode",
                        "topicClassVocabURI"                =>  "SimplePrimitiveNode",
                    );
                
                case "conformity":                  
                    return array
                    (
                        "degree"                            =>  "SimpleControlledVocabularyNode",
                        "specification"                     =>  "SimplePrimitiveNode",
                    );
                
                case "geographicBoundingBox":       
                    return array
                    (
                        "eastLongitude"                     =>  "SimplePrimitiveNode",
                        "northLongitude"                    =>  "SimplePrimitiveNode",
                        "southLongitude"                    =>  "SimplePrimitiveNode",
                        "westLongitude"                     =>  "SimplePrimitiveNode",
                    );
                
                case "geographicCoverage":          
                    return array
                    (
                        "city"                              =>  "SimplePrimitiveNode",
                        "country"                           =>  "SimpleControlledVocabularyNode",
                        "otherGeographicCoverage"           =>  "SimplePrimitiveNode",
                        "state"                             =>  "SimplePrimitiveNode",
                    );
                
                case "qualityValidity":             
                    return array
                    (
                        "lineage"                           =>  "MultiplePrimitiveNode",
                        "spatialResolution"                 =>  "MultiplePrimitiveNode",
                    );
                
                case "geographicalReferential":     
                    return array
                    (
                        "level"                             =>  "SimplePrimitiveNode",
                        "version"                           =>  "SimplePrimitiveNode",
                    );
                
                case "socialScienceNotes":          
                    return array
                    (
                        "socialScienceNotesSubject"         =>  "SimplePrimitiveNode",
                        "socialScienceNotesText"            =>  "SimplePrimitiveNode",
                        "socialScienceNotesType"            =>  "SimplePrimitiveNode",
                    );
                
                case "targetSampleSize":            
                    return array
                    (
                        "targetSampleActualSize"            =>  "SimplePrimitiveNode",
                        "targetSampleSizeFormula"           =>  "SimplePrimitiveNode",
                    );
                
                case "journalVolumeIssue":          
                    return array
                    (
                        "journalIssue"                      =>  "SimplePrimitiveNode",
                        "journalPubDate"                    =>  "SimplePrimitiveNode",
                        "journalVolume"                     =>  "SimplePrimitiveNode",
                    );
                
                case "source":                      
                    return array
                    (
                        "ageOfSource"                       =>  "MultiplePrimitiveNode",
                        "citations"                         =>  "MultiplePrimitiveNode",
                        "experimentNumber"                  =>  "MultiplePrimitiveNode",
                        "typeOfSource"                      =>  "MultiplePrimitiveNode",
                    );
                
                case "resourceVersion":             
                    return array
                    (
                        "changes"                           =>  "SimplePrimitiveNode",
                        "modificationDate"                  =>  "SimplePrimitiveNode",
                        "priorVersion"                      =>  "MultiplePrimitiveNode",
                        "versionInfo"                       =>  "SimplePrimitiveNode",
                        "versionStatus"                     =>  "MultipleControlledVocabularyNode",
                    );
                
                default:                            
                    return array();
            }
        }

        /**
         * <strong>listAuthorizedFieldsForBiomedicalNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "biomedical"
         * @return la liste des fields autorisés
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $authorizedFields = AuthorizedFields::listAuthorizedFieldsForBiomedicalNode(); 
         * </pre>
         */
        public static function listAuthorizedFieldsForBiomedicalNode()
        {
            return array
            (
                "studyAssayCellType"                =>  "MultiplePrimitiveNode",
                "studyAssayMeasurementType"         =>  "MultipleControlledVocabularyNode",
                "studyAssayOrganism"                =>  "MultipleControlledVocabularyNode",
                "studyAssayOtherMeasurmentType"     =>  "MultiplePrimitiveNode",
                "studyAssayOtherOrganism"           =>  "MultiplePrimitiveNode",
                "studyAssayPlatform"                =>  "MultipleControlledVocabularyNode",
                "studyAssayPlatformOther"           =>  "MultiplePrimitiveNode",
                "studyAssayTechnologyType"          =>  "MultipleControlledVocabularyNode",
                "studyAssayTechnologyTypeOther"     =>  "MultiplePrimitiveNode",
                "studyDesignType"                   =>  "MultipleControlledVocabularyNode",
                "studyDesignTypeOther"              =>  "MultiplePrimitiveNode",
                "studyFactorType"                   =>  "MultipleControlledVocabularyNode",
                "studyFactorTypeOther"              =>  "MultiplePrimitiveNode",
                "studyProtocolType"                 =>  "MultiplePrimitiveNode",
                "studySampleType"                   =>  "MultiplePrimitiveNode",
            );
        }

        /**
         * <strong>listAuthorizedFieldsForCitationNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "citation"
         * @return la liste des fields autorisés
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $authorizedFields = AuthorizedFields::listAuthorizedFieldsForCitationNode(); 
         * </pre>
         */
        public static function listAuthorizedFieldsForCitationNode()
        {
            return array
            (
                "accessToSources"               =>  "SimplePrimitiveNode",
                "alternativeTitle"              =>  "SimplePrimitiveNode",
                "alternativeURL"                =>  "SimplePrimitiveNode",
                "author"                        =>  "MultipleCompoundNode",
                "characteristicOfSources"       =>  "SimplePrimitiveNode",
                "contributor"                   =>  "MultipleCompoundNode",
                "dataOrigin"                    =>  "MultipleControlledVocabularyNode",
                "datasetContact"                =>  "MultipleCompoundNode",
                "dataSources"                   =>  "MultiplePrimitiveNode",
                "dateOfCollection"              =>  "MultipleCompoundNode",
                "dateOfDeposit"                 =>  "SimplePrimitiveNode",
                "depositor"                     =>  "SimplePrimitiveNode",
                "distributionDate"              =>  "SimplePrimitiveNode",
                "distributor"                   =>  "MultipleCompoundNode",
                "dsDescription"                 =>  "MultipleCompoundNode",
                "grantNumber"                   =>  "MultipleCompoundNode",
                "keyword"                       =>  "MultipleCompoundNode",
                "kindOfData"                    =>  "MultipleControlledVocabularyNode",
                "kindOfDataOther"               =>  "MultiplePrimitiveNode",
                "language"                      =>  "MultipleControlledVocabularyNode",
                "lifeCycleStep"                 =>  "MultipleControlledVocabularyNode",
                "notesText"                     =>  "SimplePrimitiveNode",
                "originOfSources"               =>  "SimplePrimitiveNode",
                "otherId"                       =>  "MultipleCompoundNode",
                "otherReferences"               =>  "MultiplePrimitiveNode",
                "producer"                      =>  "MultipleCompoundNode",
                "productionDate"                =>  "SimplePrimitiveNode",
                "productionPlace"               =>  "SimplePrimitiveNode",
                "project"                       =>  "SimpleCompoundNode",
                "publication"                   =>  "MultipleCompoundNode",
                "relatedDataset"                =>  "MultipleCompoundNode",
                "relatedMaterial"               =>  "MultiplePrimitiveNode",
                "series"                        =>  "SimpleCompoundNode",
                "software"                      =>  "MultipleCompoundNode",
                "subject"                       =>  "MultipleControlledVocabularyNode",
                "subtitle"                      =>  "SimplePrimitiveNode",
                "timePeriodCovered"             =>  "MultipleCompoundNode",
                "title"                         =>  "SimplePrimitiveNode",
                "topicClassification"           =>  "MultiplePrimitiveNode",
            );
        }

        /**
         * <strong>listAuthorizedFieldsForDerivedTextNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "Derived-text"
         * @return la liste des fields autorisés
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $authorizedFields = AuthorizedFields::listAuthorizedFieldsForDerivedTextNode(); 
         * </pre>
         */
        public static function listAuthorizedFieldsForDerivedTextNode()
        {
            return array
            (
                "source"    => "MultipleCompoundNode",
            );
        }

        /**
         * <strong>listAuthorizedFieldsForGeospatialNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "geospatial"
         * @return la liste des fields autorisés
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $authorizedFields = AuthorizedFields::listAuthorizedFieldsForGeospatialNode(); 
         * </pre>
         */
        public static function listAuthorizedFieldsForGeospatialNode()
        {
            return array
            (
                "conformity"                =>  "MultipleCompoundNode",
                "geographicBoundingBox"     =>  "MultipleCompoundNode",
                "geographicCoverage"        =>  "MultipleCompoundNode",
                "geographicUnit"            =>  "MultiplePrimitiveNode",
                "qualityValidity"           =>  "MultipleCompoundNode",
            );
        }

        /**
         * <strong>listAuthorizedFieldsForJournalNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "journal"
         * @return la liste des fields autorisés
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $authorizedFields = AuthorizedFields::listAuthorizedFieldsForJournalNode(); 
         * </pre>
         */
        public static function listAuthorizedFieldsForJournalNode()
        {
            return array(
                "journalArticleType"    =>  "SimpleControlledVocabularyNode",
                "journalVolumeIssue"    =>  "MultipleCompoundNode",
            );
        }

        /**
         * <strong>listAuthorizedFieldsForSemanticsNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "semantics"
         * @return la liste des fields autorisés
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $authorizedFields = AuthorizedFields::listAuthorizedFieldsForSemanticsNode(); 
         * </pre>
         */
        public static function listAuthorizedFieldsForSemanticsNode()
        {
            return array
            (
                "bugDatabase"                   =>  "SimplePrimitiveNode",
                "designedForOntologyTask"       =>  "MultipleControlledVocabularyNode",
                "endpoint"                      =>  "SimplePrimitiveNode",
                "hasFormalityLevel"             =>  "SimpleControlledVocabularyNode",
                "hasOntologyLanguage"           =>  "MultipleControlledVocabularyNode",
                "knownUsage"                    =>  "SimplePrimitiveNode",
                "imports"                       =>  "MultiplePrimitiveNode",
                "resourceVersion"               =>  "SimpleCompoundNode",
                "typeOfSR"                      =>  "SimpleControlledVocabularyNode",
                "URI"                           =>  "MultiplePrimitiveNode",
            );
        }

        /**
         * <strong>listAuthorizedFieldsForSocialScienceNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "socialscience"
         * @return la liste des fields autorisés
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $authorizedFields = AuthorizedFields::listAuthorizedFieldsForSocialScienceNode(); 
         * </pre>
         */
        public static function listAuthorizedFieldsForSocialScienceNode()
        {
            return array
            (
                "actionsToMinimizeLoss"         =>  "SimplePrimitiveNode",
                "cleaningOperations"            =>  "SimplePrimitiveNode",
                "collectionMode"                =>  "SimpleControlledVocabularyNode",
                "collectionModeOther"           =>  "SimplePrimitiveNode",
                "collectorTraining"             =>  "SimplePrimitiveNode",
                "controlOperations"             =>  "SimplePrimitiveNode",
                "dataCollectionSituation"       =>  "SimplePrimitiveNode",
                "dataCollector"                 =>  "SimplePrimitiveNode",
                "datasetLevelErrorNotes"        =>  "SimplePrimitiveNode",
                "deviationsFromSampleDesign"    =>  "SimplePrimitiveNode",
                "frequencyOfDataCollection"     =>  "SimplePrimitiveNode",
                "geographicalReferential"       =>  "MultipleCompoundNode",
                "otherDataAppraisal"            =>  "SimplePrimitiveNode",
                "researchInstrument"            =>  "SimplePrimitiveNode",
                "responseRate"                  =>  "SimplePrimitiveNode",
                "samplingErrorEstimates"        =>  "SimplePrimitiveNode",
                "samplingProcedure"             =>  "SimpleControlledVocabularyNode",
                "samplingProcedureOther"        =>  "SimplePrimitiveNode",
                "socialScienceNotes"            =>  "SimpleCompoundNode",
                "targetSampleSize"              =>  "SimpleCompoundNode",
                "timeMethod"                    =>  "SimpleControlledVocabularyNode",
                "timeMethodOther"               =>  "SimplePrimitiveNode",
                "unitOfAnalysis"                =>  "MultipleControlledVocabularyNode",
                "unitOfAnalysisOther"           =>  "SimplePrimitiveNode",
                "universe"                      =>  "MultiplePrimitiveNode",
                "weighting"                     =>  "SimpleCompoundNode",
            );
        }
    }

?>