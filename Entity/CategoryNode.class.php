<?php

    namespace SicpaOpenData\Metadata;
    use SicpaOpenData\Helper as Helper;




    /** Classe implémentant les nodes catégorie
     *  @author Thierry HEIRMAN
     *  @since Juillet 2021
     */
    class CategoryNode
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  

        /** Cette propriété indique la valeur à afficher dans le fichier de métadonnées pour ce node */
        protected $displayName;

        /** Cette propriété permet de gérer la liste des valeurs pour le node */
        protected $fields;


        
        
        
        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          

        /**
         * Constructeur 
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $node = new CategoryNode(); 
         * </pre>
         */
        public function __construct()
        {
            $this->setDisplayName("");
            $this->initFields();
        }


        
        
        
        //     ___  ____________________________  _____  ____
        //    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
        //   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
        //  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
        //

        /**
         * Permet d'obtenir la valeur de l'attribut <strong>displayName</strong> 
         * @return valeur de l'attribut <strong>displayName</strong> 
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *     $displayName = $node->getDisplayName(); 
         * </pre>
         */
        public function getDisplayName()
        {
            return $this->displayName;
        }
        
        /**
         * Permet d'obtenir la valeur de l'attribut <strong>fields</strong> 
         * @return valeur de l'attribut <strong>fields</strong> 
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $fields = $node->getFields(); 
         * </pre>
         */
        public function getFields()
        {
            return $this->fields;
        }
        
        /**
         * Permet de mettre à jour la valeur de l'attribut <strong>displayName</strong> 
         * @param $displayName   : valeur à enregistrer dans l'attribut <strong>fields</strong> 
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $node->setDisplayName("displayName"); 
         * </pre>
         */
        protected function setDisplayName($displayName)
        {
            $this->displayName = $displayName;
        }

        

           
        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             

        /**
         * <strong>addField</strong> est une méthode qui permet d'ajouter un noeud de type MultipleCompoundNode à la liste des fields du node
         * @param $node : node à ajouter 
         * @see MultipleCompoundNode
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $mcn  = new MultipleCompoundNode();
         *      $mcvn = new MultipleControlledVocabularyNode();
         *      $mpn  = new MultiplePrimitiveNode();
         *      $scn  = new SimpleCompoundNode();
         *      $scvn = new SimpleControlledVocabularyNode();
         *      $spn  = new SimplePrimitiveNode();
         *       
         *      $bn = new BiomedicalNode();
         *      $bn->addField($mcn);
         *      $bn->addField($mcvn);
         *      $bn->addField($mpn);
         *      $bn->addField($scn);
         *      $bn->addField($scvn);
         *      $bn->addField($spn);
         * </pre>
         */        
        public function addField($node)
        {
            switch(get_class($node))
            {
                case "SicpaOpenData\Metadata\MultipleCompoundNode":
                    return $this->addMultipleCompoundNodeField($node);

                case "SicpaOpenData\Metadata\MultipleControlledVocabularyNode":
                    return $this->addMultipleControlledVocabularyNodeField($node);

                case "SicpaOpenData\Metadata\MultiplePrimitiveNode":
                    return $this->addMultiplePrimitiveNodeField($node);

                case "SicpaOpenData\Metadata\SimpleCompoundNode":
                    return $this->addSimpleCompoundNodeField($node);

                case "SicpaOpenData\Metadata\SimpleControlledVocabularyNode":
                    return $this->addSimpleControlledVocabularyNodeField($node);

                case "SicpaOpenData\Metadata\SimplePrimitiveNode":
                    return $this->addSimplePrimitiveNodeField($node);

                default:
                    return false;
            }
        }

        /**
         * <strong>clearFields</strong> est une méthode qui permet de réinitialiser la liste des fields du node
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $node->clearFields();
         * </pre>
         */
        public function clearFields()
        {
            $this->initFields();
        }

        /**
         * <strong>containsField</strong> est une méthode qui permet de vérifier si un field existe dans la liste des fields du node
         * @param $fieldName : valeur à vérifier
         * @return true si la valeur appartient à la liste de valeurs du node, false sinon
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $contains =  $node->containsFields("fieldName");
         * </pre>
         */
        public function containsField($fieldName)
        {
            return $this->indexOfField($fieldName) >= 0;
        }

        /**
         * <strong>fieldNames</strong> est une méthode qui permet de récupérer la liste des noms des fields du node
         * @return la liste des noms des fields
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $fnList = $node->fieldNames();
         * </pre>
         */
        public function fieldNames()
        {
            $fieldNames = array();

            foreach ($this->getFields() as $node)
                array_push($fieldNames, $node->getTypeName());

            return $fieldNames;
        }

        /**
         * <strong>indexOfField</strong> est une méthode qui permet de récupérer l'index d'un field
         * @param $fieldName : valeur à rechercher
         * @return index de la valeur
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $index = $node->indexOfField("fieldName");
         * </pre>
         */
        public function indexOfField($fieldName)
        {
            for($index=0; $index < count($this->fields); $index++)
            {
                $node = $this->fields[$index];

                if ($node->getTypeName() == $fieldName)
                    return $index;
            }

            return -1;
        }

        /**
         * <strong>initField</strong> est une méthode qui permet d'initialiser la liste des fields du node
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $node->initFields();
         * </pre>
         */
        public function initFields()
        {
            $this->fields = array();
        }

        /**
         * <strong>removeField</strong> est une méthode qui permet de supprimer un field de la liste des fields
         * @param $fieldName : valeur à supprimer
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $node->removeField("fieldName");
         * </pre>
         */
        public function removeField($fieldName)
        {
            try
            {
                $index = $this->indexOfField($fieldName);
                $this->removeFieldAt($index);
            }
            catch(Exception $e)
            {
            }
        }

        /**
         * <strong>removeFieldAt</strong> est une méthode qui permet de supprimer le field située à un index précis
         * @param $index     : index de la valeur à supprimer
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $node->removeFieldAt(3);
         * </pre>
         */
        public function removeFieldAt($index)
        {
            try
            {
                unset($this->fields[$index]);
            }
            catch(Exception $e)
            {
            }
        }

        /**
         * <strong>removeAllFields</strong> est une méthode qui permet de supprimer tous les fields de la liste des fields
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $node->removeAllFields();
         * </pre>
         */
        public function removeAllFields()
        {
            $this->initFields();
        }

        /**
         * <strong>toArray</strong> est une méthode qui permet d'obtenir le node sous forme d'un tableau
         * @return la représentation du node sous forme d'un tableau
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $array = $node->toArray(); 
         * </pre>
         */
        public function toArray()
        {
            $fieldArray = array();

            foreach ($this->getFields() as $field) 
                array_push($fieldArray, $field->toArray());

            return array(
                            "displayName"   => $this->getDisplayName(),
                            "fields"        => $fieldArray
                        );
        }

        /**
         * <strong>toJSON</strong> est une méthode qui permet d'obtenir une représentation JSON du node
         * @return la représentation du node sous forme de chaine JSON
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      String json = $node->toJSON(); 
         * </pre>
         */
        public function toJSON($prettyPrint=false)
        {
            if (!$this->isValid())
                return "";

            if($prettyPrint)
                return json_encode($this->toArray(), JSON_PRETTY_PRINT);
            else
                return json_encode($this->toArray());
        }

        /**
         * <strong>toString</strong> est une méthode qui permet d'obtenir une représentation textuelle du node (JSON minifié)
         * @return la représentation du node sous forme de chaine de caractère
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      String json = $node->toString(); 
         * </pre>
         */
        public function toString()
        {
            return $this->toJSON(false);
        }




        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //




    }

?>