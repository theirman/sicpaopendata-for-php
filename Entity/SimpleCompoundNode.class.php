<?php

    namespace SicpaOpenData\Metadata;
    use SicpaOpenData\Helper as Helper;

    require_once("Entity\AuthorizedFields.class.php");
    require_once("Entity\BaseNode.class.php");
    require_once("Entity\RequiredFields.class.php");
    require_once("Helper\helper.php");



    /** Classe implémentant les nodes à valeur composée unique
     *  @author Thierry HEIRMAN
     *  @since Juillet 2021
     */
    class SimpleCompoundNode extends BaseNode
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  

        /** Cette propriété contient la valeur du node */
        private $value;





        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          

        /**
         * Constructeur
         * @param $typeName :   nom du node
         * @param $value    :   valeur du node
         * @see SimplePrimitiveNode
         * @see SimpleControlledVocabularyNode
         * @see MultiplePrimitiveNode
         * @see MultipleControlledVocabularyNode
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $arr = array(
         *                      "field1"    => new SimplePrimitiveNode("field1", "value1"),
         *                      "field2"    => new SimpleControlledVocabularyNode("field2", "value2"),
         *                      "field3"    => new SimplePrimitiveNode("field3", "value3"),
         *                  );
         *          
         *      $scn = new SimpleCompoundNode("name", $arr);
         * </pre>
         */
        public function __construct($typeName="", $value=array())
        {
            $this->setMultiple(false);
            $this->setTypeClass("compound");
            $this->setTypeName($typeName);
            $this->setValue($value);
        }


        
        
        
        //     ___  ____________________________  _____  ____
        //    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
        //   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
        //  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
        //

        /**
         * Permet d'obtenir la valeur de l'attribut <strong>value</strong> 
         * @return valeur de l'attribut <strong>value</strong> 
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $value = $scn->getValue(); 
         * </pre>
         */
        public function getValue()
        {
            return $this->value;
        }
        
        /**
         * Permet de mettre à jour la valeur de l'attribut <strong>value</strong>
         * @param $value : valeur à enregistrer dans l'attribut <strong>value</strong>
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $arr = array(
         *                      "field1"    => new SimplePrimitiveNode("field1", "value1"),
         *                      "field2"    => new SimpleControlledVocabularyNode("field2", "value2"),
         *                      "field3"    => new SimplePrimitiveNode("field3", "value3"),
         *                  );
         *          
         *      $scn->setValue($arr);
         * </pre>
         */
        public function setValue($value)
        {
            $this->value = $value;

            if (!$this->isValueValid())
            {
                $this->value = array();
                return;
            }

            if (!$this->areSubnodesValid())
            {
                $this->value = array();
                return;
            }
        }
        
        
        
        
        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             

        /**
         * <strong>AreSubnodesValid</strong> est une méthode qui permet de s'assurer de la validité de tous les subnodes 
         * @return true si les subnodes sont valides, false sinon
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      if($scn->AreSubnodesValid())
         *      {
         *          ...
         *      }
         * </pre>
         */
        private function areSubnodesValid()
        {
            if(!is_array($this->getValue()))
                return false;

            foreach($this->getValue() as $key=>$subnode)
            {
                if ( !array_key_exists($subnode->getTypeName(), AuthorizedFields::listAuthorizedFieldsForCategoryNode($this->getTypeName())) )
                    return false;

                if(!Helper::contains(get_class($subnode), AuthorizedFields::getNodeType($subnode->getTypeName())))
                    return false;
            }

            return true;
        }

        /**
         * <strong>isValid</strong> est une méthode qui permet de s'assurer de la validité du node
         * @return true si le node est valide, false sinon
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      if($scn->isValid())
         *      {
         *          ...
         *      }
         * </pre>
         */
        public function isValid()
        {
            return is_bool($this->getMultiple())        && !$this->getMultiple()
                && is_string($this->getTypeClass())     &&  $this->getTypeClass() == "compound"
                && is_string($this->getTypeName())  
                && is_array($this->getValue())         && $this->isValueValid()
                && $this->areSubnodesValid();
        }

        /**
         * <strong>isValueValid</strong> est une méthode qui permet de s'assurer de la validité de la valeur <br/>
         * @return true si la valeur est valide, false sinon
         * @see SimplePrimitiveNode
         * @see SimpleControlledVocabularyNode
         * @see MultiplePrimitiveNode
         * @see MultipleControlledVocabularyNode
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      if($this->isValueValid())
         *      {
         *          ...
         *      }
         * </pre>
         */
        private function isValueValid()
        {
            if(!is_array($this->getValue()))
                return false;

            foreach($this->getValue() as $node)
            {
                if ($node instanceof SimplePrimitiveNode
                    || $node instanceof SimpleControlledVocabularyNode
                    || $node instanceof MultiplePrimitiveNode
                    || $node instanceof MultipleControlledVocabularyNode
                   )
                    continue;
                else
                    return false;
            }

            return true;
        }

        /**
         * <strong>toArray</strong> est une méthode qui permet d'obtenir le node sous forme d'un tableau
         * @return la représentation du node sous forme d'un tableau
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $array = $this->toArray(); 
         * </pre>
         */
        public function toArray()
        {
            $valueArray = array();

            foreach ($this->value as $value) 
            {
                $valueArray[$value->getTypeName()]["multiple"]     = $value->getMultiple();
                $valueArray[$value->getTypeName()]["typeClass"]    = $value->getTypeClass();
                $valueArray[$value->getTypeName()]["typeName"]     = $value->getTypeName();
                $valueArray[$value->getTypeName()]["value"]        = $value->getValue();
            }

            return array(
                            "multiple"  => $this->getMultiple(),
                            "typeClass" => $this->getTypeClass(),
                            "typeName"  => $this->getTypeName(),
                            "value"     => $valueArray
                        );
        }

        /**
         * <strong>toJSON</strong> est une méthode qui permet d'obtenir une représentation JSON du node
         * @param $ $prettyPrint : indique si l'on doit adapter la chaine JSON à la lecture par l'humain
         * @return la représentation du node sous forme de chaine JSON
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $json = $scn->toJSON(); 
         * </pre>
         */
        public function toJSON($prettyPrint=false)
        {
            if (!$this->isValid())
                return "";

            if($prettyPrint)
                return json_encode($this->toArray(), JSON_PRETTY_PRINT);
            else
                return json_encode($this->toArray());
        }

        /**
         * <strong>toString</strong> est une méthode qui permet d'obtenir une représentation textuelle du node (JSON minifié)
         * @return la représentation du node sous forme de chaine de caractère
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $json = $scn->toString(); 
         * </pre>
         */
        public function toString()
        {
            return $this->toJSON(false);
        }

        
        
        
        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //



    }

?>