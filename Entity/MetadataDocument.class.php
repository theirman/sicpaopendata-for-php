<?php

    namespace SicpaOpenData\Metadata;



	/** Classe implémentant la construction du document JSON des métadonnées
	 *  @author Thierry HEIRMAN
	 *  @since Juin 2021
	 */
	class MetadataDocument
	{
	    //    ___ _______________  _______  __  ____________
	    //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
	    //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
	    // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
	    //                                                  

	    /** Cette propriété indique la licence du dataset */
	    private $license;

	    /** Cette propriété indique les termes d'utilisation du dataset */
	    private $termsOfUse;

	    /** Cette propriété permet de gérer la liste des category nodes du document */
	    private $metadataBlocks;




	    //   _________  _  _______________  __  ___________________  _____  ____
	    //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
	    // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
	    // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
	    //                                                                          

	    /**
	     * Constructeur nécessitant deux paramètres : license et termsOfUse
	     * @param $license : valeur à enregistrer dans l'attribut <strong>license</strong>
	     * @param $termsOfUse : valeur à enregistrer dans l'attribut <strong>termsOfUse</strong>
	     * 
	     * <hr>
	     * <strong>Exemple : </strong>
	     * <pre>
	     *      $md = new MetadataDocument("license", "termsOfUse");
	     * </pre>
	     */
	    public function __construct($license="NONE", $termsOfUse="")
	    {
	    	if(empty($termsOfUse))
	    		$termsOfUse = "<img src='https://www.etalab.gouv.fr/wp-content/uploads/2011/10/licence-ouverte-open-licence.gif' alt='Licence Ouverte' height='100'>" .
	                          "<a href='https://www.etalab.gouv.fr/licence-ouverte-open-licence'>Licence Ouverte / Open Licence Version 2.0</a>" .
	                          "compatible CC BY";

	        $this->setLicense($license);
	        $this->setTermsOfUse($termsOfUse);
	        
	        $this->metadataBlocks = array();
	    }


	    
	    
	    
	    //     ___  ____________________________  _____  ____
	    //    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
	    //   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
	    //  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
	    //

	    /**
	     * Permet d'obtenir la valeur de l'attribut <strong>license</strong> 
	     * @return valeur de l'attribut <strong>license</strong> 
	     * 
	     * <hr>
	     * <strong>Exemple : </strong>
	     * <pre>
	     *      $license = $md->getLicense();
	     * </pre>
	     */
	    public function getLicense()
	    {
	        return $this->license;
	    }

	    /**
	     * Permet d'obtenir la valeur de l'attribut <strong>metadataBlocks</strong> 
	     * @return valeur de l'attribut <strong>metadataBlocks</strong> 
	     * 
	     * <hr>
	     * <strong>Exemple : </strong>
	     * <pre>
	     *      $metadataBlocks = $md->getMetadataBlocks();
	     * </pre>
	     */
	    public function getMetadataBlocks()
	    {
	        return $this->metadataBlocks;
	    }

	    /**
	     * Permet d'obtenir la valeur de l'attribut <strong>termsOfUse</strong> 
	     * @return valeur de l'attribut <strong>termsOfUse</strong> 
	     * 
	     * <hr>
	     * <strong>Exemple : </strong>
	     * <pre>
	     *      $value = $md->getTermsOfUse();
	     * </pre>
	     */
	    public function getTermsOfUse()
	    {
	        return $this->termsOfUse;
	    }

	    /**
	     * Permet de mettre à jour la valeur de l'attribut <strong>license</strong>
	     * @param $license : valeur à enregistrer dans l'attribut <strong>license</strong>
	     * 
	     * <hr>
	     * <strong>Exemple : </strong>
	     * <pre>
	     *      $md->setLicense("license"); 
	     * </pre>
	     */
	    public function setLicense($license)
	    {
	        $this->license = $license;
	    }

	    /**
	     * Permet de mettre à jour la valeur de l'attribut <strong>termsOfUse</strong>
	     * @param $termsOfUse : valeur à enregistrer dans l'attribut <strong>termsOfUse</strong>
	     * 
	     * <hr>
	     * <strong>Exemple : </strong>
	     * <pre>
	     *      $md->setTermsOfUse("termsOfUse"); 
	     * </pre>
	     */
	    public function setTermsOfUse($termsOfUse)
	    {
	        $this->termsOfUse = $termsOfUse;
	    }


	    
	    
	    
	    //    __  _______________ ______  ___  ________
	    //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
	    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
	    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
	    //                                             


	    /**
	     * <strong>addBiomedicalNode</strong> est une méthode qui permet d'ajouter le node biomedical à la liste des category nodes
	     * @param $biomedicalNode : la valeur du node biomedical
	     * 
	     * <hr>
	     * <strong>Exemple : </strong>
	     * <pre>
	     *     $bn = new BiomedicalNode();
	     *     ...
	     *     ...
	     *     
	     *     $md->addBiomedicalNode($bn);
	     * </pre>
	     */
	    public function addBiomedicalNode($biomedicalNode)
	    {
	    	if(is_array($biomedicalNode))
	        	$this->metadataBlocks["biomedical"] = $biomedicalNode;
	        else
	        	$this->metadataBlocks["biomedical"] = $biomedicalNode->toArray();
	    }

	    /**
	     * <strong>addCitationNode</strong> est une méthode qui permet d'ajouter le node citation à la liste des category nodes
	     * @param $citationNode : la valeur du node citationNode
	     * 
	     * <hr>
	     * <strong>Exemple : </strong>
	     * <pre>
	     *     $cn = new CitationNode();
	     *     ...
	     *     ...
	     *     
	     *     $md->addCitationNode($cn);
	     * </pre>
	     */
	    public function addCitationNode($citationNode)
	    {
	    	if(is_array($citationNode))
	        	$this->metadataBlocks["citation"] = $citationNode;
	        else
	        	$this->metadataBlocks["citation"] = $citationNode->toArray();
	    }

	    /**
	     * <strong>addDerivedTextNode</strong> est une méthode qui permet d'ajouter le node derivedText à la liste des category nodes
	     * @param $derivedTextNode : la valeur du node derivedText
	     * 
	     * <hr>
	     * <strong>Exemple : </strong>
	     * <pre>
	     *     $dtn = new DerivedTextNode();
	     *     ...
	     *     ...
	     *     
	     *     $md->addDerivedTextNode($dtn);
	     * </pre>
	     */
	    public function addDerivedTextNode($derivedTextNode)
	    {
	    	if(is_array($derivedTextNode))
	        	$this->metadataBlocks["Derived-text"] = $derivedTextNode;
	        else
	        	$this->metadataBlocks["Derived-text"] = $derivedTextNode->toArray();
	    }

	    /**
	     * <strong>addGeospatialNode</strong> est une méthode qui permet d'ajouter le node geospatial à la liste des category nodes
	     * @param $geospatialNode : la valeur du node geospatial
	     * 
	     * <hr>
	     * <strong>Exemple : </strong>
	     * <pre>
	     *     $gn = new GeospatialNode();
	     *     ...
	     *     ...
	     *     
	     *     $md->addGeospatialNode($gn);
	     * </pre>
	     */
	    public function addGeospatialNode($geospatialNode)
	    {
	    	if(is_array($geospatialNode))
	        	$this->metadataBlocks["geospatial"] = $geospatialNode;
	        else
	        	$this->metadataBlocks["geospatial"] = $geospatialNode->toArray();
	    }

	    /**
	     * <strong>addJournalNode</strong> est une méthode qui permet d'ajouter le node journal à la liste des category nodes
	     * @param $journalNode : la valeur du node journal
	     * 
	     * <hr>
	     * <strong>Exemple : </strong>
	     * <pre>
	     *     $jn = new JournalNode();
	     *     ...
	     *     ...
	     *     
	     *     $md->addJournalNode($jn);
	     * </pre>
	     */
	    public function addJournalNode($journalNode)
	    {
	    	if(is_array($journalNode))
	        	$this->metadataBlocks["journal"] = $journalNode;
	        else
	        	$this->metadataBlocks["journal"] = $journalNode->toArray();
	    }

	    /**
	     * <strong>addSemanticsNode</strong> est une méthode qui permet d'ajouter le node semantics à la liste des category nodes
	     * @param $semanticsNode : la valeur du node semantics
	     * 
	     * <hr>
	     * <strong>Exemple : </strong>
	     * <pre>
	     *     $sn = new SemanticsNode();
	     *     ...
	     *     ...
	     *     
	     *     $md->addSemanticsNode($sn);
	     * </pre>
	     */
	    public function addSemanticsNode($semanticsNode)
	    {
	    	if(is_array($semanticsNode))
	        	$this->metadataBlocks["semantics"] = $semanticsNode;
	        else
	        	$this->metadataBlocks["semantics"] = $semanticsNode->toArray();
	    }

	    /**
	     * <strong>addSocialScienceNode</strong> est une méthode qui permet d'ajouter le node socialScience à la liste des category nodes
	     * @param $socialScienceNode : la valeur du node socialScience
	     * 
	     * <hr>
	     * <strong>Exemple : </strong>
	     * <pre>
	     *     $ssn = new SocialScienceNode();
	     *     ...
	     *     ...
	     *     
	     *     $md->addSocialScienceNode($ssn);
	     * </pre>
	     */
	    public function addSocialScienceNode($socialScienceNode)
	    {
	    	if(is_array($socialScienceNode))
	        	$this->metadataBlocks["socialscience"] = $socialScienceNode;
	        else
	        	$this->metadataBlocks["socialscience"] = $socialScienceNode->toArray();
	    }

        /**
         * <strong>toArray</strong> est une méthode qui permet d'obtenir le document sous forme d'un tableau
         * @return la représentation du document sous forme d'un tableau
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $array = $document->toArray(); 
         * </pre>
         */
        public function toArray()
        {
            return array(
                            "license"   		=> $this->getLicense(),
                            "termsOfUse"   		=> $this->getTermsOfUse(),
                            "metadataBlocks"   	=> $this->getMetadataBlocks(),
                        );
        }

        /**
         * <strong>toJSON</strong> est une méthode qui permet d'obtenir une représentation JSON du document
         * @return la représentation du document sous forme de chaine JSON
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      String json = $document->toJSON(); 
         * </pre>
         */
        public function toJSON($prettyPrint=false)
        {
            if($prettyPrint)
                return json_encode($this->toArray(), JSON_PRETTY_PRINT);
            else
                return json_encode($this->toArray());
        }

        /**
         * <strong>toString</strong> est une méthode qui permet d'obtenir une représentation textuelle du document (JSON minifié)
         * @return la représentation du document sous forme de chaine de caractère
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      String json = $document->toString(); 
         * </pre>
         */
        public function toString()
        {
            return $this->toJSON(false);
        }





	    //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
	    //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
	    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
	    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
	    //



	}

?>