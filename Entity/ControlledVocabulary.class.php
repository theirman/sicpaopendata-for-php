<?php

    namespace SicpaOpenData\Metadata;
    use SicpaOpenData\Helper as Helper;

    require_once("Helper/helper.php");
    require_once("Config/config-ws.php");



    /** Classe détaillant les vocabulaires contrôlés
     *  @author Thierry HEIRMAN
     *  @since Juilet 2021
     */
    class ControlledVocabulary
    {
        /**
         * <strong>getControlledVocabularies</strong> est une méthode qui permet d'obtenir la liste des vocabulaires contrôlés
         * @return une hashtable contenant la liste des vocabulaires contrôlés
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $cv = ControlledVocabulary::GetControlledVocabularies(); 
         * </pre>
         */
        public static function getControlledVocabularies()
        {
            $controlledVocabularies = json_decode(ControlledVocabulary::getFromUrl(CONTROLLED_VOCABULARY_WS));
            $flatControlledVocabularies = array();

            foreach($controlledVocabularies as $key => $cv)
            {
                if($key == "lastUpdated")
                    continue;

                foreach($cv as $nodeKey => $nodeValue)
                    $flatControlledVocabularies[$nodeKey] = $nodeValue;
            }

            return $flatControlledVocabularies;
        }


        /**
         * Cette méthode permet d'éxécuter une requête de type GET
         * @param $apiToken         jeton API de l'utilisateur
         * @param $endpoint         adresse vers la ressource HTTP
         * @return                  reponse http
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $reponse    = Helper::getFromUrl("http://localhost:8080/ControlledVocabularyWS/rest/controlledVocabularies");
         * </pre>
         */
        public static function getFromUrl($url)
        {
            // j'instancie mon client de connexion 
            $client = Helper::getHttpClient();

            // je met à jour mon client avec l'adresse du endpoint
            curl_setopt($client, CURLOPT_URL, $url);

            try                 { return curl_exec($client); }
            catch(Exception $e) { }
            finally             { curl_close($client); }
        }
            

        /**
         * <strong>listValuesFor</strong> est une méthode qui permet d'obtenir la liste des vocabulaires contrôlés
         * @param $node le noeud pour lequel recupéré les valeurs autorisées
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesFor("language"); 
         * </pre>
         */
        public static function listValuesFor($node)
        {
            $url  = CONTROLLED_VOCABULARY_WS."/listValuesFor?key=".$node;
            $json = ControlledVocabulary::getFromUrl($url);
            return json_decode($json);
        }

        /**
         * <strong>listValuesForAuthorIdentifierSchemes</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'authorIdentifierScheme'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForAuthorIdentifierSchemes(); 
         * </pre>
         */
        public static function listValuesForAuthorIdentifierSchemes()
        {
            //clé : authorIdentifierScheme
            return ControlledVocabulary::getControlledVocabularies()["authorIdentifierScheme"];
        }

        /**
         * <strong>listValuesForCollectionMode</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'collectionMode'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForCollectionMode(); 
         * </pre>
         */
        public static function listValuesForCollectionMode()
        {
            return ControlledVocabulary::getControlledVocabularies()["collectionMode"];
        }

        /**
         * <strong>listValuesForContributorIdentifierScheme</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'contributorIdentifierScheme'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForContributorIdentifierScheme(); 
         * </pre>
         */
        public static function listValuesForContributorIdentifierScheme()
        {
            return ControlledVocabulary::getControlledVocabularies()["contributorIdentifierScheme"];
        }

        /**
         * <strong>listValuesForContributorType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'contributorType'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForContributorType(); 
         * </pre>
         */
        public static function listValuesForContributorType()
        {
            return ControlledVocabulary::getControlledVocabularies()["contributorType"];
        }

        /**
         * <strong>listValuesForCountry</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'country'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForCountry(); 
         * </pre>
         */
        public static function listValuesForCountry()
        {
            return ControlledVocabulary::getControlledVocabularies()["country"];
        }

        /**
         * <strong>listValuesForDataOrigin</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'dataOrigin'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForDataOrigin(); 
         * </pre>
         */
        public static function listValuesForDataOrigin()
        {
            return ControlledVocabulary::getControlledVocabularies()["dataOrigin"];
        }

        /**
         * <strong>listValuesForDegree</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'degree'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForDegree(); 
         * </pre>
         */
        public static function listValuesForDegree()
        {
            return ControlledVocabulary::getControlledVocabularies()["degree"];
        }

        /**
         * <strong>listValuesForDesignForOntologyTask</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'designedForOntologyTask'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForDesignForOntologyTask(); 
         * </pre>
         */
        public static function listValuesForDesignForOntologyTask()
        {
            return ControlledVocabulary::getControlledVocabularies()["designedForOntologyTask"];
        }

        /**
         * <strong>listValuesForHasFormalityLevel</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'hasFormalityLevel'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForHasFormalityLevel(); 
         * </pre>
         */
        public static function listValuesForHasFormalityLevel()
        {
            return ControlledVocabulary::getControlledVocabularies()["hasFormalityLevel"];
        }

        /**
         * <strong>listValuesForHasOntologyLanguage</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'hasOntologyLanguage'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForHasOntologyLanguage(); 
         * </pre>
         */
        public static function listValuesForHasOntologyLanguage()
        {
            return ControlledVocabulary::getControlledVocabularies()["hasOntologyLanguage"];
        }

        /**
         * <strong>listValuesForJournalArticleType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'journalArticleType'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForJournalArticleType(); 
         * </pre>
         */
        public static function listValuesForJournalArticleType()
        {
            return ControlledVocabulary::getControlledVocabularies()["journalArticleType"];
        }

        /**
         * <strong>listValuesForKindOfData</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'kindOfData'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForKindOfData(); 
         * </pre>
         */
        public static function listValuesForKindOfData()
        {
            return ControlledVocabulary::getControlledVocabularies()["kindOfData"];
        }

        /**
         * <strong>listValuesForLanguage</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'language'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForLanguage(); 
         * </pre>
         */
        public static function listValuesForLanguage()
        {
            return ControlledVocabulary::getControlledVocabularies()["language"];
        }

        /**
         * <strong>listValuesForLifeCycleStep</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'lifeCycleStep'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForLifeCycleStep(); 
         * </pre>
         */
        public static function listValuesForLifeCycleStep()
        {
            return ControlledVocabulary::getControlledVocabularies()["lifeCycleStep"];
        }

        /**
         * <strong>listValuesForPublicationIDType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'publicationIDType'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForPublicationIDType(); 
         * </pre>
         */
        public static function listValuesForPublicationIDType()
        {
            return ControlledVocabulary::getControlledVocabularies()["publicationIDType"];
        }

        /**
         * <strong>listValuesForRelatedDatasetIDType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'relatedDatasetIDType'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForRelatedDatasetIDType(); 
         * </pre>
         */
        public static function listValuesForRelatedDatasetIDType()
        {
            return ControlledVocabulary::getControlledVocabularies()["relatedDatasetIDType"];
        }

        /**
         * <strong>listValuesForSamplingProcedure</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'samplingProcedure'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForSamplingProcedure(); 
         * </pre>
         */
        public static function listValuesForSamplingProcedure()
        {
            return ControlledVocabulary::getControlledVocabularies()["samplingProcedure"];
        }

        /**
         * <strong>listValuesForStudyAssayMeasurementType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyAssayMeasurementType'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForStudyAssayMeasurementType(); 
         * </pre>
         */
        public static function listValuesForStudyAssayMeasurementType()
        {
            return ControlledVocabulary::getControlledVocabularies()["studyAssayMeasurementType"];
        }

        /**
         * <strong>listValuesForStudyAssayOrganism</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyAssayOrganism'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForStudyAssayOrganism(); 
         * </pre>
         */
        public static function listValuesForStudyAssayOrganism()
        {
            return ControlledVocabulary::getControlledVocabularies()["studyAssayOrganism"];
        }

        /**
         * <strong>listValuesForStudyAssayPlatform</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyAssayPlatform'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForStudyAssayPlatform(); 
         * </pre>
         */
        public static function listValuesForStudyAssayPlatform()
        {
            return ControlledVocabulary::getControlledVocabularies()["studyAssayPlatform"];
        }

        /**
         * <strong>listValuesForStudyAssayTechnologyType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyAssayTechnologyType'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForStudyAssayTechnologyType(); 
         * </pre>
         */
        public static function listValuesForStudyAssayTechnologyType()
        {
            return ControlledVocabulary::getControlledVocabularies()["studyAssayTechnologyType"];
        }

        /**
         * <strong>listValuesForStudyDesignType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyDesignType'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForStudyDesignType(); 
         * </pre>
         */
        public static function listValuesForStudyDesignType()
        {
            //clé: studyDesignType
            return ControlledVocabulary::getControlledVocabularies()["studyDesignType"];
        }

        /**
         * <strong>listValuesForStudyFactorType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyFactorType'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForStudyFactorType(); 
         * </pre>
         */
        public static function listValuesForStudyFactorType()
        {
            return ControlledVocabulary::getControlledVocabularies()["studyFactorType"];
        }

        /**
         * <strong>listValuesForSubject</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'subject'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForSubject(); 
         * </pre>
         */
        public static function listValuesForSubject()
        {
            return ControlledVocabulary::getControlledVocabularies()["subject"];
        }

        /**
         * <strong>listValuesForTimeMethod</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'timeMethod'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForTimeMethod(); 
         * </pre>
         */
        public static function listValuesForTimeMethod()
        {
            return ControlledVocabulary::getControlledVocabularies()["timeMethod"];
        }

        /**
         * <strong>listValuesForTypeOfSR</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'typeOfSR'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForTypeOfSR(); 
         * </pre>
         */
        public static function listValuesForTypeOfSR()
        {
            return ControlledVocabulary::getControlledVocabularies()["typeOfSR"];
        }

        /**
         * <strong>listValuesForUnitOfAnalysis</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'unitOfAnalysis'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForUnitOfAnalysis(); 
         * </pre>
         */
        public static function listValuesForUnitOfAnalysis()
        {
            return ControlledVocabulary::getControlledVocabularies()["unitOfAnalysis"];
        }

        /**
         * <strong>listValuesForVersionStatus</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'versionStatus'
         * @return la liste de valeurs autorisées
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $values = ControlledVocabulary::listValuesForVersionStatus(); 
         * </pre>
         */
        public static function listValuesForVersionStatus()
        {
            return ControlledVocabulary::getControlledVocabularies()["versionStatus"];
        }
    }


?>