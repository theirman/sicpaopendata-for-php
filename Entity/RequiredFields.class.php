<?php

    namespace SicpaOpenData\Metadata;



    /** Classe détaillant les valeurs requises par nodes
     *  @author Thierry HEIRMAN
     *  @since Juillet 2021
     */
    class RequiredFields
    {
        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //


        /**
         * <strong>listRequiredFields</strong> est une méthode qui permet de récupérer la liste de tous les fields requis soit pour un node donné, soit pour le document complet des métadonnées
         * @param $node : le node pour lequel lister les champs requis
         * @return la liste des fields requis
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $requiredFields = RequiredFields::listRequiredFields("citation"); 
         * </pre>
         */
        public static function listRequiredFields($node)
        {
            if(empty($node))
            {
                return array_merge
                (
                    RequiredFields::listRequiredFieldsForBiomedicalNode(),
                    RequiredFields::listRequiredFieldsForCitationNode(),
                    RequiredFields::listRequiredFieldsForDerivedTextNode(),
                    RequiredFields::listRequiredFieldsForGeospatialNode(),
                    RequiredFields::listRequiredFieldsForJournalNode(),
                    RequiredFields::listRequiredFieldsForSemanticsNode(),
                    RequiredFields::listRequiredFieldsForSocialScienceNode()
                );
            }
            else
            {
                switch ($node)
                {
                    case "biomedical":      return RequiredFields::listRequiredFieldsForBiomedicalNode();
                    case "citation":        return RequiredFields::listRequiredFieldsForCitationNode();
                    case "Derived-text":    return RequiredFields::listRequiredFieldsForDerivedTextNode();
                    case "geospatial":      return RequiredFields::listRequiredFieldsForGeospatialNode();
                    case "journal":         return RequiredFields::listRequiredFieldsForJournalNode();
                    case "semantics":       return RequiredFields::listRequiredFieldsForSemanticsNode();
                    case "socialscience":   return RequiredFields::listRequiredFieldsForSocialScienceNode();
                    default:                return RequiredFields::listRequiredFields();
                }
            }

        }

        /**
         * <strong>listRequiredFieldsForBiomedicalNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "biomedical"
         * @return la liste des fields requis
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $requiredFields = RequiredFields::listRequiredFieldsForBiomedicalNode(); 
         * </pre>
         */
        public static function listRequiredFieldsForBiomedicalNode()
        {
            return array();
        }

        /**
         * <strong>listRequiredFieldsForCitationNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "citation"
         * @return la liste des fields requis
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $requiredFields = RequiredFields::listRequiredFieldsForCitationNode(); 
         * </pre>
         */
        public static function listRequiredFieldsForCitationNode()
        {
            return array
            (
                "title"             => "SimplePrimitiveNode",
                "author"            => "MultipleCompoundNode",
                "datasetContact"    => "MultipleCompoundNode",
                "dsDescription"     => "MultipleCompoundNode",
                "subject"           => "MultipleControlledVocabularyNode",
                "kindOfData"        => "MultipleControlledVocabularyNode",
            );
        }

        /**
         * <strong>listRequiredFieldsForDerivedTextNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "Derived-text"
         * @return la liste des fields requis
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $requiredFields = RequiredFields::listRequiredFieldsForDerivedTextNode(); 
         * </pre>
         */
        public static function listRequiredFieldsForDerivedTextNode()
        {
            return array();
        }

        /**
         * <strong>listRequiredFieldsForGeospatialNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "geospatial"
         * @return la liste des fields requis
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $requiredFields = RequiredFields::listRequiredFieldsForGeospatialNode(); 
         * </pre>
         */
        public static function listRequiredFieldsForGeospatialNode()
        {
            return array();
        }

        /**
         * <strong>listRequiredFieldsForJournalNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "journal"
         * @return la liste des fields requis
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $requiredFields = RequiredFields::listRequiredFieldsForJournalNode(); 
         * </pre>
         */
        public static function listRequiredFieldsForJournalNode()
        {
            return array();
        }

        /**
         * <strong>listRequiredFieldsForSemanticsNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "semantics"
         * @return la liste des fields requis
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $requiredFields = RequiredFields::listRequiredFieldsForSemanticsNode(); 
         * </pre>
         */
        public static function listRequiredFieldsForSemanticsNode()
        {
            return array();
        }

        /**
         * <strong>listRequiredFieldsForSocialScienceNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "socialscience"
         * @return la liste des fields requis
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      $requiredFields = RequiredFields::listRequiredFieldsForSocialScienceNode(); 
         * </pre>
         */
        public static function listRequiredFieldsForSocialScienceNode()
        {
            return array();
        }
    }

?>