<?php

    namespace SicpaOpenData\Metadata;

    require_once("Entity\BaseNode.class.php");



    /** Classe implémentant les nodes à valeur textuelle unique
     *  @author Thierry HEIRMAN
     *  @since Juilet 2021
     */
    class SimplePrimitiveNode extends BaseNode
    {
        //    ___ _______________  _______  __  ____________
        //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
        //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
        // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
        //                                                  

        /** Cette propriété contient la valeur du node */
        private $value;
        
        
        
        
        
        //   _________  _  _______________  __  ___________________  _____  ____
        //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
        // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
        // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
        //                                                                          

        /**
         * Constructeur
         * @param $typeName :   nom du node
         * @param $value    :   valeur du node
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $spn = new SimplePrimitiveNode("name", "value"); 
         * </pre>
         */
        public function __construct($typeName="", $value="")
        {
            $this->setMultiple(false);
            $this->setTypeClass("primitive");
            $this->setTypeName($typeName);
            $this->setValue($value);
        }


        
        
        
        //     ___  ____________________________  _____  ____
        //    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
        //   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
        //  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
        //

        /**
         * Permet d'obtenir la valeur de l'attribut <strong>value</strong> 
         * @return valeur de l'attribut <strong>value</strong> 
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $value = $spn->getValue();
         * </pre>
         */
        public function getValue()
        {
            return $this->value;
        }

        /**
         * Permet de mettre à jour la valeur de l'attribut <strong>value</strong>
         * @param $value : valeur à enregistrer dans l'attribut <strong>value</strong>
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $spn->setValue("value"); 
         * </pre>
         */
        public function setValue($value)
        {
            $this->value = $value;
        }


        
        
        
        //    __  _______________ ______  ___  ________
        //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
        //                                             

        /**
         * <strong>isValid</strong> est une méthode qui permet de s'assurer de la validité du node
         * @return true si le node est valide, false sinon
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre> 
         *      if($spn->isValid())
         *      {
         *          ...
         *      }
         * </pre>
         */
        public function isValid()
        {
            return is_bool($this->getMultiple())    && !$this->getMultiple()
                && is_string($this->getTypeClass()) && $this->getTypeClass() == "primitive"
                && is_string($this->getTypeName())
                && is_string($this->getValue());
        }

        /**
         * <strong>toJSON</strong> est une méthode qui permet d'obtenir une représentation JSON du node
         * @param $ $prettyPrint : indique si l'on doit adapter la chaine JSON à la lecture par l'humain
         * @return la représentation du node sous forme de chaine JSON
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $json = $spn->toJSON(); 
         * </pre>
         */
        public function toJSON($prettyPrint=false)
        {
            if (!$this->isValid())
                return "";

            if($prettyPrint)
                return json_encode($this->toArray(), JSON_PRETTY_PRINT);
            else
                return json_encode($this->toArray());
        }

        /**
         * <strong>toString</strong> est une méthode qui permet d'obtenir une représentation textuelle du node (JSON minifié)
         * @return la représentation du node sous forme de chaine de caractère
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $json = $spn->toString(); 
         * </pre>
         */
        public function toString()
        {
            return $this->toJSON(false);
        }

        
        
        
        //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
        //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
        //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
        // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
        //





    }

?>