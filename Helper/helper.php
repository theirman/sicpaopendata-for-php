<?php

	namespace SicpaOpenData;

    require_once("Config/config-curl.php");



	class Helper
	{
		public static function contains($haystack, $needle)
		{
			return (bool)preg_match('#'.$needle.'#', $haystack);		
		}

		public static function endsWith($haystack, $needle)
		{
			return (bool)preg_match('#'.$needle.'$#', $haystack);
		}

        /**
         * Cette méthode permet de créer un client HTTP pour executer les requetes REST
         * @return client HTTP
         */
        public static function getHttpClient()
        {
            // j'initie ma requete CURL
            $client = curl_init();

            // OPTIONS:
            curl_setopt($client, CURLOPT_CONNECTTIMEOUT,  MY_CURLOPT_CONNECTTIMEOUT);
            curl_setopt($client, CURLOPT_MAXREDIRS,       MY_CURLOPT_MAXREDIRS);
            curl_setopt($client, CURLOPT_RETURNTRANSFER,  MY_CURLOPT_RETURNTRANSFER);
            curl_setopt($client, CURLOPT_SSL_VERIFYHOST,  MY_CURLOPT_SSL_VERIFYHOST);
            curl_setopt($client, CURLOPT_SSL_VERIFYPEER,  MY_CURLOPT_SSL_VERIFYPEER);
            curl_setopt($client, CURLOPT_TIMEOUT,         MY_CURLOPT_TIMEOUT);
            curl_setopt($client, CURLOPT_VERBOSE,         MY_CURLOPT_VERBOSE);

            return $client;
        }

		public static function startsWith($haystack, $needle)
		{
			return (bool)preg_match('#^'.$needle.'#', $haystack);
		}
	}
	
?>