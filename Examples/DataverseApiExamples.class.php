<?php

    namespace SicpaOpenData\Examples;

    /** Classe montrant comment utiliser les classes de <c>SicpaOpenData.API</c>
     *  @author Thierry HEIRMAN
     *  @since Juillet 2021
     */
    class DataverseApiExamples
    {
        /**
         * <c>howToGetDatasetContents</c> montre la manière de lister le contenu d'un dataset sous la forme d'une trame JSON
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $adresseServeur = "http://localhost:57170";
         *      $doi            = "doi:10.12345/ABCDEF";
         *      $jetonAPI       = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *          
         *      $contenuDataset = DataverseAPI::getDatasetContents($adresseServeur, $jetonAPI, $doi);
         * </pre>
         */
        public static function howToGetDatasetContents()
        {
            echo PHP_EOL.'$adresseServeur = "http://localhost:57170";';
            echo PHP_EOL.'$doi            = "doi:10.12345/ABCDEF";';
            echo PHP_EOL.'$jetonAPI       = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";';
            echo PHP_EOL;
            echo PHP_EOL.'$contenuDataset = DataverseAPI::getDatasetContents($adresseServeur, $jetonAPI, $doi);';
            echo PHP_EOL;
        }

        /**
         * <c>howToGetDatasetFiles</c> montre la manière de lister les fichiers d'un dataset sous la forme d'une trame JSON
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $adresseServeur   = "http://localhost:57170";
         *      $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *      $idDataset        = "123456";
         *      $versionDataset   = "3.0";
         *          
         *      $listeFichiers    = DataverseAPI::getDatasetFiles($adresseServeur, $jetonAPI, $idDataset, $versionDataset);
         * </pre>
         */
        public static function howToGetDatasetFiles()
        {
            echo PHP_EOL.'$adresseServeur   = "http://localhost:57170";';
            echo PHP_EOL.'$jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";';
            echo PHP_EOL.'$idDataset        = "123456";';
            echo PHP_EOL.'$versionDataset   = "3.0";';
            echo PHP_EOL;
            echo PHP_EOL.'$listeFichiers    = DataverseAPI::getDatasetFiles($adresseServeur, $jetonAPI, $idDataset, $versionDataset);';
            echo PHP_EOL;
        }

        /**
         * <c>howToGetDatasetMetadataExport</c> montre la manière d'exporter les métadonnées d'un dataset dans un format de métadonnées demandé en paramètre
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $adresseServeur   = "http://localhost:57170";
         *      $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *      $doi              = "doi:10.12345/ABCDEF";
         *      $formatMetadonnee = "Datacite";
         *          
         *      $metadonnees      = DataverseAPI::getDatasetMetadataExport($adresseServeur, $jetonAPI, $doi, $formatMetadonnee);
         * </pre>
         */
        public static function howToGetDatasetMetadataExport()
        {
            echo PHP_EOL.'$adresseServeur   = "http://localhost:57170";';
            echo PHP_EOL.'$jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";';
            echo PHP_EOL.'$doi              = "doi:10.12345/ABCDEF";';
            echo PHP_EOL.'$formatMetadonnee = "Datacite";';
            echo PHP_EOL;
            echo PHP_EOL.'$metadonnees      = DataverseAPI::getDatasetMetadataExport($adresseServeur, $jetonAPI, $doi, $formatMetadonnee);';
            echo PHP_EOL;
        }

        /**
         * <c>howToGetDatasetVersion</c> montre la manière de lister le contenu d'une version d'un dataset sous la forme d'une trame JSON
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $adresseServeur   = "http://localhost:57170";
         *      $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *      $idDataset        = "123456";
         *      $versionDataset   = "3.0";
         *          
         *      $versionDataset   = DataverseAPI::getDatasetVersion($adresseServeur, $jetonAPI, $idDataset, $versionDataset);
         * </pre>
         */
        public static function howToGetDatasetVersion()
        {
            echo PHP_EOL.'$adresseServeur   = "http://localhost:57170";';
            echo PHP_EOL.'$jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";';
            echo PHP_EOL.'$idDataset        = "123456";';
            echo PHP_EOL.'$versionDataset   = "3.0";';
            echo PHP_EOL;
            echo PHP_EOL.'$versionDataset   = DataverseAPI::getDatasetVersion($adresseServeur, $jetonAPI, $idDataset, $versionDataset);';
            echo PHP_EOL;
        }

        /**
         * <c>howToGetDatasetVersionList</c> montre la manière de lister toutes les versions d'un dataset sous la forme d'une trame JSON
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $adresseServeur   = "http://localhost:57170";
         *      $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *      $idDataset        = "123456";
         *          
         *      $listeVersions    = DataverseAPI::getDatasetVersionList($adresseServeur, $jetonAPI, $idDataset);
         * </pre>
         */
        public static function howToGetDatasetVersionList()
        {
            echo PHP_EOL.'$adresseServeur   = "http://localhost:57170";';
            echo PHP_EOL.'$jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";';
            echo PHP_EOL.'$idDataset        = "123456";';
            echo PHP_EOL;       
            echo PHP_EOL.'$listeVersions    = DataverseAPI::getDatasetVersionList($adresseServeur, $jetonAPI, $idDataset);';
            echo PHP_EOL;       
        }

        /**
         * <c>howToGetDataverseContents</c> montre la manière de lister le contenu d'un dataverse sous la forme d'une trame JSON
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *      $adresseServeur   = "http://localhost:57170";
         *      $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *      $idDataverse      = "INRAE Dataverse";
         *          
         *      $contenuDataverse = DataverseAPI::getDataverseContents($adresseServeur, $jetonAPI, $idDataverse);
         * </pre>
         */
        public static function howToGetDataverseContents()
        {
            echo PHP_EOL.'$adresseServeur   = "http://localhost:57170";';
            echo PHP_EOL.'$jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";';
            echo PHP_EOL.'$idDataverse      = "INRAE Dataverse";';
            echo PHP_EOL;       
            echo PHP_EOL.'$contenuDataverse = DataverseAPI::getDataverseContents($adresseServeur, $jetonAPI, $idDataverse);';
            echo PHP_EOL;       
        }

        /**
         * <c>howToGetDataverseInfos</c> montre la manière de lister les infos concernant un dataverse sous la forme d'une trame JSON
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *         $adresseServeur   = "http://localhost:57170";
         *         $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *         $idDataverse      = "INRAE Dataverse";
         *         
         *         $infosDataverse   = DataverseAPI::GetDataverseInfos($adresseServeur, $jetonAPI, $idDataverse);
         * </pre>
         */
        public static function howToGetDataverseInfos()
        {
            echo PHP_EOL.'$adresseServeur   = "http://localhost:57170";';
            echo PHP_EOL.'$jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";';
            echo PHP_EOL.'$idDataverse      = "INRAE Dataverse";';
            echo PHP_EOL;       
            echo PHP_EOL.'$infosDataverse   = DataverseAPI::GetDataverseInfos($adresseServeur, $jetonAPI, $idDataverse);';
            echo PHP_EOL;       
        }

        /**
         * <c>howToGetDataverseMetadata</c> montre la manière de lister les métdonnées d'un dataverse sous la forme d'une trame JSON
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *         $adresseServeur   = "http://localhost:57170";
         *         $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *         $idDataverse      = "INRAE Dataverse";
         *         
         *         $metadonnees      = DataverseAPI::getDataverseMetadata($adresseServeur, $jetonAPI, $idDataverse);
         * </pre>
         */
        public static function howToGetDataverseMetadata()
        {
            echo PHP_EOL.'$adresseServeur   = "http://localhost:57170";';
            echo PHP_EOL.'$jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";';
            echo PHP_EOL.'$idDataverse      = "INRAE Dataverse";';
            echo PHP_EOL;       
            echo PHP_EOL.'$metadonnees      = DataverseAPI::getDataverseMetadata($adresseServeur, $jetonAPI, $idDataverse);';
            echo PHP_EOL;       
        }
      
        /**
         * <c>howToAddFileInDataset</c> montre la manière d'uploader un fichier vers un dataset
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *          $adresseServeur   = "http://localhost:57170";
         *          $doi              = "doi:10.12345/ABCDEF";
         *          $cheminFichier    = "c:\\temp\\fichier.csv";
         *          $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *          
         *          $status           = DataverseAPI::postDatasetAddFile($adresseServeur, $jetonAPI, $doi, $cheminFichier);
         * </pre>
         */
        public static function howToAddFileInDataset()
        {
            echo PHP_EOL.'$adresseServeur   = "http://localhost:57170";';
            echo PHP_EOL.'$doi              = "doi:10.12345/ABCDEF";';
            echo PHP_EOL.'$cheminFichier    = "c:\\temp\\fichier.csv";';
            echo PHP_EOL.'$jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";';
            echo PHP_EOL;       
            echo PHP_EOL.'$status           = DataverseAPI::postDatasetAddFile($adresseServeur, $jetonAPI, $doi, $cheminFichier);';
            echo PHP_EOL;       
        }

        /**
         * <c>howToCreateDataset</c> montre la manière de créer un dataset
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *          $adresseServeur   = "http://localhost:57170";
         *          $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *          $idDataverse      = "INRAE Dataverse";
         *          $metadata         = file_get_contents("c:\\temp\\metadata.json");
         *          
         *          $status           = DataverseAPI::postDatasetCreate($adresseServeur, $jetonAPI, $idDataverse, $metadata);
         * </pre>
         */
        public static function howToCreateDataset()
        {
            echo PHP_EOL.'$adresseServeur   = "http://localhost:57170";';
            echo PHP_EOL.'$jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";';
            echo PHP_EOL.'$idDataverse      = "INRAE Dataverse";';
            echo PHP_EOL.'$metadata         = file_get_contents("c:\\temp\\metadata.json");';
            echo PHP_EOL;       
            echo PHP_EOL.'$status           = DataverseAPI::postDatasetCreate($adresseServeur, $jetonAPI, $idDataverse, $metadata);';
            echo PHP_EOL;       
        }

        /**
         * <c>howToPublishtDataset</c> montre la manière de publier un dataset
         * 
         * <hr>
         * <strong>Exemple : </strong>
         * <pre>
         *          $adresseServeur   = "http://localhost:57170";
         *          $doi              = "doi:10.12345/ABCDEF";
         *          $jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
         *          $typeVersion      = "minor";
         *          
         *          $status           = DataverseAPI::postDatasetPublish($adresseServeur, $jetonAPI, $doi, $typeVersion);
         * </pre>
         */
        public static function howToPublishtDataset()
        {
            echo PHP_EOL.'$adresseServeur   = "http://localhost:57170";';
            echo PHP_EOL.'$doi              = "doi:10.12345/ABCDEF";';
            echo PHP_EOL.'$jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";';
            echo PHP_EOL.'$typeVersion      = "minor";';
            echo PHP_EOL;       
            echo PHP_EOL.'$status           = DataverseAPI::postDatasetPublish($adresseServeur, $jetonAPI, $doi, $typeVersion);';
            echo PHP_EOL;       
        }
    }

?>