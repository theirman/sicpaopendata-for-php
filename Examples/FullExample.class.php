<?php

    namespace SicpaOpenData\Examples;

    /** Classe montrant comment créer un dataset en s'appuyant sur un document de métadonnées, y ajouter des fichiers et le publier
     *  @author Thierry HEIRMAN
     *  @since Janvier 2022
     */
    class FullExample
    {
        /** <c>howToOpenData</c> montre le processus complet :
         *                                 - de création d'un document de métadonnées (simplifié)
         *                                 - de création d'un dataset
         *                                 - d'export de fichiers vers ce dataset
         *                                 - de publication du dataset
         *                                 - de vérification du contenu du dataset
         *                                 - de vérification des fichiers du dataset
         * 
         *  <hr>
         *  <strong>Exemple de code : </strong>
         *  <pre>
         *  // -------------------------------------------
         *  // BUILD A CITATION NODE
         *  // -------------------------------------------
         *  
         *  // je crée les auteurs du dataset
         *  $auteur = array();
         *  $auteur["authorAffiliation"]           = new SimplePrimitiveNode("authorAffiliation", "affiliation");
         *  $auteur["authorIdentifier"]            = new SimplePrimitiveNode("authorIdentifier", "identifier");
         *  $auteur["authorIdentifierScheme"]      = new SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID");
         *  $auteur["authorName"]                  = new SimplePrimitiveNode("authorName", "name");
         *  
         *  // je crée les contacts du dataset
         *  $contact = array();
         *  $contact["datasetContactAffiliation"]  = new SimplePrimitiveNode("datasetContactAffiliation", "affiliation");
         *  $contact["datasetContactEmail"]        = new SimplePrimitiveNode("datasetContactEmail", "email");
         *  $contact["datasetContactName"]         = new SimplePrimitiveNode("datasetContactName", "name");
         *  
         *  // je crée les descriptions du dataset
         *  $description = array();
         *  $description["dsDescriptionDate"]      = new SimplePrimitiveNode("dsDescriptionDate", "date");
         *  $description["dsDescriptionValue"]     = new SimplePrimitiveNode("dsDescriptionValue", "description");
         *  
         *  // je crée un node citation
         *  $citationNode = new CitationNode();
         *  $citationNode->addField(new SimplePrimitiveNode("title", "title"));
         *  $citationNode->addField(new MultipleCompoundNode("datasetContact", array($contact) ));
         *  $citationNode->addField(new MultipleCompoundNode("author", array($auteur) ));
         *  $citationNode->addField(new MultipleCompoundNode("dsDescription", array($description) ));
         *  $citationNode->addField(new MultipleControlledVocabularyNode("subject",array("Animal Breeding and Animal Products") ));
         *  $citationNode->addField(new MultipleControlledVocabularyNode("kindOfData",array("Audiovisual") ));
         *  
         *  
         *  // -------------------------------------------
         *  // BUILD A METADATA DOCUMENT
         *  // -------------------------------------------
         *  
         *  // je crée le document de métadonnées
         *  $metadataDocument = new MetadataDocument();
         *  
         *  // j\'ajoute mon noeud citation au document de métadonnées
         *  $metadataDocument.AddCitationNode($citationNode);
         *  
         *  
         *  // -------------------------------------------
         *  // CREATE A DATASET
         *  // -------------------------------------------
         *  
         *  // je configure l\'appel au serveur data
         *  $serverBaseURI    = "http://localhost:57170
         *  $apiToken         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx
         *  $dataverseID      = "INRAE Dataverse
         *         
         *  // je crée le nouveau dataset sur le serveur
         *  $status           = json_decode(DataverseAPI::postDatasetCreate($serverBaseURI, $apiToken, $dataverseID, $metadataDocument));
         *         
         *  // je récupère les informations du dataset
         *  $datasetID        = $status->data->id
         *  $doi              = $status->data->persistentId
         *         
         *         
         *  // -------------------------------------------
         *  // EXPORT FILES TO DATASET
         *  // -------------------------------------------
         *  
         *  // je souhaite transférer 2 fichiers CSV et leur fichier de métadonnées
         *  $files    = array( "c:\\temp\\animaux.csv", "c:\\temp\\animaux.meta.txt", "c:\\temp\\echantillons.csv", "c:\\temp\\echantillons.meta.txt" );
         *         
         *  // je transfère les 4 fichiers vers le dataset que je viens de créer
         *  foreach($files as $file)
         *      DataverseAPI::postDatasetAddFile($serverBaseURI, $apiToken, $doi, $file);
         *  
         *  
         *  // je publie le dataset
         *  $status           = DataverseAPI::postDatasetPublish($serverBaseURI, $apiToken, $doi, "major");
         *  
         *  
         *  // -------------------------------------------
         *  // VERIFY DATASET CONTENTS
         *  // -------------------------------------------
         *  
         *  // je vérifie le contenu du dataset
         *  echo DataverseAPI::getDatasetContents($serverBaseURI, $apiToken, $doi);
         *  
         *  
         *  // -------------------------------------------
         *  // VERIFY DATASET FILES
         *  // -------------------------------------------
         *  
         *  // je vérifie les fichiers du dataset
         *  echo DataverseAPI::getDatasetFiles($serverBaseURI, $apiToken, $datasetID, "1.0");
         *  </pre>
         */ 
        public static function howToOpenData()
        {
            echo PHP_EOL."//     ___       _ __   __             _ __       __  _                          __     ";
            echo PHP_EOL."//    / _ )__ __(_) /__/ / ___ _  ____(_) /____ _/ /_(_)__  ___    ___  ___  ___/ /__   ";
            echo PHP_EOL."//   / _  / // / / / _  / / _ `/ / __/ / __/ _ `/ __/ / _ \/ _ \  / _ \/ _ \/ _  / -_)  ";
            echo PHP_EOL."//  /____/\_,_/_/_/\_,_/  \_,_/  \__/_/\__/\_,_/\__/_/\___/_//_/ /_//_/\___/\_,_/\__/   ";
            echo PHP_EOL."//                                                                                      ";
            echo PHP_EOL;
            echo PHP_EOL.'// je crée les auteurs du dataset';
            echo PHP_EOL.'$auteur = array();';
            echo PHP_EOL.'$auteur["authorAffiliation"]           = new SimplePrimitiveNode("authorAffiliation", "affiliation");';
            echo PHP_EOL.'$auteur["authorIdentifier"]            = new SimplePrimitiveNode("authorIdentifier", "identifier");';
            echo PHP_EOL.'$auteur["authorIdentifierScheme"]      = new SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID");';
            echo PHP_EOL.'$auteur["authorName"]                  = new SimplePrimitiveNode("authorName", "name");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée les contacts du dataset';
            echo PHP_EOL.'$contact = array();';
            echo PHP_EOL.'$contact["datasetContactAffiliation"]  = new SimplePrimitiveNode("datasetContactAffiliation", "affiliation");';
            echo PHP_EOL.'$contact["datasetContactEmail"]        = new SimplePrimitiveNode("datasetContactEmail", "email");';
            echo PHP_EOL.'$contact["datasetContactName"]         = new SimplePrimitiveNode("datasetContactName", "name");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée les descriptions du dataset';
            echo PHP_EOL.'$description = array();';
            echo PHP_EOL.'$description["dsDescriptionDate"]      = new SimplePrimitiveNode("dsDescriptionDate", "date");';
            echo PHP_EOL.'$description["dsDescriptionValue"]     = new SimplePrimitiveNode("dsDescriptionValue", "description");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée un node citation';
            echo PHP_EOL.'$citationNode = new CitationNode();';
            echo PHP_EOL.'$citationNode->addField(new SimplePrimitiveNode("title", "title"));';
            echo PHP_EOL.'$citationNode->addField(new MultipleCompoundNode("datasetContact", array($contact) ));';
            echo PHP_EOL.'$citationNode->addField(new MultipleCompoundNode("author", array($auteur) ));';
            echo PHP_EOL.'$citationNode->addField(new MultipleCompoundNode("dsDescription", array($description) ));';
            echo PHP_EOL.'$citationNode->addField(new MultipleControlledVocabularyNode("subject",array("Animal Breeding and Animal Products") ));';
            echo PHP_EOL.'$citationNode->addField(new MultipleControlledVocabularyNode("kindOfData",array("Audiovisual") ));';
            echo PHP_EOL;
            echo PHP_EOL;
            echo PHP_EOL."//     ___       _ __   __                   __          __     __            __                             __   ";
            echo PHP_EOL."//    / _ )__ __(_) /__/ / ___ _  __ _  ___ / /____ ____/ /__ _/ /____ _  ___/ /__  ______ ____ _  ___ ___  / /_  ";
            echo PHP_EOL."//   / _  / // / / / _  / / _ `/ /  ' \/ -_) __/ _ `/ _  / _ `/ __/ _ `/ / _  / _ \/ __/ // /  ' \/ -_) _ \/ __/  ";
            echo PHP_EOL."//  /____/\_,_/_/_/\_,_/  \_,_/ /_/_/_/\__/\__/\_,_/\_,_/\_,_/\__/\_,_/  \_,_/\___/\__/\_,_/_/_/_/\__/_//_/\__/   ";
            echo PHP_EOL."//                                                                                                                ";
            echo PHP_EOL;
            echo PHP_EOL.'// je crée le document de métadonnées';
            echo PHP_EOL.'$metadataDocument = new MetadataDocument();';
            echo PHP_EOL;
            echo PHP_EOL.'// j\'ajoute mon noeud citation au document de métadonnées';
            echo PHP_EOL.'$metadataDocument.AddCitationNode($citationNode);';
            echo PHP_EOL;
            echo PHP_EOL;
            echo PHP_EOL.'//    _____             __                  __     __               __   ';
            echo PHP_EOL.'//   / ___/______ ___ _/ /____   ___ _  ___/ /__ _/ /____ ____ ___ / /_  ';
            echo PHP_EOL.'//  / /__/ __/ -_) _ `/ __/ -_) / _ `/ / _  / _ `/ __/ _ `(_-</ -_) __/  ';
            echo PHP_EOL.'//  \___/_/  \__/\_,_/\__/\__/  \_,_/  \_,_/\_,_/\__/\_,_/___/\__/\__/   ';
            echo PHP_EOL;
            echo PHP_EOL;
            echo PHP_EOL.'// je configure l\'appel au serveur data';
            echo PHP_EOL.'$serverBaseURI    = "http://localhost:57170";';
            echo PHP_EOL.'$apiToken         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";';
            echo PHP_EOL.'$dataverseID      = "INRAE Dataverse";';
            echo PHP_EOL;       
            echo PHP_EOL.'// je crée le nouveau dataset sur le serveur';
            echo PHP_EOL.'$status           = json_decode(DataverseAPI::postDatasetCreate($serverBaseURI, $apiToken, $dataverseID, $metadataDocument));';
            echo PHP_EOL;       
            echo PHP_EOL.'// je récupère les informations du dataset';
            echo PHP_EOL.'$datasetID        = $status->data->id';
            echo PHP_EOL.'$doi              = $status->data->persistentId';
            echo PHP_EOL;       
            echo PHP_EOL;       
            echo PHP_EOL.'//     ____                    __    ____ __          __            __     __               __   ';
            echo PHP_EOL.'//    / __/_ __ ___  ___  ____/ /_  / _(_) /__ ___   / /____    ___/ /__ _/ /____ ____ ___ / /_  ';
            echo PHP_EOL.'//   / _/ \ \ // _ \/ _ \/ __/ __/ / _/ / / -_|_-<  / __/ _ \  / _  / _ `/ __/ _ `(_-</ -_) __/  ';
            echo PHP_EOL.'//  /___//_\_\/ .__/\___/_/  \__/ /_//_/_/\__/___/  \__/\___/  \_,_/\_,_/\__/\_,_/___/\__/\__/   ';
            echo PHP_EOL.'//           /_/                                                                                 ';
            echo PHP_EOL;
            echo PHP_EOL;
            echo PHP_EOL.'// je souhaite transférer 2 fichiers CSV et leur fichier de métadonnées';
            echo PHP_EOL.'$files    = array( "c:\\temp\\animaux.csv", "c:\\temp\\animaux.meta.txt", "c:\\temp\\echantillons.csv", "c:\\temp\\echantillons.meta.txt" );';
            echo PHP_EOL;       
            echo PHP_EOL.'// je transfère les 4 fichiers vers le dataset que je viens de créer';
            echo PHP_EOL.'foreach($files as $file)';
            echo PHP_EOL.'    DataverseAPI::postDatasetAddFile($serverBaseURI, $apiToken, $doi, $file);';
            echo PHP_EOL;
            echo PHP_EOL;
            echo PHP_EOL.'//     ___       __   ___     __     __  __            __     __               __   ';
            echo PHP_EOL.'//    / _ \__ __/ /  / (_)__ / /    / /_/ /  ___   ___/ /__ _/ /____ ____ ___ / /_  ';
            echo PHP_EOL.'//   / ___/ // / _ \/ / (_-</ _ \  / __/ _ \/ -_) / _  / _ `/ __/ _ `(_-</ -_) __/  ';
            echo PHP_EOL.'//  /_/   \_,_/_.__/_/_/___/_//_/  \__/_//_/\__/  \_,_/\_,_/\__/\_,_/___/\__/\__/   ';
            echo PHP_EOL;
            echo PHP_EOL.'// je publie le dataset';
            echo PHP_EOL.'$status           = DataverseAPI::postDatasetPublish($serverBaseURI, $apiToken, $doi, "major");';
            echo PHP_EOL;
            echo PHP_EOL;
            echo PHP_EOL.'//    _   __        _ ___          __     __               __                  __           __      ';
            echo PHP_EOL.'//   | | / /__ ____(_) _/_ __  ___/ /__ _/ /____ ____ ___ / /_  _______  ___  / /____ ___  / /____  ';
            echo PHP_EOL.'//   | |/ / -_) __/ / _/ // / / _  / _ `/ __/ _ `(_-</ -_) __/ / __/ _ \/ _ \/ __/ -_) _ \/ __(_-<  ';
            echo PHP_EOL.'//   |___/\__/_/ /_/_/ \_, /  \_,_/\_,_/\__/\_,_/___/\__/\__/  \__/\___/_//_/\__/\__/_//_/\__/___/  ';
            echo PHP_EOL.'//                    /___/                                                                         ';
            echo PHP_EOL;
            echo PHP_EOL.'// je vérifie le contenu du dataset';
            echo PHP_EOL.'echo DataverseAPI::getDatasetContents($serverBaseURI, $apiToken, $doi);';
            echo PHP_EOL;
            echo PHP_EOL;
            echo PHP_EOL.'//   _   __        _ ___       __  __            __     __               __    ____ __        ';
            echo PHP_EOL.'//  | | / /__ ____(_) _/_ __  / /_/ /  ___   ___/ /__ _/ /____ ____ ___ / /_  / _(_) /__ ___  ';
            echo PHP_EOL.'//  | |/ / -_) __/ / _/ // / / __/ _ \/ -_) / _  / _ `/ __/ _ `(_-</ -_) __/ / _/ / / -_|_-<  ';
            echo PHP_EOL.'//  |___/\__/_/ /_/_/ \_, /  \__/_//_/\__/  \_,_/\_,_/\__/\_,_/___/\__/\__/ /_//_/_/\__/___/  ';
            echo PHP_EOL.'//                   /___/                                                                    ';
            echo PHP_EOL;
            echo PHP_EOL.'// je vérifie les fichiers du dataset';
            echo PHP_EOL.'echo DataverseAPI::getDatasetFiles($serverBaseURI, $apiToken, $datasetID, "1.0");';
            echo PHP_EOL;
        }
    }
?>