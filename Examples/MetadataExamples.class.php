<?php

    namespace SicpaOpenData\Examples;

    /** Classe montrant comment utiliser les classes de <c>SicpaOpenData.Metadata</c>
     *  @author Thierry HEIRMAN
     *  @since Juillet 2021
     */
    class MetadataExamples
    {
        /** <c>howToBuildABiomedicalNode</c> montre la manière de construire un <c>CitationNode</c>
         * 
         *  <hr>
         *  <strong>Exemple de code : </strong>
         *  <pre>
         *  // je crée un node biomedical
         *  $bn = new BiomedicalNode();
         *  
         *  $bn->addField(new MultipleControlledVocabularyNode("studyDesignType",             array("Case Control", "Cross Sectional") ));
         *  $bn->addField(new MultiplePrimitiveNode("studyDesignTypeOther",                   array("studyDesignTypeOther1", "studyDesignTypeOther2") ));
         *  $bn->addField(new MultipleControlledVocabularyNode("studyFactorType",             array("Age", "Biomarkers") ));
         *  $bn->addField(new MultiplePrimitiveNode("studyFactorTypeOther",                   array("studyFactorTypeOther1", "studyFactorTypeOther2") ));
         *  $bn->addField(new MultipleControlledVocabularyNode("studyAssayOrganism",          array("Arabidopsis thaliana", "Bos taurus") ));
         *  $bn->addField(new MultiplePrimitiveNode("studyAssayOtherOrganism",                array("studyAssayOtherOrganism1", "studyAssayOtherOrganism2") ));
         *  $bn->addField(new MultipleControlledVocabularyNode("studyAssayMeasurementType",   array("cell counting", "cell sorting") ));
         *  $bn->addField(new MultiplePrimitiveNode("studyAssayOtherMeasurmentType",          array("studyAssayOtherMeasurmentType1", "studyAssayOtherMeasurmentType2") ));
         *  $bn->addField(new MultipleControlledVocabularyNode("studyAssayTechnologyType",    array("culture based drug susceptibility testing, single concentration", "culture based drug susceptibility testing, two concentrations") ));
         *  $bn->addField(new MultiplePrimitiveNode("studyAssayTechnologyTypeOther",          array("studyAssayTechnologyTypeOther1", "studyAssayTechnologyTypeOther2") ));
         *  $bn->addField(new MultipleControlledVocabularyNode("studyAssayPlatform",          array("210-MS GC Ion Trap (Varian)", "220-MS GC Ion Trap (Varian)") ));
         *  $bn->addField(new MultiplePrimitiveNode("studyAssayPlatformOther",                array("studyAssayPlatformOther1", "studyAssayPlatformOther2") ));
         *  $bn->addField(new MultiplePrimitiveNode("studyAssayCellType",                     array("studyAssayCellType1", "studyAssayCellType2") ));
         *  $bn->addField(new MultiplePrimitiveNode("studySampleType",                        array("studySampleType1", "studySampleType2") ));
         *  $bn->addField(new MultiplePrimitiveNode("studyProtocolType",                      array("studyProtocolType1", "studyProtocolType2") ));
         *  
         *  echo $bn.toJSON();
         *  </pre>
         */ 
        public static function howToBuildABiomedicalNode()
        {
            echo PHP_EOL.'// je crée un node biomedical';
            echo PHP_EOL.'$bn = new BiomedicalNode();';
            echo PHP_EOL;
            echo PHP_EOL.'$bn->addField(new MultipleControlledVocabularyNode("studyDesignType",             array("Case Control", "Cross Sectional") ));';
            echo PHP_EOL.'$bn->addField(new MultiplePrimitiveNode("studyDesignTypeOther",                   array("studyDesignTypeOther1", "studyDesignTypeOther2") ));';
            echo PHP_EOL.'$bn->addField(new MultipleControlledVocabularyNode("studyFactorType",             array("Age", "Biomarkers") ));';
            echo PHP_EOL.'$bn->addField(new MultiplePrimitiveNode("studyFactorTypeOther",                   array("studyFactorTypeOther1", "studyFactorTypeOther2") ));';
            echo PHP_EOL.'$bn->addField(new MultipleControlledVocabularyNode("studyAssayOrganism",          array("Arabidopsis thaliana", "Bos taurus") ));';
            echo PHP_EOL.'$bn->addField(new MultiplePrimitiveNode("studyAssayOtherOrganism",                array("studyAssayOtherOrganism1", "studyAssayOtherOrganism2") ));';
            echo PHP_EOL.'$bn->addField(new MultipleControlledVocabularyNode("studyAssayMeasurementType",   array("cell counting", "cell sorting") ));';
            echo PHP_EOL.'$bn->addField(new MultiplePrimitiveNode("studyAssayOtherMeasurmentType",          array("studyAssayOtherMeasurmentType1", "studyAssayOtherMeasurmentType2") ));';
            echo PHP_EOL.'$bn->addField(new MultipleControlledVocabularyNode("studyAssayTechnologyType",    array("culture based drug susceptibility testing, single concentration", "culture based drug susceptibility testing, two concentrations") ));';
            echo PHP_EOL.'$bn->addField(new MultiplePrimitiveNode("studyAssayTechnologyTypeOther",          array("studyAssayTechnologyTypeOther1", "studyAssayTechnologyTypeOther2") ));';
            echo PHP_EOL.'$bn->addField(new MultipleControlledVocabularyNode("studyAssayPlatform",          array("210-MS GC Ion Trap (Varian)", "220-MS GC Ion Trap (Varian)") ));';
            echo PHP_EOL.'$bn->addField(new MultiplePrimitiveNode("studyAssayPlatformOther",                array("studyAssayPlatformOther1", "studyAssayPlatformOther2") ));';
            echo PHP_EOL.'$bn->addField(new MultiplePrimitiveNode("studyAssayCellType",                     array("studyAssayCellType1", "studyAssayCellType2") ));';
            echo PHP_EOL.'$bn->addField(new MultiplePrimitiveNode("studySampleType",                        array("studySampleType1", "studySampleType2") ));';
            echo PHP_EOL.'$bn->addField(new MultiplePrimitiveNode("studyProtocolType",                      array("studyProtocolType1", "studyProtocolType2") ));';
            echo PHP_EOL;
            echo PHP_EOL.'echo $bn.toJSON();';
            echo PHP_EOL;
        }

        /** <c>howToBuildACitationNode</c> montre la manière de construire un <c>CitationNode</c>
         * 
         *  <hr>
         *  <strong>Exemple de code : </strong>
         *  <pre>
         *  // je crée les auteurs du dataset
         *  $auteur1 = array();
         *  $auteur1["authorAffiliation"]                   = new SimplePrimitiveNode("authorAffiliation", "authorAffiliation1");
         *  $auteur1["authorIdentifier"]                    = new SimplePrimitiveNode("authorIdentifier", "authorIdentifier1");
         *  $auteur1["authorIdentifierScheme"]              = new SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID");
         *  $auteur1["authorName"]                          = new SimplePrimitiveNode("authorName", "authorName1");
         *  
         *  $auteur2 = array();
         *  $auteur2["authorAffiliation"]                   = new SimplePrimitiveNode("authorAffiliation", "authorAffiliation2");
         *  $auteur2["authorIdentifier"]                    = new SimplePrimitiveNode("authorIdentifier", "authorIdentifier2");
         *  $auteur2["authorIdentifierScheme"]              = new SimpleControlledVocabularyNode("authorIdentifierScheme", "idHAL");
         *  $auteur2["authorName"]                          = new SimplePrimitiveNode("authorName", "authorName2");
         *  
         *  // je crée les autres identifiants
         *  $autreId1 = array();
         *  $autreId1["otherIdAgency"]                      = new SimplePrimitiveNode("otherIdAgency", "otherIdAgency1");
         *  $autreId1["otherIdValue"]                       = new SimplePrimitiveNode("otherIdValue", "otherIdAgencyIdentifier1");
         *  
         *  $autreId2 = array();
         *  $autreId2["otherIdAgency"]                      = new SimplePrimitiveNode("otherIdAgency", "otherIdAgency2");
         *  $autreId2["otherIdValue"]                       = new SimplePrimitiveNode("otherIdValue", "otherIdAgencyIdentifier2");
         *  
         *  // je crée les contacts du dataset
         *  $contact1 = array();
         *  $contact1["datasetContactAffiliation"]          = new SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation1");
         *  $contact1["datasetContactEmail"]                = new SimplePrimitiveNode("datasetContactEmail", "contact1@mail.com");
         *  $contact1["datasetContactName"]                 = new SimplePrimitiveNode("datasetContactName", "contactName1");
         *  
         *  $contact2 = array();
         *  $contact2["datasetContactAffiliation"]          = new SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation2");
         *  $contact2["datasetContactEmail"]                = new SimplePrimitiveNode("datasetContactEmail", "contact2@mail.com");
         *  $contact2["datasetContactName"]                 = new SimplePrimitiveNode("datasetContactName", "contactName2");
         *  
         *  // je crée les contributeurs du dataset
         *  $contributeur1 = array();
         *  $contributeur1["contributorAffiliation"]        = new SimplePrimitiveNode("contributorAffiliation", "contributorAffiliation1");
         *  $contributeur1["contributorIdentifier"]         = new SimplePrimitiveNode("contributorIdentifier", "contributorIdentifier1");
         *  $contributeur1["contributorIdentifierScheme"]   = new SimpleControlledVocabularyNode("contributorIdentifierScheme", "ORCID");
         *  $contributeur1["contributorName"]               = new SimplePrimitiveNode("contributorName", "contributorName1");
         *  $contributeur1["contributorType"]               = new SimpleControlledVocabularyNode("contributorType", "Data collector");
         *  
         *  $contributeur2 = array();
         *  $contributeur2["contributorAffiliation"]        = new SimplePrimitiveNode("contributorAffiliation", "contributorAffiliation2");
         *  $contributeur2["contributorIdentifier"]         = new SimplePrimitiveNode("contributorIdentifier", "contributorIdentifier2");
         *  $contributeur2["contributorIdentifierScheme"]   = new SimpleControlledVocabularyNode("contributorIdentifierScheme", "ORCID");
         *  $contributeur2["contributorName"]               = new SimplePrimitiveNode("contributorName", "contributorName2");
         *  $contributeur2["contributorType"]               = new SimpleControlledVocabularyNode("contributorType", "Editor");
         *  
         *  // je crée les dates des collections du dataset
         *  $dates1 = array();
         *  $dates1["dateOfCollectionStart"]                = new SimplePrimitiveNode("dateOfCollectionStart", "2000-01-01");
         *  $dates1["dateOfCollectionEnd"]                  = new SimplePrimitiveNode("dateOfCollectionEnd", "2000-01-02");
         *  
         *  $dates2 = array();
         *  $dates2["dateOfCollectionStart"]                = new SimplePrimitiveNode("dateOfCollectionStart", "2000-01-03");
         *  $dates2["dateOfCollectionEnd"]                  = new SimplePrimitiveNode("dateOfCollectionEnd", "2000-01-04");
         *  
         *  // je crée les descriptions du dataset
         *  $description1 = array();
         *  $description1["dsDescriptionDate"]              = new SimplePrimitiveNode("dsDescriptionDate", "2000-01-01");
         *  $description1["dsDescriptionValue"]             = new SimplePrimitiveNode("dsDescriptionValue", "descriptionText1");
         *  
         *  $description2 = array();
         *  $description2["dsDescriptionDate"]              = new SimplePrimitiveNode("dsDescriptionDate", "2000-01-01");
         *  $description2["dsDescriptionValue"]             = new SimplePrimitiveNode("dsDescriptionValue", "descriptionText2");
         *  
         *  // je crée les distributeurs du dataset
         *  $distributeur1 = array();
         *  $distributeur1["distributorAbbreviation"]       = new SimplePrimitiveNode("distributorAbbreviation", "distributorAbbreviation1");
         *  $distributeur1["distributorAffiliation"]        = new SimplePrimitiveNode("distributorAffiliation", "distributorAffiliation1");
         *  $distributeur1["distributorLogoURL"]            = new SimplePrimitiveNode("distributorLogoURL", "distributorLogoURL1");
         *  $distributeur1["distributorName"]               = new SimplePrimitiveNode("distributorName", "distributorName1");
         *  $distributeur1["distributorURL"]                = new SimplePrimitiveNode("distributorURL", "distributorURL1");
         *  
         *  $distributeur2 = array();
         *  $distributeur2["distributorAbbreviation"]       = new SimplePrimitiveNode("distributorAbbreviation", "distributorAbbreviation2");
         *  $distributeur2["distributorAffiliation"]        = new SimplePrimitiveNode("distributorAffiliation", "distributorAffiliation2");
         *  $distributeur2["distributorLogoURL"]            = new SimplePrimitiveNode("distributorLogoURL", "distributorLogoURL2");
         *  $distributeur2["distributorName"]               = new SimplePrimitiveNode("distributorName", "distributorName2");
         *  $distributeur2["distributorURL"]                = new SimplePrimitiveNode("distributorURL", "distributorURL2");
         *  
         *  // je crée les grant number du dataset
         *  $grantNumber1 = array();
         *  $grantNumber1["grantNumberAgency"]              = new SimplePrimitiveNode("grantNumberAgency", "grantAgency1");
         *  $grantNumber1["grantNumberValue"]               = new SimplePrimitiveNode("grantNumberValue", "grantNumber1");
         *  
         *  $grantNumber2 = array();
         *  $grantNumber2["grantNumberAgency"]              = new SimplePrimitiveNode("grantNumberAgency", "grantAgency2");
         *  $grantNumber2["grantNumberValue"]               = new SimplePrimitiveNode("grantNumberValue", "grantNumber2");
         *  
         *  // je crée les logiciels du dataset
         *  $logiciel1 = array();
         *  $logiciel1["softwareName"]                      = new SimplePrimitiveNode("softwareName", "softwareName1");
         *  $logiciel1["softwareVersion"]                   = new SimplePrimitiveNode("softwareVersion", "softwareVersion1");
         *  
         *  $logiciel2 = array();
         *  $logiciel2["softwareName"]                      = new SimplePrimitiveNode("softwareName", "softwareName2");
         *  $logiciel2["softwareVersion"]                   = new SimplePrimitiveNode("softwareVersion", "softwareVersion2");
         *  
         *  // je crée les mots clés du dataset
         *  $motCle1 = array();
         *  $motCle1["keywordTermURI"]                      = new SimplePrimitiveNode("keywordTermURI", "keywordTermURI1");
         *  $motCle1["keywordValue"]                        = new SimplePrimitiveNode("keywordValue", "keywordValue1");
         *  $motCle1["keywordVocabulary"]                   = new SimplePrimitiveNode("keywordVocabulary", "keywordVocabulary1");
         *  $motCle1["keywordVocabularyURI"]                = new SimplePrimitiveNode("keywordVocabularyURI", "keywordVocabularyURI1");
         *  
         *  $motCle2 = array();
         *  $motCle2["keywordTermURI"]                      = new SimplePrimitiveNode("keywordTermURI", "keywordTermURI2");
         *  $motCle2["keywordValue"]                        = new SimplePrimitiveNode("keywordValue", "keywordValue2");
         *  $motCle2["keywordVocabulary"]                   = new SimplePrimitiveNode("keywordVocabulary", "keywordVocabulary2");
         *  $motCle2["keywordVocabularyURI"]                = new SimplePrimitiveNode("keywordVocabularyURI", "keywordVocabularyURI2");
         *  
         *  // je crée les producteurs du dataset
         *  $producteur1 = array();
         *  $producteur1["producerAbbreviation"]            = new SimplePrimitiveNode("producerAbbreviation", "producerAbbreviation1");
         *  $producteur1["producerAffiliation"]             = new SimplePrimitiveNode("producerAffiliation", "producerAffiliation1");
         *  $producteur1["producerLogoURL"]                 = new SimplePrimitiveNode("producerLogoURL", "producerLogoURL1");
         *  $producteur1["producerName"]                    = new SimplePrimitiveNode("producerName", "producerName1");
         *  $producteur1["producerURL"]                     = new SimplePrimitiveNode("producerURL", "producerURL1");
         *  
         *  $producteur2 = array();
         *  $producteur2["producerAbbreviation"]            = new SimplePrimitiveNode("producerAbbreviation", "producerAbbreviation2");
         *  $producteur2["producerAffiliation"]             = new SimplePrimitiveNode("producerAffiliation", "producerAffiliation2");
         *  $producteur2["producerLogoURL"]                 = new SimplePrimitiveNode("producerLogoURL", "producerLogoURL2");
         *  $producteur2["producerName"]                    = new SimplePrimitiveNode("producerName", "producerName2");
         *  $producteur2["producerURL"]                     = new SimplePrimitiveNode("producerURL", "producerURL2");
         *  
         *  // je crée la période du dataset
         *  $periode1 = array();
         *  $periode1["timePeriodCoveredStart"]             = new SimplePrimitiveNode("timePeriodCoveredStart", "2000-01-01");
         *  $periode1["timePeriodCoveredEnd"]               = new SimplePrimitiveNode("timePeriodCoveredEnd", "2000-01-02");
         *  
         *  $periode2 = array();
         *  $periode2["timePeriodCoveredStart"]             = new SimplePrimitiveNode("timePeriodCoveredStart", "2000-01-03");
         *  $periode2["timePeriodCoveredEnd"]               = new SimplePrimitiveNode("timePeriodCoveredEnd", "2000-01-04");
         *  
         *  // je crée le projet du dataset
         *  $projet = array();
         *  $projet["projectAcronym"]                       = new SimplePrimitiveNode("projectAcronym", "projectAcronym");
         *  $projet["projectId"]                            = new SimplePrimitiveNode("projectId", "projectId");
         *  $projet["projectTask"]                          = new SimplePrimitiveNode("projectTask", "projectTask");
         *  $projet["projectTitle"]                         = new SimplePrimitiveNode("projectTitle", "projectTitle");
         *  $projet["projectURL"]                           = new SimplePrimitiveNode("projectURL", "http://project.url");
         *  $projet["projectWorkPackage"]                   = new SimplePrimitiveNode("projectWorkPackage", "projectWP");
         *  
         *  // je crée les producteurs du dataset
         *  $publication1 = array();
         *  $publication1["publicationCitation"]            = new SimplePrimitiveNode("publicationCitation", "relatedPublicationCitation1");
         *  $publication1["publicationIDNumber"]            = new SimplePrimitiveNode("publicationIDNumber", "relatedPublicationIDNumber1");
         *  $publication1["publicationIDType"]              = new SimpleControlledVocabularyNode("publicationIDType", "ark");
         *  $publication1["publicationURL"]                 = new SimplePrimitiveNode("publicationURL", "http://related.publication.url1");
         *  
         *  $publication2 = array();
         *  $publication2["publicationCitation"]            = new SimplePrimitiveNode("publicationCitation", "relatedPublicationCitation2");
         *  $publication2["publicationIDNumber"]            = new SimplePrimitiveNode("publicationIDNumber", "relatedPublicationIDNumber2");
         *  $publication2["publicationIDType"]              = new SimpleControlledVocabularyNode("publicationIDType", "arXiv");
         *  $publication2["publicationURL"]                 = new SimplePrimitiveNode("publicationURL", "http://related.publication.url2");
         *  
         *  // je crée les relatedDataset du dataset
         *  $relatedDataset1 = array();
         *  $relatedDataset1["relatedDatasetCitation"]      = new SimplePrimitiveNode("relatedDatasetCitation", "relatedDatasetCitation1");
         *  $relatedDataset1["relatedDatasetIDNumber"]      = new SimplePrimitiveNode("relatedDatasetIDNumber", "relatedDatasetIDNumber1");
         *  $relatedDataset1["relatedDatasetIDType"]        = new SimpleControlledVocabularyNode("relatedDatasetIDType", "ark");
         *  $relatedDataset1["relatedDatasetURL"]           = new SimplePrimitiveNode("relatedDatasetURL", "http://related.dataset.url1");
         *  
         *  $relatedDataset2 = array();
         *  $relatedDataset2["relatedDatasetCitation"]      = new SimplePrimitiveNode("relatedDatasetCitation", "relatedDatasetCitation2");
         *  $relatedDataset2["relatedDatasetIDNumber"]      = new SimplePrimitiveNode("relatedDatasetIDNumber", "relatedDatasetIDNumber2");
         *  $relatedDataset2["relatedDatasetIDType"]        = new SimpleControlledVocabularyNode("relatedDatasetIDType", "doi");
         *  $relatedDataset2["relatedDatasetURL"]           = new SimplePrimitiveNode("relatedDatasetURL", "http://related.dataset.url2");
         *  
         *  // je crée la série du dataset
         *  $serie = array();
         *  $serie["seriesInformation"]                     = new SimplePrimitiveNode("seriesInformation", "seriesInformation");
         *  $serie["seriesName"]                            = new SimplePrimitiveNode("seriesName", "seriesName");
         *  
         *  // je crée les topics classifications du dataset
         *  $topicClassification1 = array();
         *  $topicClassification1["topicClassValue"]        = new SimplePrimitiveNode("topicClassValue", "topicClassValue1");
         *  $topicClassification1["topicClassVocab"]        = new SimplePrimitiveNode("topicClassVocab", "topicClassVocab1");
         *  $topicClassification1["topicClassVocabURI"]     = new SimplePrimitiveNode("topicClassVocabURI", "topicClassVocabURI1");
         *  
         *  $topicClassification2 = array();
         *  $topicClassification2["topicClassValue"]        = new SimplePrimitiveNode("topicClassValue", "topicClassValue2");
         *  $topicClassification2["topicClassVocab"]        = new SimplePrimitiveNode("topicClassVocab", "topicClassVocab2");
         *  $topicClassification2["topicClassVocabURI"]     = new SimplePrimitiveNode("topicClassVocabURI", "topicClassVocabURI2");
         *  
         *  // je crée un node citation
         *  $cn = new CitationNode();
         *  $cn->addField(new SimplePrimitiveNode("title", "title"));
         *  $cn->addField(new SimplePrimitiveNode("subtitle", "subtitle"));
         *  $cn->addField(new SimplePrimitiveNode("alternativeTitle", "alternativeTitle"));
         *  $cn->addField(new SimplePrimitiveNode("alternativeURL", "http://link.to.data"));
         *  $cn->addField(new MultipleCompoundNode("otherId", array($autreId1, $autreId2) ));
         *  $cn->addField(new MultipleCompoundNode("datasetContact", array($contact1, $contact2) ));
         *  $cn->addField(new MultipleCompoundNode("author", array($auteur1, $auteur2) ));
         *  $cn->addField(new MultipleCompoundNode("contributor", array($contributeur1, $contributeur2) ));
         *  $cn->addField(new MultipleCompoundNode("producer", array($producteur1, $producteur2) ));
         *  $cn->addField(new SimplePrimitiveNode("productionDate", "2000-01-01"));
         *  $cn->addField(new SimplePrimitiveNode("productionPlace", "productionPlace"));
         *  $cn->addField(new MultipleCompoundNode("distributor", array($distributeur1, $distributeur2) ));
         *  $cn->addField(new SimplePrimitiveNode("distributionDate", "2000-01-01"));
         *  $cn->addField(new MultipleCompoundNode("dsDescription", array($description1, $description2) ));
         *  $cn->addField(new MultipleControlledVocabularyNode("language",array("Abkhaz", "Afar") ));
         *  $cn->addField(new MultipleControlledVocabularyNode("subject",array("Animal Breeding and Animal Products", "Animal Health and Pathology") ));
         *  $cn->addField(new MultipleCompoundNode("keyword", array($motCle1, $motCle1) ));
         *  $cn->addField(new MultipleCompoundNode("topicClassification", array($topicClassification1, $topicClassification2) ));
         *  $cn->addField(new MultipleControlledVocabularyNode("kindOfData",array("Audiovisual", "Collection") ));
         *  $cn->addField(new MultiplePrimitiveNode("kindOfDataOther",array("otherKindOfData1", "otherKindOfData2") ));
         *  $cn->addField(new MultipleControlledVocabularyNode("dataOrigin",array("observational data", "experimental data") ));
         *  $cn->addField(new MultiplePrimitiveNode("dataSources",array("dataSource1", "dataSource2") ));
         *  $cn->addField(new SimplePrimitiveNode("originOfSources", "originOfSource"));
         *  $cn->addField(new SimplePrimitiveNode("characteristicOfSources", "characteristicOfSource"));
         *  $cn->addField(new SimplePrimitiveNode("accessToSources", "accessToSource"));
         *  $cn->addField(new MultipleCompoundNode("software", array($logiciel1, $logiciel2) ));
         *  $cn->addField(new SimpleCompoundNode("series", $serie));
         *  $cn->addField(new MultipleControlledVocabularyNode("lifeCycleStep",array("Study proposal", "Funding") ));
         *  $cn->addField(new SimplePrimitiveNode("notesText", "notes"));
         *  $cn->addField(new MultipleCompoundNode("publication", array($publication1, $publication2) ));
         *  $cn->addField(new MultiplePrimitiveNode("relatedMaterial",array("relatedMaterial1", "relatedMaterial2") ));
         *  $cn->addField(new MultipleCompoundNode("relatedDataset", array($relatedDataset1, $relatedDataset2) ));
         *  $cn->addField(new MultiplePrimitiveNode("otherReferences",array("otherReference1", "otherReference2") ));
         *  $cn->addField(new MultipleCompoundNode("grantNumber", array($grantNumber1, $grantNumber2) ));
         *  $cn->addField(new SimpleCompoundNode("project", $projet));
         *  $cn->addField(new MultipleCompoundNode("timePeriodCovered", array($periode1, $periode2) ));
         *  $cn->addField(new MultipleCompoundNode("dateOfCollection", array($dates1, $dates2) ));
         *  $cn->addField(new SimplePrimitiveNode("depositor", "depositor"));
         *  $cn->addField(new SimplePrimitiveNode("dateOfDeposit", "2000-01-01"));
         *  
         *  echo $cn.toJSON();
         *  </pre>
         */ 
        public static function howToBuildACitationNode()
        {
            echo PHP_EOL.'// je crée les auteurs du dataset';
            echo PHP_EOL.'$auteur1 = array();';
            echo PHP_EOL.'$auteur1["authorAffiliation"]                   = new SimplePrimitiveNode("authorAffiliation", "authorAffiliation1");';
            echo PHP_EOL.'$auteur1["authorIdentifier"]                    = new SimplePrimitiveNode("authorIdentifier", "authorIdentifier1");';
            echo PHP_EOL.'$auteur1["authorIdentifierScheme"]              = new SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID");';
            echo PHP_EOL.'$auteur1["authorName"]                          = new SimplePrimitiveNode("authorName", "authorName1");';
            echo PHP_EOL;
            echo PHP_EOL.'$auteur2 = array();';
            echo PHP_EOL.'$auteur2["authorAffiliation"]                   = new SimplePrimitiveNode("authorAffiliation", "authorAffiliation2");';
            echo PHP_EOL.'$auteur2["authorIdentifier"]                    = new SimplePrimitiveNode("authorIdentifier", "authorIdentifier2");';
            echo PHP_EOL.'$auteur2["authorIdentifierScheme"]              = new SimpleControlledVocabularyNode("authorIdentifierScheme", "idHAL");';
            echo PHP_EOL.'$auteur2["authorName"]                          = new SimplePrimitiveNode("authorName", "authorName2");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée les autres identifiants';
            echo PHP_EOL.'$autreId1 = array();';
            echo PHP_EOL.'$autreId1["otherIdAgency"]                      = new SimplePrimitiveNode("otherIdAgency", "otherIdAgency1");';
            echo PHP_EOL.'$autreId1["otherIdValue"]                       = new SimplePrimitiveNode("otherIdValue", "otherIdAgencyIdentifier1");';
            echo PHP_EOL;
            echo PHP_EOL.'$autreId2 = array();';
            echo PHP_EOL.'$autreId2["otherIdAgency"]                      = new SimplePrimitiveNode("otherIdAgency", "otherIdAgency2");';
            echo PHP_EOL.'$autreId2["otherIdValue"]                       = new SimplePrimitiveNode("otherIdValue", "otherIdAgencyIdentifier2");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée les contacts du dataset';
            echo PHP_EOL.'$contact1 = array();';
            echo PHP_EOL.'$contact1["datasetContactAffiliation"]          = new SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation1");';
            echo PHP_EOL.'$contact1["datasetContactEmail"]                = new SimplePrimitiveNode("datasetContactEmail", "contact1@mail.com");';
            echo PHP_EOL.'$contact1["datasetContactName"]                 = new SimplePrimitiveNode("datasetContactName", "contactName1");';
            echo PHP_EOL;
            echo PHP_EOL.'$contact2 = array();';
            echo PHP_EOL.'$contact2["datasetContactAffiliation"]          = new SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation2");';
            echo PHP_EOL.'$contact2["datasetContactEmail"]                = new SimplePrimitiveNode("datasetContactEmail", "contact2@mail.com");';
            echo PHP_EOL.'$contact2["datasetContactName"]                 = new SimplePrimitiveNode("datasetContactName", "contactName2");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée les contributeurs du dataset';
            echo PHP_EOL.'$contributeur1 = array();';
            echo PHP_EOL.'$contributeur1["contributorAffiliation"]        = new SimplePrimitiveNode("contributorAffiliation", "contributorAffiliation1");';
            echo PHP_EOL.'$contributeur1["contributorIdentifier"]         = new SimplePrimitiveNode("contributorIdentifier", "contributorIdentifier1");';
            echo PHP_EOL.'$contributeur1["contributorIdentifierScheme"]   = new SimpleControlledVocabularyNode("contributorIdentifierScheme", "ORCID");';
            echo PHP_EOL.'$contributeur1["contributorName"]               = new SimplePrimitiveNode("contributorName", "contributorName1");';
            echo PHP_EOL.'$contributeur1["contributorType"]               = new SimpleControlledVocabularyNode("contributorType", "Data collector");';
            echo PHP_EOL;
            echo PHP_EOL.'$contributeur2 = array();';
            echo PHP_EOL.'$contributeur2["contributorAffiliation"]        = new SimplePrimitiveNode("contributorAffiliation", "contributorAffiliation2");';
            echo PHP_EOL.'$contributeur2["contributorIdentifier"]         = new SimplePrimitiveNode("contributorIdentifier", "contributorIdentifier2");';
            echo PHP_EOL.'$contributeur2["contributorIdentifierScheme"]   = new SimpleControlledVocabularyNode("contributorIdentifierScheme", "ORCID");';
            echo PHP_EOL.'$contributeur2["contributorName"]               = new SimplePrimitiveNode("contributorName", "contributorName2");';
            echo PHP_EOL.'$contributeur2["contributorType"]               = new SimpleControlledVocabularyNode("contributorType", "Editor");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée les dates des collections du dataset';
            echo PHP_EOL.'$dates1 = array();';
            echo PHP_EOL.'$dates1["dateOfCollectionStart"]                = new SimplePrimitiveNode("dateOfCollectionStart", "2000-01-01");';
            echo PHP_EOL.'$dates1["dateOfCollectionEnd"]                  = new SimplePrimitiveNode("dateOfCollectionEnd", "2000-01-02");';
            echo PHP_EOL;
            echo PHP_EOL.'$dates2 = array();';
            echo PHP_EOL.'$dates2["dateOfCollectionStart"]                = new SimplePrimitiveNode("dateOfCollectionStart", "2000-01-03");';
            echo PHP_EOL.'$dates2["dateOfCollectionEnd"]                  = new SimplePrimitiveNode("dateOfCollectionEnd", "2000-01-04");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée les descriptions du dataset';
            echo PHP_EOL.'$description1 = array();';
            echo PHP_EOL.'$description1["dsDescriptionDate"]              = new SimplePrimitiveNode("dsDescriptionDate", "2000-01-01");';
            echo PHP_EOL.'$description1["dsDescriptionValue"]             = new SimplePrimitiveNode("dsDescriptionValue", "descriptionText1");';
            echo PHP_EOL;
            echo PHP_EOL.'$description2 = array();';
            echo PHP_EOL.'$description2["dsDescriptionDate"]              = new SimplePrimitiveNode("dsDescriptionDate", "2000-01-01");';
            echo PHP_EOL.'$description2["dsDescriptionValue"]             = new SimplePrimitiveNode("dsDescriptionValue", "descriptionText2");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée les distributeurs du dataset';
            echo PHP_EOL.'$distributeur1 = array();';
            echo PHP_EOL.'$distributeur1["distributorAbbreviation"]       = new SimplePrimitiveNode("distributorAbbreviation", "distributorAbbreviation1");';
            echo PHP_EOL.'$distributeur1["distributorAffiliation"]        = new SimplePrimitiveNode("distributorAffiliation", "distributorAffiliation1");';
            echo PHP_EOL.'$distributeur1["distributorLogoURL"]            = new SimplePrimitiveNode("distributorLogoURL", "distributorLogoURL1");';
            echo PHP_EOL.'$distributeur1["distributorName"]               = new SimplePrimitiveNode("distributorName", "distributorName1");';
            echo PHP_EOL.'$distributeur1["distributorURL"]                = new SimplePrimitiveNode("distributorURL", "distributorURL1");';
            echo PHP_EOL;
            echo PHP_EOL.'$distributeur2 = array();';
            echo PHP_EOL.'$distributeur2["distributorAbbreviation"]       = new SimplePrimitiveNode("distributorAbbreviation", "distributorAbbreviation2");';
            echo PHP_EOL.'$distributeur2["distributorAffiliation"]        = new SimplePrimitiveNode("distributorAffiliation", "distributorAffiliation2");';
            echo PHP_EOL.'$distributeur2["distributorLogoURL"]            = new SimplePrimitiveNode("distributorLogoURL", "distributorLogoURL2");';
            echo PHP_EOL.'$distributeur2["distributorName"]               = new SimplePrimitiveNode("distributorName", "distributorName2");';
            echo PHP_EOL.'$distributeur2["distributorURL"]                = new SimplePrimitiveNode("distributorURL", "distributorURL2");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée les grant number du dataset';
            echo PHP_EOL.'$grantNumber1 = array();';
            echo PHP_EOL.'$grantNumber1["grantNumberAgency"]              = new SimplePrimitiveNode("grantNumberAgency", "grantAgency1");';
            echo PHP_EOL.'$grantNumber1["grantNumberValue"]               = new SimplePrimitiveNode("grantNumberValue", "grantNumber1");';
            echo PHP_EOL;
            echo PHP_EOL.'$grantNumber2 = array();';
            echo PHP_EOL.'$grantNumber2["grantNumberAgency"]              = new SimplePrimitiveNode("grantNumberAgency", "grantAgency2");';
            echo PHP_EOL.'$grantNumber2["grantNumberValue"]               = new SimplePrimitiveNode("grantNumberValue", "grantNumber2");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée les logiciels du dataset';
            echo PHP_EOL.'$logiciel1 = array();';
            echo PHP_EOL.'$logiciel1["softwareName"]                      = new SimplePrimitiveNode("softwareName", "softwareName1");';
            echo PHP_EOL.'$logiciel1["softwareVersion"]                   = new SimplePrimitiveNode("softwareVersion", "softwareVersion1");';
            echo PHP_EOL;
            echo PHP_EOL.'$logiciel2 = array();';
            echo PHP_EOL.'$logiciel2["softwareName"]                      = new SimplePrimitiveNode("softwareName", "softwareName2");';
            echo PHP_EOL.'$logiciel2["softwareVersion"]                   = new SimplePrimitiveNode("softwareVersion", "softwareVersion2");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée les mots clés du dataset';
            echo PHP_EOL.'$motCle1 = array();';
            echo PHP_EOL.'$motCle1["keywordTermURI"]                      = new SimplePrimitiveNode("keywordTermURI", "keywordTermURI1");';
            echo PHP_EOL.'$motCle1["keywordValue"]                        = new SimplePrimitiveNode("keywordValue", "keywordValue1");';
            echo PHP_EOL.'$motCle1["keywordVocabulary"]                   = new SimplePrimitiveNode("keywordVocabulary", "keywordVocabulary1");';
            echo PHP_EOL.'$motCle1["keywordVocabularyURI"]                = new SimplePrimitiveNode("keywordVocabularyURI", "keywordVocabularyURI1");';
            echo PHP_EOL;
            echo PHP_EOL.'$motCle2 = array();';
            echo PHP_EOL.'$motCle2["keywordTermURI"]                      = new SimplePrimitiveNode("keywordTermURI", "keywordTermURI2");';
            echo PHP_EOL.'$motCle2["keywordValue"]                        = new SimplePrimitiveNode("keywordValue", "keywordValue2");';
            echo PHP_EOL.'$motCle2["keywordVocabulary"]                   = new SimplePrimitiveNode("keywordVocabulary", "keywordVocabulary2");';
            echo PHP_EOL.'$motCle2["keywordVocabularyURI"]                = new SimplePrimitiveNode("keywordVocabularyURI", "keywordVocabularyURI2");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée les producteurs du dataset';
            echo PHP_EOL.'$producteur1 = array();';
            echo PHP_EOL.'$producteur1["producerAbbreviation"]            = new SimplePrimitiveNode("producerAbbreviation", "producerAbbreviation1");';
            echo PHP_EOL.'$producteur1["producerAffiliation"]             = new SimplePrimitiveNode("producerAffiliation", "producerAffiliation1");';
            echo PHP_EOL.'$producteur1["producerLogoURL"]                 = new SimplePrimitiveNode("producerLogoURL", "producerLogoURL1");';
            echo PHP_EOL.'$producteur1["producerName"]                    = new SimplePrimitiveNode("producerName", "producerName1");';
            echo PHP_EOL.'$producteur1["producerURL"]                     = new SimplePrimitiveNode("producerURL", "producerURL1");';
            echo PHP_EOL;
            echo PHP_EOL.'$producteur2 = array();';
            echo PHP_EOL.'$producteur2["producerAbbreviation"]            = new SimplePrimitiveNode("producerAbbreviation", "producerAbbreviation2");';
            echo PHP_EOL.'$producteur2["producerAffiliation"]             = new SimplePrimitiveNode("producerAffiliation", "producerAffiliation2");';
            echo PHP_EOL.'$producteur2["producerLogoURL"]                 = new SimplePrimitiveNode("producerLogoURL", "producerLogoURL2");';
            echo PHP_EOL.'$producteur2["producerName"]                    = new SimplePrimitiveNode("producerName", "producerName2");';
            echo PHP_EOL.'$producteur2["producerURL"]                     = new SimplePrimitiveNode("producerURL", "producerURL2");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée la période du dataset';
            echo PHP_EOL.'$periode1 = array();';
            echo PHP_EOL.'$periode1["timePeriodCoveredStart"]             = new SimplePrimitiveNode("timePeriodCoveredStart", "2000-01-01");';
            echo PHP_EOL.'$periode1["timePeriodCoveredEnd"]               = new SimplePrimitiveNode("timePeriodCoveredEnd", "2000-01-02");';
            echo PHP_EOL;
            echo PHP_EOL.'$periode2 = array();';
            echo PHP_EOL.'$periode2["timePeriodCoveredStart"]             = new SimplePrimitiveNode("timePeriodCoveredStart", "2000-01-03");';
            echo PHP_EOL.'$periode2["timePeriodCoveredEnd"]               = new SimplePrimitiveNode("timePeriodCoveredEnd", "2000-01-04");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée le projet du dataset';
            echo PHP_EOL.'$projet = array();';
            echo PHP_EOL.'$projet["projectAcronym"]                       = new SimplePrimitiveNode("projectAcronym", "projectAcronym");';
            echo PHP_EOL.'$projet["projectId"]                            = new SimplePrimitiveNode("projectId", "projectId");';
            echo PHP_EOL.'$projet["projectTask"]                          = new SimplePrimitiveNode("projectTask", "projectTask");';
            echo PHP_EOL.'$projet["projectTitle"]                         = new SimplePrimitiveNode("projectTitle", "projectTitle");';
            echo PHP_EOL.'$projet["projectURL"]                           = new SimplePrimitiveNode("projectURL", "http://project.url");';
            echo PHP_EOL.'$projet["projectWorkPackage"]                   = new SimplePrimitiveNode("projectWorkPackage", "projectWP");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée les producteurs du dataset';
            echo PHP_EOL.'$publication1 = array();';
            echo PHP_EOL.'$publication1["publicationCitation"]            = new SimplePrimitiveNode("publicationCitation", "relatedPublicationCitation1");';
            echo PHP_EOL.'$publication1["publicationIDNumber"]            = new SimplePrimitiveNode("publicationIDNumber", "relatedPublicationIDNumber1");';
            echo PHP_EOL.'$publication1["publicationIDType"]              = new SimpleControlledVocabularyNode("publicationIDType", "ark");';
            echo PHP_EOL.'$publication1["publicationURL"]                 = new SimplePrimitiveNode("publicationURL", "http://related.publication.url1");';
            echo PHP_EOL;
            echo PHP_EOL.'$publication2 = array();';
            echo PHP_EOL.'$publication2["publicationCitation"]            = new SimplePrimitiveNode("publicationCitation", "relatedPublicationCitation2");';
            echo PHP_EOL.'$publication2["publicationIDNumber"]            = new SimplePrimitiveNode("publicationIDNumber", "relatedPublicationIDNumber2");';
            echo PHP_EOL.'$publication2["publicationIDType"]              = new SimpleControlledVocabularyNode("publicationIDType", "arXiv");';
            echo PHP_EOL.'$publication2["publicationURL"]                 = new SimplePrimitiveNode("publicationURL", "http://related.publication.url2");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée les relatedDataset du dataset';
            echo PHP_EOL.'$relatedDataset1 = array();';
            echo PHP_EOL.'$relatedDataset1["relatedDatasetCitation"]      = new SimplePrimitiveNode("relatedDatasetCitation", "relatedDatasetCitation1");';
            echo PHP_EOL.'$relatedDataset1["relatedDatasetIDNumber"]      = new SimplePrimitiveNode("relatedDatasetIDNumber", "relatedDatasetIDNumber1");';
            echo PHP_EOL.'$relatedDataset1["relatedDatasetIDType"]        = new SimpleControlledVocabularyNode("relatedDatasetIDType", "ark");';
            echo PHP_EOL.'$relatedDataset1["relatedDatasetURL"]           = new SimplePrimitiveNode("relatedDatasetURL", "http://related.dataset.url1");';
            echo PHP_EOL;
            echo PHP_EOL.'$relatedDataset2 = array();';
            echo PHP_EOL.'$relatedDataset2["relatedDatasetCitation"]      = new SimplePrimitiveNode("relatedDatasetCitation", "relatedDatasetCitation2");';
            echo PHP_EOL.'$relatedDataset2["relatedDatasetIDNumber"]      = new SimplePrimitiveNode("relatedDatasetIDNumber", "relatedDatasetIDNumber2");';
            echo PHP_EOL.'$relatedDataset2["relatedDatasetIDType"]        = new SimpleControlledVocabularyNode("relatedDatasetIDType", "doi");';
            echo PHP_EOL.'$relatedDataset2["relatedDatasetURL"]           = new SimplePrimitiveNode("relatedDatasetURL", "http://related.dataset.url2");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée la série du dataset';
            echo PHP_EOL.'$serie = array();';
            echo PHP_EOL.'$serie["seriesInformation"]                     = new SimplePrimitiveNode("seriesInformation", "seriesInformation");';
            echo PHP_EOL.'$serie["seriesName"]                            = new SimplePrimitiveNode("seriesName", "seriesName");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée les topics classifications du dataset';
            echo PHP_EOL.'$topicClassification1 = array();';
            echo PHP_EOL.'$topicClassification1["topicClassValue"]        = new SimplePrimitiveNode("topicClassValue", "topicClassValue1");';
            echo PHP_EOL.'$topicClassification1["topicClassVocab"]        = new SimplePrimitiveNode("topicClassVocab", "topicClassVocab1");';
            echo PHP_EOL.'$topicClassification1["topicClassVocabURI"]     = new SimplePrimitiveNode("topicClassVocabURI", "topicClassVocabURI1");';
            echo PHP_EOL;
            echo PHP_EOL.'$topicClassification2 = array();';
            echo PHP_EOL.'$topicClassification2["topicClassValue"]        = new SimplePrimitiveNode("topicClassValue", "topicClassValue2");';
            echo PHP_EOL.'$topicClassification2["topicClassVocab"]        = new SimplePrimitiveNode("topicClassVocab", "topicClassVocab2");';
            echo PHP_EOL.'$topicClassification2["topicClassVocabURI"]     = new SimplePrimitiveNode("topicClassVocabURI", "topicClassVocabURI2");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée un node citation';
            echo PHP_EOL.'$cn = new CitationNode();';
            echo PHP_EOL.'$cn->addField(new SimplePrimitiveNode("title", "title"));';
            echo PHP_EOL.'$cn->addField(new SimplePrimitiveNode("subtitle", "subtitle"));';
            echo PHP_EOL.'$cn->addField(new SimplePrimitiveNode("alternativeTitle", "alternativeTitle"));';
            echo PHP_EOL.'$cn->addField(new SimplePrimitiveNode("alternativeURL", "http://link.to.data"));';
            echo PHP_EOL.'$cn->addField(new MultipleCompoundNode("otherId", array($autreId1, $autreId2) ));';
            echo PHP_EOL.'$cn->addField(new MultipleCompoundNode("datasetContact", array($contact1, $contact2) ));';
            echo PHP_EOL.'$cn->addField(new MultipleCompoundNode("author", array($auteur1, $auteur2) ));';
            echo PHP_EOL.'$cn->addField(new MultipleCompoundNode("contributor", array($contributeur1, $contributeur2) ));';
            echo PHP_EOL.'$cn->addField(new MultipleCompoundNode("producer", array($producteur1, $producteur2) ));';
            echo PHP_EOL.'$cn->addField(new SimplePrimitiveNode("productionDate", "2000-01-01"));';
            echo PHP_EOL.'$cn->addField(new SimplePrimitiveNode("productionPlace", "productionPlace"));';
            echo PHP_EOL.'$cn->addField(new MultipleCompoundNode("distributor", array($distributeur1, $distributeur2) ));';
            echo PHP_EOL.'$cn->addField(new SimplePrimitiveNode("distributionDate", "2000-01-01"));';
            echo PHP_EOL.'$cn->addField(new MultipleCompoundNode("dsDescription", array($description1, $description2) ));';
            echo PHP_EOL.'$cn->addField(new MultipleControlledVocabularyNode("language",array("Abkhaz", "Afar") ));';
            echo PHP_EOL.'$cn->addField(new MultipleControlledVocabularyNode("subject",array("Animal Breeding and Animal Products", "Animal Health and Pathology") ));';
            echo PHP_EOL.'$cn->addField(new MultipleCompoundNode("keyword", array($motCle1, $motCle1) ));';
            echo PHP_EOL.'$cn->addField(new MultipleCompoundNode("topicClassification", array($topicClassification1, $topicClassification2) ));';
            echo PHP_EOL.'$cn->addField(new MultipleControlledVocabularyNode("kindOfData",array("Audiovisual", "Collection") ));';
            echo PHP_EOL.'$cn->addField(new MultiplePrimitiveNode("kindOfDataOther",array("otherKindOfData1", "otherKindOfData2") ));';
            echo PHP_EOL.'$cn->addField(new MultipleControlledVocabularyNode("dataOrigin",array("observational data", "experimental data") ));';
            echo PHP_EOL.'$cn->addField(new MultiplePrimitiveNode("dataSources",array("dataSource1", "dataSource2") ));';
            echo PHP_EOL.'$cn->addField(new SimplePrimitiveNode("originOfSources", "originOfSource"));';
            echo PHP_EOL.'$cn->addField(new SimplePrimitiveNode("characteristicOfSources", "characteristicOfSource"));';
            echo PHP_EOL.'$cn->addField(new SimplePrimitiveNode("accessToSources", "accessToSource"));';
            echo PHP_EOL.'$cn->addField(new MultipleCompoundNode("software", array($logiciel1, $logiciel2) ));';
            echo PHP_EOL.'$cn->addField(new SimpleCompoundNode("series", $serie));';
            echo PHP_EOL.'$cn->addField(new MultipleControlledVocabularyNode("lifeCycleStep",array("Study proposal", "Funding") ));';
            echo PHP_EOL.'$cn->addField(new SimplePrimitiveNode("notesText", "notes"));';
            echo PHP_EOL.'$cn->addField(new MultipleCompoundNode("publication", array($publication1, $publication2) ));';
            echo PHP_EOL.'$cn->addField(new MultiplePrimitiveNode("relatedMaterial",array("relatedMaterial1", "relatedMaterial2") ));';
            echo PHP_EOL.'$cn->addField(new MultipleCompoundNode("relatedDataset", array($relatedDataset1, $relatedDataset2) ));';
            echo PHP_EOL.'$cn->addField(new MultiplePrimitiveNode("otherReferences",array("otherReference1", "otherReference2") ));';
            echo PHP_EOL.'$cn->addField(new MultipleCompoundNode("grantNumber", array($grantNumber1, $grantNumber2) ));';
            echo PHP_EOL.'$cn->addField(new SimpleCompoundNode("project", $projet));';
            echo PHP_EOL.'$cn->addField(new MultipleCompoundNode("timePeriodCovered", array($periode1, $periode2) ));';
            echo PHP_EOL.'$cn->addField(new MultipleCompoundNode("dateOfCollection", array($dates1, $dates2) ));';
            echo PHP_EOL.'$cn->addField(new SimplePrimitiveNode("depositor", "depositor"));';
            echo PHP_EOL.'$cn->addField(new SimplePrimitiveNode("dateOfDeposit", "2000-01-01"));';
            echo PHP_EOL;
            echo PHP_EOL.'echo $cn.toJSON();';
            echo PHP_EOL;
        }

        /** <c>howToBuildADerivedTextNode</c> montre la manière de construire un <c>DerivedTextNode</c>
         * 
         *  <hr>
         *  <strong>Exemple de code : </strong>
         *  <pre>
         *  // je crée les sources du dataset
         *  $source1 = array();
         *  $source1["ageOfSource"]         = new MultiplePrimitiveNode("ageOfSource", array("sourceAge1"));
         *  $source1["citations"]           = new MultiplePrimitiveNode("citations", array("1"));
         *  $source1["experimentNumber"]    = new MultiplePrimitiveNode("experimentNumber", array("1"));
         *  $source1["typeOfSource"]        = new MultiplePrimitiveNode("typeOfSource", array("sourceType1"));
         *  
         *  $source2 = array();
         *  $source2["ageOfSource"]         = new MultiplePrimitiveNode("ageOfSource", array("sourceAge2"));
         *  $source2["citations"]           = new MultiplePrimitiveNode("citations", array("2"));
         *  $source2["experimentNumber"]    = new MultiplePrimitiveNode("experimentNumber", array("2"));
         *  $source2["typeOfSource"]        = new MultiplePrimitiveNode("typeOfSource", array("sourceType2"));
         *  
         *  // je crée un node derived-text
         *  $dtn = new DerivedTextNode();
         *  $dtn->addField(new MultipleCompoundNode("source", array($source1, $source2) ));
         *          
         *  echo $dtn->toJSON();
         *  </pre>
         */ 
        public static function howToBuildADerivedTextNode()
        {
            echo PHP_EOL.'// je crée les sources du dataset';
            echo PHP_EOL.'$source1 = array();';
            echo PHP_EOL.'$source1["ageOfSource"]         = new MultiplePrimitiveNode("ageOfSource", array("sourceAge1"));';
            echo PHP_EOL.'$source1["citations"]           = new MultiplePrimitiveNode("citations", array("1"));';
            echo PHP_EOL.'$source1["experimentNumber"]    = new MultiplePrimitiveNode("experimentNumber", array("1"));';
            echo PHP_EOL.'$source1["typeOfSource"]        = new MultiplePrimitiveNode("typeOfSource", array("sourceType1"));';
            echo PHP_EOL;
            echo PHP_EOL.'$source2 = array();';
            echo PHP_EOL.'$source2["ageOfSource"]         = new MultiplePrimitiveNode("ageOfSource", array("sourceAge2"));';
            echo PHP_EOL.'$source2["citations"]           = new MultiplePrimitiveNode("citations", array("2"));';
            echo PHP_EOL.'$source2["experimentNumber"]    = new MultiplePrimitiveNode("experimentNumber", array("2"));';
            echo PHP_EOL.'$source2["typeOfSource"]        = new MultiplePrimitiveNode("typeOfSource", array("sourceType2"));';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée un node derived-text';
            echo PHP_EOL.'$dtn = new DerivedTextNode();';
            echo PHP_EOL.'$dtn->addField(new MultipleCompoundNode("source", array($source1, $source2) ));';
            echo PHP_EOL;        
            echo PHP_EOL.'echo $dtn->toJSON();';
        }

        /** <c>howToBuildAGeospatialNode</c> montre la manière de construire un <c>GeospatialNode</c>
         * 
         *  <hr>
         *  <strong>Exemple de code : </strong>
         *  <pre>
         *  // je crée la boite geographique du dataset
         *  $boite1 = array();
         *  $boite1["eastLongitude"]                = new SimplePrimitiveNode("eastLongitude", "0");
         *  $boite1["northLongitude"]               = new SimplePrimitiveNode("northLongitude", "1");
         *  $boite1["southLongitude"]               = new SimplePrimitiveNode("southLongitude", "2");
         *  $boite1["westLongitude"]                = new SimplePrimitiveNode("westLongitude", "3");
         *  
         *  $boite2 = array();
         *  $boite2["eastLongitude"]                = new SimplePrimitiveNode("eastLongitude", "4");
         *  $boite2["northLongitude"]               = new SimplePrimitiveNode("northLongitude", "5");
         *  $boite2["southLongitude"]               = new SimplePrimitiveNode("southLongitude", "6");
         *  $boite2["westLongitude"]                = new SimplePrimitiveNode("westLongitude", "7");
         *  
         *  // je crée la conformite du dataset
         *  $conformite1 = array();
         *  $conformite1["specification"]           = new MultiplePrimitiveNode("specification", array("conformitySpecification1"));
         *  $conformite1["degree"]                  = new SimpleControlledVocabularyNode("degree", "Conformant");
         *  
         *  $conformite2 = array();
         *  $conformite2["specification"]           = new MultiplePrimitiveNode("specification", array("conformitySpecification2"));
         *  $conformite2["degree"]                  = new SimpleControlledVocabularyNode("degree", "Not Conformant");
         *  
         *  // je crée la couverture geographique du dataset
         *  $couverture1 = array();
         *  $couverture1["city"]                    = new SimplePrimitiveNode("city", "geographicCoverageCity1");
         *  $couverture1["country"]                 = new SimpleControlledVocabularyNode("country", "Afghanistan");
         *  $couverture1["otherGeographicCoverage"] = new SimplePrimitiveNode("otherGeographicCoverage", "geographicCoverageOther1");
         *  $couverture1["state"]                   = new SimplePrimitiveNode("state", "geographicCoverageProvince1");
         *  
         *  $couverture2 = array();
         *  $couverture2["city"]                    = new SimplePrimitiveNode("city", "geographicCoverageCity2");
         *  $couverture2["country"]                 = new SimpleControlledVocabularyNode("country", "Albania");
         *  $couverture2["otherGeographicCoverage"] = new SimplePrimitiveNode("otherGeographicCoverage", "geographicCoverageOther2");
         *  $couverture2["state"]                   = new SimplePrimitiveNode("state", "geographicCoverageProvince2");
         *  
         *  // je crée la qualité du dataset
         *  $qualite1 = array();
         *  $qualite1["lineage"]                    = new MultiplePrimitiveNode("lineage", array("QualityAndValidityLineage1"));
         *  $qualite1["spatialResolution"]          = new MultiplePrimitiveNode("lineage", array("QualityAndValiditySpatialResolution1"));
         *  
         *  $qualite2 = array();
         *  $qualite2["lineage"]                    = new MultiplePrimitiveNode("lineage", array("QualityAndValidityLineage2"));
         *  $qualite2["spatialResolution"]          = new MultiplePrimitiveNode("lineage", array("QualityAndValiditySpatialResolution2"));
         *  
         *  // je crée un node geospatial
         *  $gn = new GeospatialNode();
         *  $gn->addField(new MultipleCompoundNode("geographicCoverage", array($couverture1, $couverture2)));
         *  $gn->addField(new MultiplePrimitiveNode("geographicUnit", array("geographicUnit1", "geographicUnit2")));
         *  $gn->addField(new MultipleCompoundNode("geographicBoundingBox", array($boite1, $boite2)));
         *  $gn->addField(new MultipleCompoundNode("qualityValidity", array($qualite1, $qualite2)));
         *  $gn->addField(new MultipleCompoundNode("conformity", array($conformite1, $conformite2)));
         *  
         *  echo $gn->toJSON();
         *  </pre>
         */ 
        public static function howToBuildAGeospatialNode()
        {
            echo PHP_EOL.'// je crée la boite geographique du dataset';
            echo PHP_EOL.'$boite1 = array();';
            echo PHP_EOL.'$boite1["eastLongitude"]                = new SimplePrimitiveNode("eastLongitude", "0");';
            echo PHP_EOL.'$boite1["northLongitude"]               = new SimplePrimitiveNode("northLongitude", "1");';
            echo PHP_EOL.'$boite1["southLongitude"]               = new SimplePrimitiveNode("southLongitude", "2");';
            echo PHP_EOL.'$boite1["westLongitude"]                = new SimplePrimitiveNode("westLongitude", "3");';
            echo PHP_EOL;
            echo PHP_EOL.'$boite2 = array();';
            echo PHP_EOL.'$boite2["eastLongitude"]                = new SimplePrimitiveNode("eastLongitude", "4");';
            echo PHP_EOL.'$boite2["northLongitude"]               = new SimplePrimitiveNode("northLongitude", "5");';
            echo PHP_EOL.'$boite2["southLongitude"]               = new SimplePrimitiveNode("southLongitude", "6");';
            echo PHP_EOL.'$boite2["westLongitude"]                = new SimplePrimitiveNode("westLongitude", "7");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée la conformite du dataset';
            echo PHP_EOL.'$conformite1 = array();';
            echo PHP_EOL.'$conformite1["specification"]           = new MultiplePrimitiveNode("specification", array("conformitySpecification1"));';
            echo PHP_EOL.'$conformite1["degree"]                  = new SimpleControlledVocabularyNode("degree", "Conformant");';
            echo PHP_EOL;
            echo PHP_EOL.'$conformite2 = array();';
            echo PHP_EOL.'$conformite2["specification"]           = new MultiplePrimitiveNode("specification", array("conformitySpecification2"));';
            echo PHP_EOL.'$conformite2["degree"]                  = new SimpleControlledVocabularyNode("degree", "Not Conformant");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée la couverture geographique du dataset';
            echo PHP_EOL.'$couverture1 = array();';
            echo PHP_EOL.'$couverture1["city"]                    = new SimplePrimitiveNode("city", "geographicCoverageCity1");';
            echo PHP_EOL.'$couverture1["country"]                 = new SimpleControlledVocabularyNode("country", "Afghanistan");';
            echo PHP_EOL.'$couverture1["otherGeographicCoverage"] = new SimplePrimitiveNode("otherGeographicCoverage", "geographicCoverageOther1");';
            echo PHP_EOL.'$couverture1["state"]                   = new SimplePrimitiveNode("state", "geographicCoverageProvince1");';
            echo PHP_EOL;
            echo PHP_EOL.'$couverture2 = array();';
            echo PHP_EOL.'$couverture2["city"]                    = new SimplePrimitiveNode("city", "geographicCoverageCity2");';
            echo PHP_EOL.'$couverture2["country"]                 = new SimpleControlledVocabularyNode("country", "Albania");';
            echo PHP_EOL.'$couverture2["otherGeographicCoverage"] = new SimplePrimitiveNode("otherGeographicCoverage", "geographicCoverageOther2");';
            echo PHP_EOL.'$couverture2["state"]                   = new SimplePrimitiveNode("state", "geographicCoverageProvince2");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée la qualité du dataset';
            echo PHP_EOL.'$qualite1 = array();';
            echo PHP_EOL.'$qualite1["lineage"]                    = new MultiplePrimitiveNode("lineage", array("QualityAndValidityLineage1"));';
            echo PHP_EOL.'$qualite1["spatialResolution"]          = new MultiplePrimitiveNode("lineage", array("QualityAndValiditySpatialResolution1"));';
            echo PHP_EOL;
            echo PHP_EOL.'$qualite2 = array();';
            echo PHP_EOL.'$qualite2["lineage"]                    = new MultiplePrimitiveNode("lineage", array("QualityAndValidityLineage2"));';
            echo PHP_EOL.'$qualite2["spatialResolution"]          = new MultiplePrimitiveNode("lineage", array("QualityAndValiditySpatialResolution2"));';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée un node geospatial';
            echo PHP_EOL.'$gn = new GeospatialNode();';
            echo PHP_EOL.'$gn->addField(new MultipleCompoundNode("geographicCoverage", array($couverture1, $couverture2)));';
            echo PHP_EOL.'$gn->addField(new MultiplePrimitiveNode("geographicUnit", array("geographicUnit1", "geographicUnit2")));';
            echo PHP_EOL.'$gn->addField(new MultipleCompoundNode("geographicBoundingBox", array($boite1, $boite2)));';
            echo PHP_EOL.'$gn->addField(new MultipleCompoundNode("qualityValidity", array($qualite1, $qualite2)));';
            echo PHP_EOL.'$gn->addField(new MultipleCompoundNode("conformity", array($conformite1, $conformite2)));';
            echo PHP_EOL;
            echo PHP_EOL.'echo $gn->toJSON();';
            echo PHP_EOL;
        }   

        /** <c>howToBuildAJournalNode</c> montre la manière de construire un <c>JournalNode</c>
         * 
         *  <hr>
         *  <strong>Exemple de code : </strong>
         *  <pre>
         *  // je crée les support de publication du dataset
         *  $journalVolumeIssue1 = array();
         *  $journalVolumeIssue1["journalIssue"]    = new SimplePrimitiveNode("journalIssue", "journalIssue1");
         *  $journalVolumeIssue1["journalPubDate"]  = new SimplePrimitiveNode("journalPubDate", "2000-01-01");
         *  $journalVolumeIssue1["journalVolume"]   = new SimplePrimitiveNode("journalVolume", "journalVolume1");
         *  
         *  $journalVolumeIssue2 = array();
         *  $journalVolumeIssue2["journalIssue"]    = new SimplePrimitiveNode("journalIssue", "journalIssue2");
         *  $journalVolumeIssue2["journalPubDate"]  = new SimplePrimitiveNode("journalPubDate", "2000-01-02");
         *  $journalVolumeIssue2["journalVolume"]   = new SimplePrimitiveNode("journalVolume", "journalVolume2");
         *  
         *  // je crée un node journal
         *  $jn = new JournalNode();
         *  $jn->addField(new MultipleCompoundNode("journalVolumeIssue", array($journalVolumeIssue1, $journalVolumeIssue2)));
         *  $jn->addField(new SimpleControlledVocabularyNode("journalArticleType", "abstract"));
         *  
         *  echo $jn->toJSON();
         *  </pre>
         */ 
        public static function howToBuildAJournalNode()
        {
            echo PHP_EOL.'// je crée les support de publication du dataset';
            echo PHP_EOL.'$journalVolumeIssue1 = array();';
            echo PHP_EOL.'$journalVolumeIssue1["journalIssue"]    = new SimplePrimitiveNode("journalIssue", "journalIssue1");';
            echo PHP_EOL.'$journalVolumeIssue1["journalPubDate"]  = new SimplePrimitiveNode("journalPubDate", "2000-01-01");';
            echo PHP_EOL.'$journalVolumeIssue1["journalVolume"]   = new SimplePrimitiveNode("journalVolume", "journalVolume1");';
            echo PHP_EOL;
            echo PHP_EOL.'$journalVolumeIssue2 = array();';
            echo PHP_EOL.'$journalVolumeIssue2["journalIssue"]    = new SimplePrimitiveNode("journalIssue", "journalIssue2");';
            echo PHP_EOL.'$journalVolumeIssue2["journalPubDate"]  = new SimplePrimitiveNode("journalPubDate", "2000-01-02");';
            echo PHP_EOL.'$journalVolumeIssue2["journalVolume"]   = new SimplePrimitiveNode("journalVolume", "journalVolume2");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée un node journal';
            echo PHP_EOL.'$jn = new JournalNode();';
            echo PHP_EOL.'$jn->addField(new MultipleCompoundNode("journalVolumeIssue", array($journalVolumeIssue1, $journalVolumeIssue2)));';
            echo PHP_EOL.'$jn->addField(new SimpleControlledVocabularyNode("journalArticleType", "abstract"));';
            echo PHP_EOL;
            echo PHP_EOL.'echo $jn->toJSON();';
            echo PHP_EOL;            
        }

        /** <c>howToBuildAMetadataDocument</c> montre la manière de construire un <c>MetadataDocument</c>
         * 
         *  <hr>
         *  <strong>Exemple de code : </strong>
         *  <pre>
         *  $bn =  new BiomedicalNode();           // ... puis configurer $bn  comme décrit dans "howToBuildABiomedicalNode"
         *  $cn =  new CitationNode();             // ... puis configurer $cn  comme décrit dans "howToBuildACitationNode"
         *  $dtn = new DerivedTextNode();          // ... puis configurer $dtn comme décrit dans "howToBuildADerivedTextNode"
         *  $gn =  new GeospatialNode();           // ... puis configurer $gn  comme décrit dans "howToBuildAGeospatialNode"
         *  $jn =  new JournalNode();              // ... puis configurer $jn  comme décrit dans "howToBuildAJournalNode"
         *  $sn =  new SemanticsNode();            // ... puis configurer $sn  comme décrit dans "howToBuildASemanticsNode"
         *  $ssn = new SocialScienceNode();        // ... puis configurer $ssn comme décrit dans "howToBuildASocialScienceNode"
         *  
         *  $md = new MetadataDocument();
         *  $md.AddCitationNode($cn);
         *  $md.AddGeospatialNode($gn);
         *  $md.AddSocialScienceNode($ssn);
         *  $md.AddBiomedicalNode($bn);
         *  $md.AddJournalNode($jn);
         *  $md.AddDerivedTextNode($dtn);
         *  $md.AddSemanticsNode($sn);
         *  
         *  echo $md.ToJSON();
         *  </pre>
         */ 
        public static function howToBuildAMetadataDocument()
        {
         echo PHP_EOL.'$bn =  new BiomedicalNode();           // ... puis configurer $bn  comme décrit dans "howToBuildABiomedicalNode"';
         echo PHP_EOL.'$cn =  new CitationNode();             // ... puis configurer $cn  comme décrit dans "howToBuildACitationNode"';
         echo PHP_EOL.'$dtn = new DerivedTextNode();          // ... puis configurer $dtn comme décrit dans "howToBuildADerivedTextNode"';
         echo PHP_EOL.'$gn =  new GeospatialNode();           // ... puis configurer $gn  comme décrit dans "howToBuildAGeospatialNode"';
         echo PHP_EOL.'$jn =  new JournalNode();              // ... puis configurer $jn  comme décrit dans "howToBuildAJournalNode"';
         echo PHP_EOL.'$sn =  new SemanticsNode();            // ... puis configurer $sn  comme décrit dans "howToBuildASemanticsNode"';
         echo PHP_EOL.'$ssn = new SocialScienceNode();        // ... puis configurer $ssn comme décrit dans "howToBuildASocialScienceNode"';
         echo PHP_EOL;
         echo PHP_EOL.'$md = new MetadataDocument();';
         echo PHP_EOL.'$md.AddCitationNode($cn);';
         echo PHP_EOL.'$md.AddGeospatialNode($gn);';
         echo PHP_EOL.'$md.AddSocialScienceNode($ssn);';
         echo PHP_EOL.'$md.AddBiomedicalNode($bn);';
         echo PHP_EOL.'$md.AddJournalNode($jn);';
         echo PHP_EOL.'$md.AddDerivedTextNode($dtn);';
         echo PHP_EOL.'$md.AddSemanticsNode($sn);';
         echo PHP_EOL;
         echo PHP_EOL.'echo $md.ToJSON();';
         echo PHP_EOL;
        }        

        /** <c>howToBuildAMinimalCitationNode</c> montre la manière de construire un <c>CitationNode</c> minimal
         * 
         *  <hr>
         *  <strong>Exemple de code : </strong>
         *  <pre>
         *  // je crée les auteurs du dataset
         *  $auteur1 = array();
         *  $auteur1["authorAffiliation"]           = new SimplePrimitiveNode("authorAffiliation", "authorAffiliation1");
         *  $auteur1["authorIdentifier"]            = new SimplePrimitiveNode("authorIdentifier", "authorIdentifier1");
         *  $auteur1["authorIdentifierScheme"]      = new SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID");
         *  $auteur1["authorName"]                  = new SimplePrimitiveNode("authorName", "authorName1");
         * 
         *  $auteur2 = array();
         *  $auteur2["authorAffiliation"]           = new SimplePrimitiveNode("authorAffiliation", "authorAffiliation2");
         *  $auteur2["authorIdentifier"]            = new SimplePrimitiveNode("authorIdentifier", "authorIdentifier2");
         *  $auteur2["authorIdentifierScheme"]      = new SimpleControlledVocabularyNode("authorIdentifierScheme", "idHAL");
         *  $auteur2["authorName"]                  = new SimplePrimitiveNode("authorName", "authorName2");
         * 
         *  // je crée les contacts du dataset
         *  $contact1 = array();
         *  $contact1["datasetContactAffiliation"]  = new SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation1");
         *  $contact1["datasetContactEmail"]        = new SimplePrimitiveNode("datasetContactEmail", "contact1@mail.com");
         *  $contact1["datasetContactName"]         = new SimplePrimitiveNode("datasetContactName", "contactName1");
         * 
         *  $contact2 = array();
         *  $contact2["datasetContactAffiliation"]  = new SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation2");
         *  $contact2["datasetContactEmail"]        = new SimplePrimitiveNode("datasetContactEmail", "contact2@mail.com");
         *  $contact2["datasetContactName"]         = new SimplePrimitiveNode("datasetContactName", "contactName2");
         * 
         *  // je crée les descriptions du dataset
         *  $description1 = array();
         *  $description1["dsDescriptionDate"]      = new SimplePrimitiveNode("dsDescriptionDate", "2000-01-01");
         *  $description1["dsDescriptionValue"]     = new SimplePrimitiveNode("dsDescriptionValue", "descriptionText1");
         * 
         *  $description2 = array();
         *  $description2["dsDescriptionDate"]      = new SimplePrimitiveNode("dsDescriptionDate", "2000-01-01");
         *  $description2["dsDescriptionValue"]     = new SimplePrimitiveNode("dsDescriptionValue", "descriptionText2");
         * 
         *  // je crée un node citation
         *  $cn = new CitationNode();
         *  $cn->addField(new SimplePrimitiveNode("title", "title"));
         *  $cn->addField(new MultipleCompoundNode("datasetContact", array($contact1, $contact2) ));
         *  $cn->addField(new MultipleCompoundNode("author", array($auteur1, $auteur2) ));
         *  $cn->addField(new MultipleCompoundNode("dsDescription", array($description1, $description2) ));
         *  $cn->addField(new MultipleControlledVocabularyNode("subject",array("Animal Breeding and Animal Products", "Animal Health and Pathology") ));
         *  $cn->addField(new MultipleControlledVocabularyNode("kindOfData",array("Audiovisual", "Collection") ));
         *      
         *  echo $cn.toJSON();
         *  </pre>
         */ 
        public static function howToBuildAMinimalCitationNode()
        {
            echo PHP_EOL.'// je crée les auteurs du dataset';
            echo PHP_EOL.'$auteur1 = array();';
            echo PHP_EOL.'$auteur1["authorAffiliation"]           = new SimplePrimitiveNode("authorAffiliation", "authorAffiliation1");';
            echo PHP_EOL.'$auteur1["authorIdentifier"]            = new SimplePrimitiveNode("authorIdentifier", "authorIdentifier1");';
            echo PHP_EOL.'$auteur1["authorIdentifierScheme"]      = new SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID");';
            echo PHP_EOL.'$auteur1["authorName"]                  = new SimplePrimitiveNode("authorName", "authorName1");';
            echo PHP_EOL;
            echo PHP_EOL.'$auteur2 = array();';
            echo PHP_EOL.'$auteur2["authorAffiliation"]           = new SimplePrimitiveNode("authorAffiliation", "authorAffiliation2");';
            echo PHP_EOL.'$auteur2["authorIdentifier"]            = new SimplePrimitiveNode("authorIdentifier", "authorIdentifier2");';
            echo PHP_EOL.'$auteur2["authorIdentifierScheme"]      = new SimpleControlledVocabularyNode("authorIdentifierScheme", "idHAL");';
            echo PHP_EOL.'$auteur2["authorName"]                  = new SimplePrimitiveNode("authorName", "authorName2");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée les contacts du dataset';
            echo PHP_EOL.'$contact1 = array();';
            echo PHP_EOL.'$contact1["datasetContactAffiliation"]  = new SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation1");';
            echo PHP_EOL.'$contact1["datasetContactEmail"]        = new SimplePrimitiveNode("datasetContactEmail", "contact1@mail.com");';
            echo PHP_EOL.'$contact1["datasetContactName"]         = new SimplePrimitiveNode("datasetContactName", "contactName1");';
            echo PHP_EOL;
            echo PHP_EOL.'$contact2 = array();';
            echo PHP_EOL.'$contact2["datasetContactAffiliation"]  = new SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation2");';
            echo PHP_EOL.'$contact2["datasetContactEmail"]        = new SimplePrimitiveNode("datasetContactEmail", "contact2@mail.com");';
            echo PHP_EOL.'$contact2["datasetContactName"]         = new SimplePrimitiveNode("datasetContactName", "contactName2");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée les descriptions du dataset';
            echo PHP_EOL.'$description1 = array();';
            echo PHP_EOL.'$description1["dsDescriptionDate"]      = new SimplePrimitiveNode("dsDescriptionDate", "2000-01-01");';
            echo PHP_EOL.'$description1["dsDescriptionValue"]     = new SimplePrimitiveNode("dsDescriptionValue", "descriptionText1");';
            echo PHP_EOL;
            echo PHP_EOL.'$description2 = array();';
            echo PHP_EOL.'$description2["dsDescriptionDate"]      = new SimplePrimitiveNode("dsDescriptionDate", "2000-01-01");';
            echo PHP_EOL.'$description2["dsDescriptionValue"]     = new SimplePrimitiveNode("dsDescriptionValue", "descriptionText2");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée un node citation';
            echo PHP_EOL.'$cn = new CitationNode();';
            echo PHP_EOL.'$cn->addField(new SimplePrimitiveNode("title", "title"));';
            echo PHP_EOL.'$cn->addField(new MultipleCompoundNode("datasetContact", array($contact1, $contact2) ));';
            echo PHP_EOL.'$cn->addField(new MultipleCompoundNode("author", array($auteur1, $auteur2) ));';
            echo PHP_EOL.'$cn->addField(new MultipleCompoundNode("dsDescription", array($description1, $description2) ));';
            echo PHP_EOL.'$cn->addField(new MultipleControlledVocabularyNode("subject",array("Animal Breeding and Animal Products", "Animal Health and Pathology") ));';
            echo PHP_EOL.'$cn->addField(new MultipleControlledVocabularyNode("kindOfData",array("Audiovisual", "Collection") ));';
            echo PHP_EOL;
            echo PHP_EOL.'echo $cn.toJSON();';
            echo PHP_EOL;
        }

        /** <c>howToBuildAMultipleCompoundNode</c> montre la manière de construire un <c>MultipleCompoundNode</c>
         * 
         *  <hr>
         *  <strong>Exemple de code : </strong>
         *  <pre>
         *  $author1 = array(
         *                      "authorAffiliation"         => new SimplePrimitiveNode("authorAffiliation", "authorAffiliation1"),
         *                      "authorIdentifier"          => new SimplePrimitiveNode("authorIdentifier", "authorIdentifier1"),
         *                      "authorIdentifierScheme"    => new SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID"),
         *                      "authorName"                => new SimplePrimitiveNode("authorName", "authorName1"),
         *                  );
         *  
         *  $author2 = array(
         *                      "authorAffiliation"         => new SimplePrimitiveNode("authorAffiliation", "authorAffiliation2"),
         *                      "authorIdentifier"          => new SimplePrimitiveNode("authorIdentifier", "authorIdentifier2"),
         *                      "authorIdentifierScheme"    => new SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID"),
         *                      "authorName"                => new SimplePrimitiveNode("authorName", "authorName2"),
         *                  );
         *  
         *  $author3 = array(
         *                      "authorAffiliation"         => new SimplePrimitiveNode("authorAffiliation", "authorAffiliation3"),
         *                      "authorIdentifier"          => new SimplePrimitiveNode("authorIdentifier", "authorIdentifier3"),
         *                      "authorIdentifierScheme"    => new SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID"),
         *                      "authorName"                => new SimplePrimitiveNode("authorName", "authorName3"),
         *                  );
         *  
         *  $mcn = new MultipleCompoundNode("author", array($author1, $author2, $author));
         *  echo $mcn->toJSON();
         *  </pre>
         */ 
        public static function howToBuildAMultipleCompoundNode()
        {
            echo PHP_EOL.'$author1 = array(';
            echo PHP_EOL.'                    "authorAffiliation"         => new SimplePrimitiveNode("authorAffiliation", "authorAffiliation1"),';
            echo PHP_EOL.'                    "authorIdentifier"          => new SimplePrimitiveNode("authorIdentifier", "authorIdentifier1"),';
            echo PHP_EOL.'                    "authorIdentifierScheme"    => new SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID"),';
            echo PHP_EOL.'                    "authorName"                => new SimplePrimitiveNode("authorName", "authorName1"),';
            echo PHP_EOL.'                );';
            echo PHP_EOL;
            echo PHP_EOL.'$author2 = array(';
            echo PHP_EOL.'                    "authorAffiliation"         => new SimplePrimitiveNode("authorAffiliation", "authorAffiliation2"),';
            echo PHP_EOL.'                    "authorIdentifier"          => new SimplePrimitiveNode("authorIdentifier", "authorIdentifier2"),';
            echo PHP_EOL.'                    "authorIdentifierScheme"    => new SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID"),';
            echo PHP_EOL.'                    "authorName"                => new SimplePrimitiveNode("authorName", "authorName2"),';
            echo PHP_EOL.'                );';
            echo PHP_EOL;
            echo PHP_EOL.'$author3 = array(';
            echo PHP_EOL.'                    "authorAffiliation"         => new SimplePrimitiveNode("authorAffiliation", "authorAffiliation3"),';
            echo PHP_EOL.'                    "authorIdentifier"          => new SimplePrimitiveNode("authorIdentifier", "authorIdentifier3"),';
            echo PHP_EOL.'                    "authorIdentifierScheme"    => new SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID"),';
            echo PHP_EOL.'                    "authorName"                => new SimplePrimitiveNode("authorName", "authorName3"),';
            echo PHP_EOL.'                );';
            echo PHP_EOL;
            echo PHP_EOL.'$mcn = new MultipleCompoundNode("author", array($author1, $author2, $author3));';
            echo PHP_EOL.'echo $mcn->toJSON();';
            echo PHP_EOL;
        }

        /** <c>howToBuildAMultipleControlledVocabularyNode</c> montre la manière de construire un <c>MultipleControlledVocabularyNode</c>
         * 
         *  <hr>
         *  <strong>Exemple de code : </strong>
         *  <pre>
         *  $languages = array(
         *                        "French",
         *                        "English",
         *                        "Occitan"
         *                    );
         *
         *  $mcvn = new MultipleControlledVocabularyNode("language", $languages);
         *  echo $mcvn->toJSON(true);         *
         *  </pre>
         */ 
        public static function howToBuildAMultipleControlledVocabularyNode()
        {
            echo PHP_EOL.'$languages = array(';
            echo PHP_EOL.'                      "French",';
            echo PHP_EOL.'                      "English",';
            echo PHP_EOL.'                      "Occitan"';
            echo PHP_EOL.'                  );';
            echo PHP_EOL;
            echo PHP_EOL.'$mcvn = new MultipleControlledVocabularyNode("language", $languages);';
            echo PHP_EOL.'echo $mcvn->toJSON(true);';
            echo PHP_EOL;
        }

        /** <c>howToBuildAMultiplePrimitiveNode</c> montre la manière de construire un <c>MultiplePrimitiveNode</c>
         * 
         *  <hr>
         *  <strong>Exemple de code : </strong>
         *  <pre>
         *  $dataSources = array(
         *                          "dataSource1", 
         *                          "dataSource2", 
         *                          "dataSource3"
         *                      );
         
         *  $mpn = new MultiplePrimitiveNode("dataSources", $dataSources);
         *  echo $mpn->toJSON(true);
         *  </pre>
         */ 
        public static function howToBuildAMultiplePrimitiveNode()
        {
            echo PHP_EOL.'$dataSources = array(';
            echo PHP_EOL.'                        "dataSource1",';
            echo PHP_EOL.'                        "dataSource2",';
            echo PHP_EOL.'                        "dataSource3"';
            echo PHP_EOL.'                    );';
            echo PHP_EOL;
            echo PHP_EOL.'$mpn = new MultiplePrimitiveNode("dataSources", $dataSources);';
            echo PHP_EOL.'echo $mpn->toJSON(true);';
            echo PHP_EOL;
        }

        /** <c>howToBuildASemanticsNode</c> montre la manière de construire un <c>SemanticsNode</c>
         * 
         *  <hr>
         *  <strong>Exemple de code : </strong>
         *  <pre>
         *  // je crée la version de ressource du dataset
         *  $resourceVersion = array();
         *  $resourceVersion["versionStatus"]       = new MultipleControlledVocabularyNode("versionStatus", array("alpha", "beta"));
         *  $resourceVersion["versionInfo"]         = new SimplePrimitiveNode("versionInfo", "version");
         *  $resourceVersion["priorVersion"]        = new MultiplePrimitiveNode("priorVersion", array("http://prior.version"));
         *  $resourceVersion["modificationDate"]    = new SimplePrimitiveNode("modificationDate", "2000-01-01");
         *  $resourceVersion["changes"]             = new SimplePrimitiveNode("changes", "http://semantic.version.changes");
         *  
         *  // je crée un node semantics
         *  $sn = new SemanticsNode();
         *  $sn->addField(new SimpleControlledVocabularyNode("hasFormalityLevel", "Classification scheme"));
         *  $sn->addField(new SimpleControlledVocabularyNode("hasOntologyLanguage", "JSON-LD"));
         *  $sn->addField(new SimpleControlledVocabularyNode("typeOfSR", "Application Ontology"));
         *  $sn->addField(new MultipleControlledVocabularyNode("designedForOntologyTask", array("Annotation Task", "Configuration Task")));
         *  $sn->addField(new SimplePrimitiveNode("knownUsage", "knownUsage"));
         *  $sn->addField(new SimpleCompoundNode("resourceVersion", $resourceVersion));
         *  $sn->addField(new MultiplePrimitiveNode("imports", array("http://semantic.imports1", "http://semantic.imports2")));
         *  $sn->addField(new SimplePrimitiveNode("bugDatabase", "http://semantic.bug.tracker"));
         *  $sn->addField(new SimplePrimitiveNode("endpoint", "http://semantic.sparql.endpoint"));
         *  $sn->addField(new MultiplePrimitiveNode("imports", array("http://semantic.uri1", "http://semantic.uri2")));
         *  
         *  echo $sn->toJSON();
         *  </pre>
         */ 
        public static function howToBuildASemanticsNode()
        {
            echo PHP_EOL.'// je crée la version de ressource du dataset';
            echo PHP_EOL.'$resourceVersion = array();';
            echo PHP_EOL.'$resourceVersion["versionStatus"]       = new MultipleControlledVocabularyNode("versionStatus", array("alpha", "beta"));';
            echo PHP_EOL.'$resourceVersion["versionInfo"]         = new SimplePrimitiveNode("versionInfo", "version");';
            echo PHP_EOL.'$resourceVersion["priorVersion"]        = new MultiplePrimitiveNode("priorVersion", array("http://prior.version"));';
            echo PHP_EOL.'$resourceVersion["modificationDate"]    = new SimplePrimitiveNode("modificationDate", "2000-01-01");';
            echo PHP_EOL.'$resourceVersion["changes"]             = new SimplePrimitiveNode("changes", "http://semantic.version.changes");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée un node semantics';
            echo PHP_EOL.'$sn = new SemanticsNode();';
            echo PHP_EOL.'$sn->addField(new SimpleControlledVocabularyNode("hasFormalityLevel", "Classification scheme"));';
            echo PHP_EOL.'$sn->addField(new SimpleControlledVocabularyNode("hasOntologyLanguage", "JSON-LD"));';
            echo PHP_EOL.'$sn->addField(new SimpleControlledVocabularyNode("typeOfSR", "Application Ontology"));';
            echo PHP_EOL.'$sn->addField(new MultipleControlledVocabularyNode("designedForOntologyTask", array("Annotation Task", "Configuration Task")));';
            echo PHP_EOL.'$sn->addField(new SimplePrimitiveNode("knownUsage", "knownUsage"));';
            echo PHP_EOL.'$sn->addField(new SimpleCompoundNode("resourceVersion", $resourceVersion));';
            echo PHP_EOL.'$sn->addField(new MultiplePrimitiveNode("imports", array("http://semantic.imports1", "http://semantic.imports2")));';
            echo PHP_EOL.'$sn->addField(new SimplePrimitiveNode("bugDatabase", "http://semantic.bug.tracker"));';
            echo PHP_EOL.'$sn->addField(new SimplePrimitiveNode("endpoint", "http://semantic.sparql.endpoint"));';
            echo PHP_EOL.'$sn->addField(new MultiplePrimitiveNode("imports", array("http://semantic.uri1", "http://semantic.uri2")));';
            echo PHP_EOL;
            echo PHP_EOL.'echo $sn->toJSON();';
            echo PHP_EOL;
        }

        /** <c>howToBuildASimpleCompoundNode</c> montre la manière de construire un <c>SimpleCompoundNode</c>
         * 
         *  <hr>
         *  <strong>Exemple de code : </strong>
         *  <pre>
         *  $project = array(
         *                      "projectAcronym"     => new SimplePrimitiveNode("projectAcronym",     "acronym"),
         *                      "projectId"          => new SimplePrimitiveNode("projectId",          "id"),
         *                      "projectTitle"       => new SimplePrimitiveNode("projectTitle",       "title"),
         *                      "projectTask"        => new SimplePrimitiveNode("projectTask",        "task"),
         *                      "projectURL"         => new SimplePrimitiveNode("projectURL",         "url"),
         *                      "projectWorkPackage" => new SimplePrimitiveNode("projectWorkPackage", "workPackage"),
         *                  );
         *  
         *  $scn = new SimpleCompoundNode();
         *  $scn->setTypeName("project");
         *  $scn->setValue($arr);
         *  </pre>
         */ 
        public static function howToBuildASimpleCompoundNode()
        {
            echo PHP_EOL.'$project = array(';
            echo PHP_EOL.'                    "projectAcronym"     => new SimplePrimitiveNode("projectAcronym",     "acronym"),';
            echo PHP_EOL.'                    "projectId"          => new SimplePrimitiveNode("projectId",          "id"),';
            echo PHP_EOL.'                    "projectTitle"       => new SimplePrimitiveNode("projectTitle",       "title"),';
            echo PHP_EOL.'                    "projectTask"        => new SimplePrimitiveNode("projectTask",        "task"),';
            echo PHP_EOL.'                    "projectURL"         => new SimplePrimitiveNode("projectURL",         "url"),';
            echo PHP_EOL.'                    "projectWorkPackage" => new SimplePrimitiveNode("projectWorkPackage", "workPackage"),';
            echo PHP_EOL.'                );';
            echo PHP_EOL;
            echo PHP_EOL.'$scn = new SimpleCompoundNode();';
            echo PHP_EOL.'$scn->setTypeName("project");';
            echo PHP_EOL.'$scn->setValue($arr);';
            echo PHP_EOL;
        }

        /** <c>howToBuildASimpleControlledVocabularyNode</c> montre la manière de construire un <c>SimpleControlledVocabularyNode</c>
         * 
         *  <hr>
         *  <strong>Exemple de code : </strong>
         *  <pre>
         *  $scvn = new SimpleControlledVocabularyNode("journalArticleType", "news");
         *  echo $scvn->toJSON();
         *  </pre>
         */ 
        public static function howToBuildASimpleControlledVocabularyNode()
        {
            echo PHP_EOL.'$scvn = new SimpleControlledVocabularyNode("journalArticleType", "news");';
            echo PHP_EOL.'echo $scvn->toJSON();';
            echo PHP_EOL;
        }

        /** <c>howToBuildASimplePrimitiveNode</c> montre la manière de construire un <c>SimplePrimitiveNode</c>
         * 
         *  <hr>
         *  <strong>Exemple de code : </strong>
         *  <pre>
         *  $spn = new SimplePrimitiveNode("softwareName", "softwareVersion");
         *  echo $spn->toJSON();
         *  </pre>
         */ 
        public static function howToBuildASimplePrimitiveNode()
        {
            echo PHP_EOL.'$spn = new SimplePrimitiveNode("softwareName", "softwareName");';
            echo PHP_EOL.'echo $spn->toJSON();';
            echo PHP_EOL;
        }

        /** <c>howToBuildASocialScienceNode</c> montre la manière de construire un <c>SocialScienceNode</c>
         * 
         *  <hr>
         *  <strong>Exemple de code : </strong>
         *  <pre>
         *  // je crée la taille de l'échantillon 
         *  $targetSampleSize = array();
         *  $targetSampleSize["targetSampleActualSize"]         = new SimplePrimitiveNode("targetSampleActualSize", "42");
         *  $targetSampleSize["targetSampleSizeFormula"]        = new SimplePrimitiveNode("targetSampleSizeFormula", "targetSampleSizeFormula");
         *  
         *  // je crée notes du node
         *  $socialScienceNotes = array();
         *  $socialScienceNotes["socialScienceNotesSubject"]    = new SimplePrimitiveNode("socialScienceNotesSubject", "socialScienceNotesSubject");
         *  $socialScienceNotes["socialScienceNotesText"]       = new SimplePrimitiveNode("socialScienceNotesText", "socialScienceNotesText");
         *  $socialScienceNotes["socialScienceNotesType"]       = new SimplePrimitiveNode("socialScienceNotesType", "socialScienceNotesType");
         *  
         *  // je crée notes du node
         *  $geographicalReferential1 = array();
         *  $geographicalReferential1["level"]                  = new SimplePrimitiveNode("level", "geographicalReferentialLevel1");
         *  $geographicalReferential1["version"]                = new SimplePrimitiveNode("version", "geographicalReferentialVersion1");
         *  
         *  $geographicalReferential2 = array();
         *  $geographicalReferential2["level"]                  = new SimplePrimitiveNode("level", "geographicalReferentialLevel2");
         *  $geographicalReferential2["version"]                = new SimplePrimitiveNode("version", "geographicalReferentialVersion2");
         *  
         *  // je crée un node socialscience
         *  $ssn = new SocialScienceNode();
         *  $ssn->addField(new MultipleControlledVocabularyNode("unitOfAnalysis", array("Individual", "Organization")));
         *  $ssn->addField(new MultiplePrimitiveNode("universe", array("universe1", "universe2")));
         *  $ssn->addField(new SimpleControlledVocabularyNode("timeMethod", "Longitudinal"));
         *  $ssn->addField(new SimplePrimitiveNode("timeMethodOther", "timeMethodOther"));
         *  $ssn->addField(new SimplePrimitiveNode("dataCollector", "dataCollector"));
         *  $ssn->addField(new SimplePrimitiveNode("collectorTraining", "collectorTraining"));
         *  $ssn->addField(new SimplePrimitiveNode("frequencyOfDataCollection", "frequency"));
         *  $ssn->addField(new SimpleControlledVocabularyNode("samplingProcedure", "Total universe/Complete enumeration"));
         *  $ssn->addField(new SimpleControlledVocabularyNode("samplingProcedureOther", "samplingProcedureOther"));
         *  $ssn->addField(new SimpleCompoundNode("targetSampleSize", $targetSampleSize));
         *  $ssn->addField(new SimplePrimitiveNode("deviationsFromSampleDesign", "deviationsFromSampleDesign"));
         *  $ssn->addField(new SimpleControlledVocabularyNode("collectionMode", "Interview"));
         *  $ssn->addField(new SimplePrimitiveNode("collectionModeOther", "collectionModeOther"));
         *  $ssn->addField(new SimplePrimitiveNode("researchInstrument", "researchInstrument"));
         *  $ssn->addField(new SimplePrimitiveNode("dataCollectionSituation", "dataCollectionSituation"));
         *  $ssn->addField(new SimplePrimitiveNode("actionsToMinimizeLoss", "actionsToMinimizeLoss"));
         *  $ssn->addField(new SimplePrimitiveNode("controlOperations", "controlOperations"));
         *  $ssn->addField(new SimplePrimitiveNode("weighting", "weighting"));
         *  $ssn->addField(new SimplePrimitiveNode("cleaningOperations", "cleaningOperations"));
         *  $ssn->addField(new SimplePrimitiveNode("datasetLevelErrorNotes", "datasetLevelErrorNotes"));
         *  $ssn->addField(new SimplePrimitiveNode("responseRate", "responseRate"));
         *  $ssn->addField(new SimplePrimitiveNode("samplingErrorEstimates", "samplingErrorEstimates"));
         *  $ssn->addField(new SimplePrimitiveNode("otherDataAppraisal", "otherDataAppraisal"));
         *  $ssn->addField(new SimpleCompoundNode("socialScienceNotes", $socialScienceNotes));
         *  $ssn->addField(new MultipleCompoundNode("geographicalReferential", array($geographicalReferential1, $geographicalReferential2)));
         *  
         *  echo $ssn->toJSON()
         *  </pre>
         */ 
        public static function howToBuildASocialScienceNode()
        {
            echo PHP_EOL.'// je crée la taille de l\'échantillon';
            echo PHP_EOL.'$targetSampleSize = array();';
            echo PHP_EOL.'$targetSampleSize["targetSampleActualSize"]         = new SimplePrimitiveNode("targetSampleActualSize", "42");';
            echo PHP_EOL.'$targetSampleSize["targetSampleSizeFormula"]        = new SimplePrimitiveNode("targetSampleSizeFormula", "targetSampleSizeFormula");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée notes du node';
            echo PHP_EOL.'$socialScienceNotes = array();';
            echo PHP_EOL.'$socialScienceNotes["socialScienceNotesSubject"]    = new SimplePrimitiveNode("socialScienceNotesSubject", "socialScienceNotesSubject");';
            echo PHP_EOL.'$socialScienceNotes["socialScienceNotesText"]       = new SimplePrimitiveNode("socialScienceNotesText", "socialScienceNotesText");';
            echo PHP_EOL.'$socialScienceNotes["socialScienceNotesType"]       = new SimplePrimitiveNode("socialScienceNotesType", "socialScienceNotesType");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée notes du node';
            echo PHP_EOL.'$geographicalReferential1 = array();';
            echo PHP_EOL.'$geographicalReferential1["level"]                  = new SimplePrimitiveNode("level", "geographicalReferentialLevel1");';
            echo PHP_EOL.'$geographicalReferential1["version"]                = new SimplePrimitiveNode("version", "geographicalReferentialVersion1");';
            echo PHP_EOL;
            echo PHP_EOL.'$geographicalReferential2 = array();';
            echo PHP_EOL.'$geographicalReferential2["level"]                  = new SimplePrimitiveNode("level", "geographicalReferentialLevel2");';
            echo PHP_EOL.'$geographicalReferential2["version"]                = new SimplePrimitiveNode("version", "geographicalReferentialVersion2");';
            echo PHP_EOL;
            echo PHP_EOL.'// je crée un node socialscience';
            echo PHP_EOL.'$ssn = new SocialScienceNode();';
            echo PHP_EOL.'$ssn->addField(new MultipleControlledVocabularyNode("unitOfAnalysis", array("Individual", "Organization")));';
            echo PHP_EOL.'$ssn->addField(new MultiplePrimitiveNode("universe", array("universe1", "universe2")));';
            echo PHP_EOL.'$ssn->addField(new SimpleControlledVocabularyNode("timeMethod", "Longitudinal"));';
            echo PHP_EOL.'$ssn->addField(new SimplePrimitiveNode("timeMethodOther", "timeMethodOther"));';
            echo PHP_EOL.'$ssn->addField(new SimplePrimitiveNode("dataCollector", "dataCollector"));';
            echo PHP_EOL.'$ssn->addField(new SimplePrimitiveNode("collectorTraining", "collectorTraining"));';
            echo PHP_EOL.'$ssn->addField(new SimplePrimitiveNode("frequencyOfDataCollection", "frequency"));';
            echo PHP_EOL.'$ssn->addField(new SimpleControlledVocabularyNode("samplingProcedure", "Total universe/Complete enumeration"));';
            echo PHP_EOL.'$ssn->addField(new SimpleControlledVocabularyNode("samplingProcedureOther", "samplingProcedureOther"));';
            echo PHP_EOL.'$ssn->addField(new SimpleCompoundNode("targetSampleSize", $targetSampleSize));';
            echo PHP_EOL.'$ssn->addField(new SimplePrimitiveNode("deviationsFromSampleDesign", "deviationsFromSampleDesign"));';
            echo PHP_EOL.'$ssn->addField(new SimpleControlledVocabularyNode("collectionMode", "Interview"));';
            echo PHP_EOL.'$ssn->addField(new SimplePrimitiveNode("collectionModeOther", "collectionModeOther"));';
            echo PHP_EOL.'$ssn->addField(new SimplePrimitiveNode("researchInstrument", "researchInstrument"));';
            echo PHP_EOL.'$ssn->addField(new SimplePrimitiveNode("dataCollectionSituation", "dataCollectionSituation"));';
            echo PHP_EOL.'$ssn->addField(new SimplePrimitiveNode("actionsToMinimizeLoss", "actionsToMinimizeLoss"));';
            echo PHP_EOL.'$ssn->addField(new SimplePrimitiveNode("controlOperations", "controlOperations"));';
            echo PHP_EOL.'$ssn->addField(new SimplePrimitiveNode("weighting", "weighting"));';
            echo PHP_EOL.'$ssn->addField(new SimplePrimitiveNode("cleaningOperations", "cleaningOperations"));';
            echo PHP_EOL.'$ssn->addField(new SimplePrimitiveNode("datasetLevelErrorNotes", "datasetLevelErrorNotes"));';
            echo PHP_EOL.'$ssn->addField(new SimplePrimitiveNode("responseRate", "responseRate"));';
            echo PHP_EOL.'$ssn->addField(new SimplePrimitiveNode("samplingErrorEstimates", "samplingErrorEstimates"));';
            echo PHP_EOL.'$ssn->addField(new SimplePrimitiveNode("otherDataAppraisal", "otherDataAppraisal"));';
            echo PHP_EOL.'$ssn->addField(new SimpleCompoundNode("socialScienceNotes", $socialScienceNotes));';
            echo PHP_EOL.'$ssn->addField(new MultipleCompoundNode("geographicalReferential", array($geographicalReferential1, $geographicalReferential2)));';
            echo PHP_EOL;
            echo PHP_EOL.'echo $ssn->toJSON()';
            echo PHP_EOL;
        }

        /** <c>showMetedataMap</c> détaille la carte des nodes du document de métadonnées
         * 
         *  <hr>
         *  <strong>Exemple de code : </strong>
         *  <pre>
         *  metadata
         *  |  citation
         *  |  |  accessToSources                       SimplePrimitiveNode
         *  |  |  alternativeTitle                      SimplePrimitiveNode
         *  |  |  alternativeURL                        SimplePrimitiveNode
         *  |  |  author                                MultipleCompoundNode
         *  |  |  |  authorAffiliation                  SimplePrimitiveNode
         *  |  |  |  authorIdentifier                   SimplePrimitiveNode
         *  |  |  |  authorIdentifierScheme             SimpleControlledVocabularyNode
         *  |  |  |  authorName                         SimplePrimitiveNode
         *  |  |  characteristicOfSources               SimplePrimitiveNode
         *  |  |  contributor                           MultipleCompoundNode
         *  |  |  |  contributorAffiliation             SimplePrimitiveNode
         *  |  |  |  contributorIdentifier              SimplePrimitiveNode
         *  |  |  |  contributorIdentifierScheme        SimpleControlledVocabularyNode
         *  |  |  |  contributorName                    SimplePrimitiveNode
         *  |  |  |  contributorType                    SimpleControlledVocabularyNode
         *  |  |  dataOrigin                            MultipleControlledVocabularyNode
         *  |  |  datasetContact                        MultipleCompoundNode
         *  |  |  |  datasetContactAffiliation          SimplePrimitiveNode
         *  |  |  |  datasetContactEmail                SimplePrimitiveNode
         *  |  |  |  datasetContactName                 SimplePrimitiveNode
         *  |  |  dataSources                           MultiplePrimitiveNode
         *  |  |  dateOfCollection                      MultipleCompoundNode
         *  |  |  |  dateOfCollectionStart              SimplePrimitiveNode
         *  |  |  |  dateOfCollectionEnd                SimplePrimitiveNode
         *  |  |  dateOfDeposit                         SimplePrimitiveNode
         *  |  |  depositor                             SimplePrimitiveNode
         *  |  |  distributionDate                      SimplePrimitiveNode
         *  |  |  distributor                           MultipleCompoundNode
         *  |  |  |  distributorAbbreviation            SimplePrimitiveNode
         *  |  |  |  distributorAffiliation             SimplePrimitiveNode
         *  |  |  |  distributorLogoURL                 SimplePrimitiveNode
         *  |  |  |  distributorName                    SimplePrimitiveNode
         *  |  |  |  distributorURL                     SimplePrimitiveNode
         *  |  |  dsDescription                         MultipleCompoundNode
         *  |  |  |  dsDescriptionDate                  SimplePrimitiveNode
         *  |  |  |  dsDescriptionValue                 SimplePrimitiveNode
         *  |  |  grantNumber                           MultipleCompoundNode
         *  |  |  |  grantNumberAgency                  SimplePrimitiveNode
         *  |  |  |  grantNumberValue                   SimplePrimitiveNode
         *  |  |  keyword                               MultipleCompoundNode
         *  |  |  |  keywordTermURI                     SimplePrimitiveNode
         *  |  |  |  keywordValue                       SimplePrimitiveNode
         *  |  |  |  keywordVocabulary                  SimplePrimitiveNode
         *  |  |  |  keywordVocabularyURI               SimplePrimitiveNode
         *  |  |  kindOfData                            MultipleControlledVocabularyNode
         *  |  |  kindOfDataOther                       MultiplePrimitiveNode
         *  |  |  language                              MultipleControlledVocabularyNode
         *  |  |  lifeCycleStep                         MultipleControlledVocabularyNode
         *  |  |  notesText                             SimplePrimitiveNode
         *  |  |  originOfSources                       SimplePrimitiveNode
         *  |  |  otherId                               MultipleCompoundNode
         *  |  |  |  otherIdAgency                      SimplePrimitiveNode
         *  |  |  |  otherIdValue                       SimplePrimitiveNode
         *  |  |  otherReferences                       MultiplePrimitiveNode
         *  |  |  producer                              MultipleCompoundNode
         *  |  |  |  producerAbbreviation               SimplePrimitiveNode
         *  |  |  |  producerAffiliation                SimplePrimitiveNode
         *  |  |  |  producerLogoURL                    SimplePrimitiveNode
         *  |  |  |  producerName                       SimplePrimitiveNode
         *  |  |  |  producerURL                        SimplePrimitiveNode
         *  |  |  productionDate                        SimplePrimitiveNode
         *  |  |  productionPlace                       SimplePrimitiveNode
         *  |  |  project                               SimpleCompoundNode
         *  |  |  |  projectAcronym                     SimplePrimitiveNode
         *  |  |  |  projectId                          SimplePrimitiveNode
         *  |  |  |  projectTask                        SimplePrimitiveNode
         *  |  |  |  projectTitle                       SimplePrimitiveNode
         *  |  |  |  projectURL                         SimplePrimitiveNode
         *  |  |  |  projectWorkPackage                 SimplePrimitiveNode
         *  |  |  publication                           MultipleCompoundNode
         *  |  |  |  publicationCitation                SimplePrimitiveNode
         *  |  |  |  publicationIDNumber                SimplePrimitiveNode
         *  |  |  |  publicationIDType                  SimpleControlledVocabularyNode
         *  |  |  |  publicationURL                     SimplePrimitiveNode
         *  |  |  relatedDataset                        MultipleCompoundNode
         *  |  |  |  relatedDatasetCitation             SimplePrimitiveNode
         *  |  |  |  relatedDatasetIDNumber             SimplePrimitiveNode
         *  |  |  |  relatedDatasetIDType               SimpleControlledVocabularyNode
         *  |  |  |  relatedDatasetURL                  SimplePrimitiveNode
         *  |  |  relatedMaterial                       MultiplePrimitiveNode
         *  |  |  series                                SimpleCompoundNode
         *  |  |  |  seriesInformation                  SimplePrimitiveNode
         *  |  |  |  seriesName                         SimplePrimitiveNode
         *  |  |  software                              MultipleCompoundNode
         *  |  |  |  softwareName                       SimplePrimitiveNode
         *  |  |  |  softwareVersion                    SimplePrimitiveNode
         *  |  |  subject                               MultipleControlledVocabularyNode
         *  |  |  subtitle                              SimplePrimitiveNode
         *  |  |  timePeriodCovered                     MultipleCompoundNode
         *  |  |  |  timePeriodCoveredStart             SimplePrimitiveNode
         *  |  |  |  timePeriodCoveredEnd               SimplePrimitiveNode
         *  |  |  title                                 SimplePrimitiveNode
         *  |  |  topicClassification                   MultipleCompoundNode
         *  |  |  |  topicClassValue                    SimplePrimitiveNode
         *  |  |  |  topicClassVocab                    SimplePrimitiveNode
         *  |  |  |  topicClassVocabURI                 SimplePrimitiveNode
         *  |  geospatial
         *  |  |  conformity                            MultipleCompoundNode
         *  |  |  |  degree                             SimpleControlledVocabularyNode
         *  |  |  |  specification                      MultiplePrimitiveNode
         *  |  |  geographicBoundingBox                 MultipleCompoundNode
         *  |  |  |  eastLongitude                      SimplePrimitiveNode
         *  |  |  |  northLongitude                     SimplePrimitiveNode
         *  |  |  |  southLongitude                     SimplePrimitiveNode
         *  |  |  |  westLongitude                      SimplePrimitiveNode
         *  |  |  geographicCoverage                    MultipleCompoundNode
         *  |  |  |  city                               SimplePrimitiveNode
         *  |  |  |  country                            SimpleControlledVocabularyNode
         *  |  |  |  otherGeographicCoverage            SimplePrimitiveNode
         *  |  |  |  state                              SimplePrimitiveNode
         *  |  |  geographicUnit                        MultiplePrimitiveNode
         *  |  |  qualityValidity                       MultipleCompoundNode
         *  |  |  |  lineage                            MultiplePrimitiveNode
         *  |  |  |  spatialResolution                  MultiplePrimitiveNode
         *  |  socialscience
         *  |  |  actionsToMinimizeLoss                 SimplePrimitiveNode
         *  |  |  cleaningOperations                    SimplePrimitiveNode
         *  |  |  collectionMode                        SimpleControlledVocabularyNode
         *  |  |  collectionModeOther                   SimplePrimitiveNode
         *  |  |  collectorTraining                     SimplePrimitiveNode
         *  |  |  controlOperations                     SimplePrimitiveNode
         *  |  |  dataCollectionSituation               SimplePrimitiveNode
         *  |  |  dataCollector                         SimplePrimitiveNode
         *  |  |  datasetLevelErrorNotes                SimplePrimitiveNode
         *  |  |  deviationsFromSampleDesign            SimplePrimitiveNode
         *  |  |  frequencyOfDataCollection             SimplePrimitiveNode
         *  |  |  geographicalReferential               MultipleCompoundNode
         *  |  |  |  level                              SimplePrimitiveNode
         *  |  |  |  version                            SimplePrimitiveNode
         *  |  |  otherDataAppraisal                    SimplePrimitiveNode
         *  |  |  researchInstrument                    SimplePrimitiveNode
         *  |  |  responseRate                          SimplePrimitiveNode
         *  |  |  samplingErrorEstimates                SimplePrimitiveNode
         *  |  |  samplingProcedure                     SimpleControlledVocabularyNode
         *  |  |  samplingProcedureOther                SimplePrimitiveNode
         *  |  |  socialScienceNotes                    SimpleCompoundNode
         *  |  |  |  socialScienceNotesSubject          SimplePrimitiveNode
         *  |  |  |  socialScienceNotesText             SimplePrimitiveNode
         *  |  |  |  socialScienceNotesType             SimplePrimitiveNode
         *  |  |  targetSampleSize                      SimpleCompoundNode
         *  |  |  |  targetSampleActualSize             SimplePrimitiveNode
         *  |  |  |  targetSampleSizeFormula            SimplePrimitiveNode
         *  |  |  timeMethod                            SimpleControlledVocabularyNode
         *  |  |  timeMethodOther                       SimplePrimitiveNode
         *  |  |  unitOfAnalysis                        MultipleControlledVocabularyNode
         *  |  |  unitOfAnalysisOther                   SimplePrimitiveNode
         *  |  |  universe                              MultiplePrimitiveNode
         *  |  |  weighting                             SimplePrimitiveNode
         *  |  biomedical
         *  |  |  studyAssayCellType                    MultiplePrimitiveNode
         *  |  |  studyAssayMeasurementType             MultipleControlledVocabularyNode
         *  |  |  studyAssayOrganism                    MultipleControlledVocabularyNode
         *  |  |  studyAssayOtherOrganism               MultiplePrimitiveNode
         *  |  |  studyAssayOtherMeasurmentType         MultiplePrimitiveNode
         *  |  |  studyAssayPlatform                    MultipleControlledVocabularyNode
         *  |  |  studyAssayPlatformOther               MultiplePrimitiveNode
         *  |  |  studyAssayTechnologyType              MultipleControlledVocabularyNode
         *  |  |  studyAssayTechnologyTypeOther         MultiplePrimitiveNode
         *  |  |  studyDesignType                       MultipleControlledVocabularyNode
         *  |  |  studyDesignTypeOther                  MultiplePrimitiveNode
         *  |  |  studyFactorType                       MultipleControlledVocabularyNode
         *  |  |  studyFactorTypeOther                  MultiplePrimitiveNode
         *  |  |  studySampleType                       MultiplePrimitiveNode
         *  |  |  studyProtocolType                     MultiplePrimitiveNode
         *  |  journal
         *  |  |  journalArticleType                    SimpleControlledVocabularyNode
         *  |  |  journalVolumeIssue                    MultipleCompoundNode
         *  |  |  |  journalIssue                       SimplePrimitiveNode
         *  |  |  |  journalPubDate                     SimplePrimitiveNode
         *  |  |  |  journalVolume                      SimplePrimitiveNode
         *  |  Derived-text
         *  |  |  source                                MultipleCompoundNode
         *  |  |  |  ageOfSource                        MultiplePrimitiveNode
         *  |  |  |  citations                          MultiplePrimitiveNode
         *  |  |  |  experimentNumber                   MultiplePrimitiveNode
         *  |  |  |  typeOfSource                       MultiplePrimitiveNode
         *  |  semantics
         *  |  |  bugDatabase                           SimplePrimitiveNode
         *  |  |  designedForOntologyTask               MultipleControlledVocabularyNode
         *  |  |  endpoint                              SimplePrimitiveNode
         *  |  |  hasFormalityLevel                     SimpleControlledVocabularyNode
         *  |  |  hasOntologyLanguage                   SimpleControlledVocabularyNode
         *  |  |  knownUsage                            SimplePrimitiveNode
         *  |  |  imports                               MultiplePrimitiveNode
         *  |  |  resourceVersion                       SimpleCompoundNode
         *  |  |  |  changes                            SimplePrimitiveNode
         *  |  |  |  modificationDate                   SimplePrimitiveNode
         *  |  |  |  priorVersion                       MultiplePrimitiveNode
         *  |  |  |  versionInfo                        SimplePrimitiveNode
         *  |  |  |  versionStatus                      MultipleControlledVocabularyNode
         *  |  |  typeOfSR                              SimpleControlledVocabularyNode
         *  |  |  URI                                   MultiplePrimitiveNode
         *  </pre>
         */ 
        public static function showMetedataMap()
        {
         echo PHP_EOL.'metadata';
         echo PHP_EOL.'|  citation';
         echo PHP_EOL.'|  |  accessToSources                       SimplePrimitiveNode';
         echo PHP_EOL.'|  |  alternativeTitle                      SimplePrimitiveNode';
         echo PHP_EOL.'|  |  alternativeURL                        SimplePrimitiveNode';
         echo PHP_EOL.'|  |  author                                MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  authorAffiliation                  SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  authorIdentifier                   SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  authorIdentifierScheme             SimpleControlledVocabularyNode';
         echo PHP_EOL.'|  |  |  authorName                         SimplePrimitiveNode';
         echo PHP_EOL.'|  |  characteristicOfSources               SimplePrimitiveNode';
         echo PHP_EOL.'|  |  contributor                           MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  contributorAffiliation             SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  contributorIdentifier              SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  contributorIdentifierScheme        SimpleControlledVocabularyNode';
         echo PHP_EOL.'|  |  |  contributorName                    SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  contributorType                    SimpleControlledVocabularyNode';
         echo PHP_EOL.'|  |  dataOrigin                            MultipleControlledVocabularyNode';
         echo PHP_EOL.'|  |  datasetContact                        MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  datasetContactAffiliation          SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  datasetContactEmail                SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  datasetContactName                 SimplePrimitiveNode';
         echo PHP_EOL.'|  |  dataSources                           MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  dateOfCollection                      MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  dateOfCollectionStart              SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  dateOfCollectionEnd                SimplePrimitiveNode';
         echo PHP_EOL.'|  |  dateOfDeposit                         SimplePrimitiveNode';
         echo PHP_EOL.'|  |  depositor                             SimplePrimitiveNode';
         echo PHP_EOL.'|  |  distributionDate                      SimplePrimitiveNode';
         echo PHP_EOL.'|  |  distributor                           MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  distributorAbbreviation            SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  distributorAffiliation             SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  distributorLogoURL                 SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  distributorName                    SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  distributorURL                     SimplePrimitiveNode';
         echo PHP_EOL.'|  |  dsDescription                         MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  dsDescriptionDate                  SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  dsDescriptionValue                 SimplePrimitiveNode';
         echo PHP_EOL.'|  |  grantNumber                           MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  grantNumberAgency                  SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  grantNumberValue                   SimplePrimitiveNode';
         echo PHP_EOL.'|  |  keyword                               MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  keywordTermURI                     SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  keywordValue                       SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  keywordVocabulary                  SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  keywordVocabularyURI               SimplePrimitiveNode';
         echo PHP_EOL.'|  |  kindOfData                            MultipleControlledVocabularyNode';
         echo PHP_EOL.'|  |  kindOfDataOther                       MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  language                              MultipleControlledVocabularyNode';
         echo PHP_EOL.'|  |  lifeCycleStep                         MultipleControlledVocabularyNode';
         echo PHP_EOL.'|  |  notesText                             SimplePrimitiveNode';
         echo PHP_EOL.'|  |  originOfSources                       SimplePrimitiveNode';
         echo PHP_EOL.'|  |  otherId                               MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  otherIdAgency                      SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  otherIdValue                       SimplePrimitiveNode';
         echo PHP_EOL.'|  |  otherReferences                       MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  producer                              MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  producerAbbreviation               SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  producerAffiliation                SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  producerLogoURL                    SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  producerName                       SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  producerURL                        SimplePrimitiveNode';
         echo PHP_EOL.'|  |  productionDate                        SimplePrimitiveNode';
         echo PHP_EOL.'|  |  productionPlace                       SimplePrimitiveNode';
         echo PHP_EOL.'|  |  project                               SimpleCompoundNode';
         echo PHP_EOL.'|  |  |  projectAcronym                     SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  projectId                          SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  projectTask                        SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  projectTitle                       SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  projectURL                         SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  projectWorkPackage                 SimplePrimitiveNode';
         echo PHP_EOL.'|  |  publication                           MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  publicationCitation                SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  publicationIDNumber                SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  publicationIDType                  SimpleControlledVocabularyNode';
         echo PHP_EOL.'|  |  |  publicationURL                     SimplePrimitiveNode';
         echo PHP_EOL.'|  |  relatedDataset                        MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  relatedDatasetCitation             SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  relatedDatasetIDNumber             SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  relatedDatasetIDType               SimpleControlledVocabularyNode';
         echo PHP_EOL.'|  |  |  relatedDatasetURL                  SimplePrimitiveNode';
         echo PHP_EOL.'|  |  relatedMaterial                       MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  series                                SimpleCompoundNode';
         echo PHP_EOL.'|  |  |  seriesInformation                  SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  seriesName                         SimplePrimitiveNode';
         echo PHP_EOL.'|  |  software                              MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  softwareName                       SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  softwareVersion                    SimplePrimitiveNode';
         echo PHP_EOL.'|  |  subject                               MultipleControlledVocabularyNode';
         echo PHP_EOL.'|  |  subtitle                              SimplePrimitiveNode';
         echo PHP_EOL.'|  |  timePeriodCovered                     MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  timePeriodCoveredStart             SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  timePeriodCoveredEnd               SimplePrimitiveNode';
         echo PHP_EOL.'|  |  title                                 SimplePrimitiveNode';
         echo PHP_EOL.'|  |  topicClassification                   MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  topicClassValue                    SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  topicClassVocab                    SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  topicClassVocabURI                 SimplePrimitiveNode';
         echo PHP_EOL.'|  geospatial';
         echo PHP_EOL.'|  |  conformity                            MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  degree                             SimpleControlledVocabularyNode';
         echo PHP_EOL.'|  |  |  specification                      MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  geographicBoundingBox                 MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  eastLongitude                      SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  northLongitude                     SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  southLongitude                     SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  westLongitude                      SimplePrimitiveNode';
         echo PHP_EOL.'|  |  geographicCoverage                    MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  city                               SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  country                            SimpleControlledVocabularyNode';
         echo PHP_EOL.'|  |  |  otherGeographicCoverage            SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  state                              SimplePrimitiveNode';
         echo PHP_EOL.'|  |  geographicUnit                        MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  qualityValidity                       MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  lineage                            MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  |  spatialResolution                  MultiplePrimitiveNode';
         echo PHP_EOL.'|  socialscience';
         echo PHP_EOL.'|  |  actionsToMinimizeLoss                 SimplePrimitiveNode';
         echo PHP_EOL.'|  |  cleaningOperations                    SimplePrimitiveNode';
         echo PHP_EOL.'|  |  collectionMode                        SimpleControlledVocabularyNode';
         echo PHP_EOL.'|  |  collectionModeOther                   SimplePrimitiveNode';
         echo PHP_EOL.'|  |  collectorTraining                     SimplePrimitiveNode';
         echo PHP_EOL.'|  |  controlOperations                     SimplePrimitiveNode';
         echo PHP_EOL.'|  |  dataCollectionSituation               SimplePrimitiveNode';
         echo PHP_EOL.'|  |  dataCollector                         SimplePrimitiveNode';
         echo PHP_EOL.'|  |  datasetLevelErrorNotes                SimplePrimitiveNode';
         echo PHP_EOL.'|  |  deviationsFromSampleDesign            SimplePrimitiveNode';
         echo PHP_EOL.'|  |  frequencyOfDataCollection             SimplePrimitiveNode';
         echo PHP_EOL.'|  |  geographicalReferential               MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  level                              SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  version                            SimplePrimitiveNode';
         echo PHP_EOL.'|  |  otherDataAppraisal                    SimplePrimitiveNode';
         echo PHP_EOL.'|  |  researchInstrument                    SimplePrimitiveNode';
         echo PHP_EOL.'|  |  responseRate                          SimplePrimitiveNode';
         echo PHP_EOL.'|  |  samplingErrorEstimates                SimplePrimitiveNode';
         echo PHP_EOL.'|  |  samplingProcedure                     SimpleControlledVocabularyNode';
         echo PHP_EOL.'|  |  samplingProcedureOther                SimplePrimitiveNode';
         echo PHP_EOL.'|  |  socialScienceNotes                    SimpleCompoundNode';
         echo PHP_EOL.'|  |  |  socialScienceNotesSubject          SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  socialScienceNotesText             SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  socialScienceNotesType             SimplePrimitiveNode';
         echo PHP_EOL.'|  |  targetSampleSize                      SimpleCompoundNode';
         echo PHP_EOL.'|  |  |  targetSampleActualSize             SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  targetSampleSizeFormula            SimplePrimitiveNode';
         echo PHP_EOL.'|  |  timeMethod                            SimpleControlledVocabularyNode';
         echo PHP_EOL.'|  |  timeMethodOther                       SimplePrimitiveNode';
         echo PHP_EOL.'|  |  unitOfAnalysis                        MultipleControlledVocabularyNode';
         echo PHP_EOL.'|  |  unitOfAnalysisOther                   SimplePrimitiveNode';
         echo PHP_EOL.'|  |  universe                              MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  weighting                             SimplePrimitiveNode';
         echo PHP_EOL.'|  biomedical';
         echo PHP_EOL.'|  |  studyAssayCellType                    MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  studyAssayMeasurementType             MultipleControlledVocabularyNode';
         echo PHP_EOL.'|  |  studyAssayOrganism                    MultipleControlledVocabularyNode';
         echo PHP_EOL.'|  |  studyAssayOtherOrganism               MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  studyAssayOtherMeasurmentType         MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  studyAssayPlatform                    MultipleControlledVocabularyNode';
         echo PHP_EOL.'|  |  studyAssayPlatformOther               MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  studyAssayTechnologyType              MultipleControlledVocabularyNode';
         echo PHP_EOL.'|  |  studyAssayTechnologyTypeOther         MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  studyDesignType                       MultipleControlledVocabularyNode';
         echo PHP_EOL.'|  |  studyDesignTypeOther                  MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  studyFactorType                       MultipleControlledVocabularyNode';
         echo PHP_EOL.'|  |  studyFactorTypeOther                  MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  studySampleType                       MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  studyProtocolType                     MultiplePrimitiveNode';
         echo PHP_EOL.'|  journal';
         echo PHP_EOL.'|  |  journalArticleType                    SimpleControlledVocabularyNode';
         echo PHP_EOL.'|  |  journalVolumeIssue                    MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  journalIssue                       SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  journalPubDate                     SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  journalVolume                      SimplePrimitiveNode';
         echo PHP_EOL.'|  Derived-text';
         echo PHP_EOL.'|  |  source                                MultipleCompoundNode';
         echo PHP_EOL.'|  |  |  ageOfSource                        MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  |  citations                          MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  |  experimentNumber                   MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  |  typeOfSource                       MultiplePrimitiveNode';
         echo PHP_EOL.'|  semantics';
         echo PHP_EOL.'|  |  bugDatabase                           SimplePrimitiveNode';
         echo PHP_EOL.'|  |  designedForOntologyTask               MultipleControlledVocabularyNode';
         echo PHP_EOL.'|  |  endpoint                              SimplePrimitiveNode';
         echo PHP_EOL.'|  |  hasFormalityLevel                     SimpleControlledVocabularyNode';
         echo PHP_EOL.'|  |  hasOntologyLanguage                   SimpleControlledVocabularyNode';
         echo PHP_EOL.'|  |  knownUsage                            SimplePrimitiveNode';
         echo PHP_EOL.'|  |  imports                               MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  resourceVersion                       SimpleCompoundNode';
         echo PHP_EOL.'|  |  |  changes                            SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  modificationDate                   SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  priorVersion                       MultiplePrimitiveNode';
         echo PHP_EOL.'|  |  |  versionInfo                        SimplePrimitiveNode';
         echo PHP_EOL.'|  |  |  versionStatus                      MultipleControlledVocabularyNode';
         echo PHP_EOL.'|  |  typeOfSR                              SimpleControlledVocabularyNode';
         echo PHP_EOL.'|  |  URI                                   MultiplePrimitiveNode';
        }        
    }

?>