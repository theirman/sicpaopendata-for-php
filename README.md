# Utiliser SicpaOpenData dans un projet PHP

![Logo SICPA](https://germinal.toulouse.inra.fr/~theirman/img/CC_BY_INRAE_CatiSicpa.jpg)

Ce projet consiste en la création d'une librairie au format PHAR pour faciliter les déploiements de sets de données vers les portails d'ouverture des données de l'INRAE

## Prérequis :
-	Le PHAR `SicpaOpenData.phar` *(je considère dans cette doc qu’il se trouve dans le dossier lib du projet)*

## Importer et utiliser les classes de SicpaOpenData 

### *API*
Pour importer la classe API de SicpaOpenData, ajouter les lignes suivantes :
```php
require_once("phar://vendor/sicpaopendata.phar/Entity/DataverseAPI.class.php");
use SicpaOpenData\Metadata\DataverseAPI as DataverseAPI;
```
### *Metadonnées*
Pour importer les classes de métadonnées de SicpaOpenData, ajouter les lignes suivantes pour chaque entité que vous souhaitez importer en remplaçant le tag `_CLASS_` par le nom de votre classe *(voir liste des classes ci-dessous)*
```php
require_once("phar://vendor/sicpaopendata.phar/Entity/_CLASS_.class.php");
use SicpaOpenData\Metadata\_CLASS_  as _CLASS_;
```

Et pour la suite, consulter la documentation technique de l’API [ici](https://germinal.toulouse.inra.fr/~theirman/SicpaOpenData/PHP/index.html)

---

## Annexe : Liste des classes de métadonnées disponibles : 

### Le document de métadonnées
`MetadataDocument`
 
### Les classes de catégories
`BiomedicalNode`
`CitationNode`
`DerivedTextNode`
`GeospatialNode`
`JournalNode`
`SemanticsNode`
`SocialScienceNode`

### Les classes de bases
`MultipleCompoundNode`
`MultipleControlledVocabularyNode`
`MultiplePrimitiveNode`
`SimpleCompoundNode`
`SimpleControlledVocabularyNode`
`SimplePrimitiveNode`

 

