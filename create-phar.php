<?php

	#
	# Command : php --define phar.readonly=0 create-phar.php
	# 



	//    ___  ___   ___  ___   __  ________________  ________
	//   / _ \/ _ | / _ \/ _ | /  |/  / __/_  __/ _ \/ __/ __/
	//  / ___/ __ |/ , _/ __ |/ /|_/ / _/  / / / , _/ _/_\ \  
	// /_/  /_/ |_/_/|_/_/ |_/_/  /_/___/ /_/ /_/|_/___/___/  
	//                                                       

	// je définis le nom du fichier
	$pharFile 	= "target/SicpaOpenData.phar";

	// je définis les dossiers à ajouter au fichier PHAR
	$folders 	= array("Entity", "Helper");






	//    ___  ___  ____  ________  ___   __  _____  _______
	//   / _ \/ _ \/ __ \/ ___/ _ \/ _ | /  |/  /  |/  / __/
	//  / ___/ , _/ /_/ / (_ / , _/ __ |/ /|_/ / /|_/ / _/  
	// /_/  /_/|_|\____/\___/_/|_/_/ |_/_/  /_/_/  /_/___/  
	//                                                     

	// -------------------------------- NE RIEN CHANGER SOUS CETTE LIGNE --------------------------------

	// je prépare mon arborescence temporaire
	exec(escapeshellcmd("mkdir tempPharArchive"));

	// je copie tous les dossiers dans l'arborescence temporaire
	foreach($folders as $folder)
		exec(escapeshellcmd("cp -r ".$folder." tempPharArchive"));



	try
	{
		// s'il existe, je supprime le fichier PHAR
		if(file_exists($pharFile))
			unlink($pharFile);

		// s'il existe, je supprime le fichier PHAR au format GZIP
		if(file_exists($pharFile.".gz"))
			unlink($pharFile.".gz");

		// j'instancie un objet PHAR
		$phar = new Phar(
							$pharFile, 
							Phar::CURRENT_AS_FILEINFO | Phar::KEY_AS_FILENAME, 
							basename($pharFile)
					 	);

		// je mets en tampon les modifications que j'effectue sur l'archive
		$phar->startBuffering();

		// j'ajoute mes fichiers à l'archive
		$phar->buildFromDirectory(__DIR__ . "/tempPharArchive");

		// je crée mon bouchon phar
		$stub = "#!/usr/bin/env php \n" . $phar->createDefaultStub();
		$phar->setStub($stub);

		// je termine la mise en tampon des modifications que j'effectue sur l'archive
		$phar->stopBuffering();

		// je change le format de fichier de sortie
		$phar = $phar->compressFiles(Phar::GZ);
		
		// je change les droits d'accès à mon archive 
		chmod(__DIR__."/".$pharFile, 0755);

		// j'affiche le message de succès
		echo "l'archive '".$pharFile."' a ete cree avec succes !".PHP_EOL;
	}
	catch(Exception $e)
	{
		// je renvoie l'éventuel message d'erreur 
		echo $e->getMessage();
	}
	finally
	{
		// je détruis l'arborescence temporaire
		exec(escapeshellcmd("rm -rf tempPharArchive"));
	}

?>